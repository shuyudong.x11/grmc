## Graph Regularized Matrix Completion
This repository contains the implementation of algorithms for Graph-Regularized Matrix Completion proposed in the paper 

S. Dong, P.-A. Absil, and K. A. Gallivan. Riemannian gradient descent methods for graph-regularized matrix completion. *Linear Algebra and its Applications*, 2020. https://doi.org/10.1016/j.laa.2020.06.010.


Shuyu Dong. ICTEAM, UCLouvain, Louvain-la-Neuve, Belgium. 
Email: `shuyu.dong@uclouvain.be` 




### Installation and examples 

Download or clone this repository and access the folder in MATLAB. Try running the following script: 

```matlab
startup; 

% Run one of the experiments in Section 6.2.1. The input value is a given sampling rate. 
pb = expe_gdatPIn0rstar_largemn_1SR(0.2);
```

The following script contains experiments on synthetic data with noisy observations; see Section 6.2.2. 
```matlab
startup; 

% Run one of the experiments in Section 6.2.2. 
run_synNoisy_fewSRs; 
```

The following scripts contains experiments on real data; see Section 6.3. The datasets are included in the mat-files (around 92MB) in `./data/`. 
```matlab
startup; 

% Run experiments in Section 6.3:
run_realML100k;
% or 
run_realTraffic;
```

