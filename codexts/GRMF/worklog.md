


### aug-25
The following two blocks verify that the additional trust regtion-related stopping criterion in `tron.cpp/trcg()` may change the accuracy of the solutions to each of the two gr-least-sq subproblems. 

In addition to the stopping criterion of the standard CG algorithm (res < eps*res_0), the solver (`TRON.cpp/trcg()`) that GRALS/grmf-exp uses has one more "trust region boudary" criterion. Hence the actual method implemented alternatingly minimizes the gr-least-sq subproblems restricted in a "trust region" (of radius \| \partial_G f\| or \| \partial_H f\|). 
**Note**: the algorithm/solver for the two gr-least-sq subproblems is indeed `TR-CG` and not the trust-region algorithm, since the parameter `max_tron_iter=1`, hence the actual solver is `tron.cpp/trcg()`. See more details in [here](./grmf_exp_notes.txt). 

### Illustration 

The setting of the data matrices M* (to be completed) is the same, except that in (1) |M_{ij}|~1 and in (2) |M_{ij}|~1e-3. In (2), the solver (`tron.cpp/trcg()`) is mostly stopped by the trust region-related criterion. 

1. Below are results returned by grals{1,2} and qprecon-RCG, when |M_{ij}|~1. 
```
>> clear; grmf_verify_paramsopt
LRMC: OMEGA sets ready for sampling rates=0.360,
all elements constructed...
The parameters: eps=1.000e-20, max_cg_iter=1000, with solver type (21).
A type 1 (1000 1000 0)
B type 1 (1200 1200 0)
max_cg_iter 1000
max_cg_iter 1000
 loss 3.70805e+06 reg 0 obj 1.85402e+06 rmse 2.930135
GRMF-iter 1 W 0.025477 H 0.042465 walltime 0.067942 loss 728103 reg 0 obj 364051....CG:tron ncg= (1,4) ........... rmse 1.340425
GRMF-iter 2 W 0.063572 H 0.089883 walltime 0.15346 loss 114533 reg 0 obj 57266.3....CG:tron ncg= (4,6) ........... rmse 0.549090
GRMF-iter 3 W 0.13697 H 0.19864 walltime 0.33561 loss 7672.48 reg 0 obj 3836.24....CG:tron ncg= (11,19) ........... rmse 0.141597
GRMF-iter 4 W 0.26439 H 0.24847 walltime 0.51286 loss 135.804 reg 0 obj 67.9021....CG:tron ncg= (22,7) ........... rmse 0.019443
GRMF-iter 5 W 0.37475 H 0.33906 walltime 0.71381 loss 3.33961 reg 0 obj 1.66981....CG:tron ncg= (17,15) ........... rmse 0.003120
GRMF-iter 6 W 0.44874 H 0.43411 walltime 0.88286 loss 0.259966 reg 0 obj 0.129983....CG:tron ncg= (11,16) ........... rmse 0.000870
GRMF-iter 7 W 0.5518 H 0.52419 walltime 1.076 loss 0.00216451 reg 0 obj 0.00108226....CG:tron ncg= (17,15) ........... rmse 0.000082
GRMF-iter 8 W 0.66551 H 0.62458 walltime 1.2901 loss 2.60328e-05 reg 0 obj 1.30164e-05....CG:tron ncg= (19,17) ........... rmse 0.000009
GRMF-iter 9 W 0.7676 H 0.71522 walltime 1.4828 loss 8.79177e-07 reg 0 obj 4.39588e-07....CG:tron ncg= (17,15) ........... rmse 0.000002
GRMF-iter 10 W 0.90122 H 0.81442 walltime 1.7156 loss 6.99238e-09 reg 0 obj 3.49619e-09....CG:tron ncg= (22,17) ........... rmse 0.000000
The parameters: eps=1.000e-01, max_cg_iter=10, with solver type (21).
A type 1 (1000 1000 0)
B type 1 (1200 1200 0)
max_cg_iter 10
max_cg_iter 10
 loss 3.70805e+06 reg 0 obj 1.85402e+06 rmse 2.930135
GRMF-iter 1 W 0.025989 H 0.042433 walltime 0.068422 loss 728103 reg 0 obj 364051....CG:tron ncg= (1,4) ........... rmse 1.340425
GRMF-iter 2 W 0.067398 H 0.089325 walltime 0.15672 loss 114533 reg 0 obj 57266.3....CG:tron ncg= (4,6) ........... rmse 0.549090
GRMF-iter 3 W 0.13551 H 0.15511 walltime 0.29061 loss 10836.8 reg 0 obj 5418.4....CG:tron ncg= (10,10) ........... rmse 0.168604
GRMF-iter 4 W 0.20461 H 0.22256 walltime 0.42717 loss 2663.22 reg 0 obj 1331.61....CG:tron ncg= (10,10) ........... rmse 0.083410
GRMF-iter 5 W 0.26843 H 0.28772 walltime 0.55615 loss 169.941 reg 0 obj 84.9703....CG:tron ncg= (9,10) ........... rmse 0.021559
GRMF-iter 6 W 0.33695 H 0.35397 walltime 0.69092 loss 10.8233 reg 0 obj 5.41164....CG:tron ncg= (10,10) ........... rmse 0.005599
GRMF-iter 7 W 0.39987 H 0.42508 walltime 0.82495 loss 1.08389 reg 0 obj 0.541947....CG:tron ncg= (9,10) ........... rmse 0.001797
GRMF-iter 8 W 0.46401 H 0.49024 walltime 0.95425 loss 0.13111 reg 0 obj 0.065555....CG:tron ncg= (9,10) ........... rmse 0.000635
GRMF-iter 9 W 0.52903 H 0.55759 walltime 1.0866 loss 0.0167999 reg 0 obj 0.00839993....CG:tron ncg= (9,10) ........... rmse 0.000229
GRMF-iter 10 W 0.5928 H 0.62335 walltime 1.2161 loss 0.00226528 reg 0 obj 0.00113264....CG:tron ncg= (9,10) ........... rmse 0.000085
 iter                  cost val     grad. norm
    0   +1.5133486746695967e+05 4.56360599e+02
    1   +1.2830098843364853e+04 9.12729245e+01
    2   +3.6143353230326670e+03 5.23849052e+01
    3   +2.0756871465388317e+03 2.87997431e+01
    4   +1.3876056586260852e+03 1.98490761e+01
    5   +8.1150653229036766e+02 1.77093134e+01
    6   +5.2750242488999447e+02 1.10539336e+01
    7   +3.3051935634698555e+02 7.59348205e+00
    8   +1.7610203580683208e+02 8.60693313e+00
    9   +8.7099767764167794e+01 7.38056743e+00
   10   +1.1529848839594795e+01 3.03300413e+00
   11   +1.7630922593106306e+00 1.19192799e+00
   12   +1.4944618363120932e-01 3.25660107e-01
   13   +1.3775794813455696e-02 1.07985240e-01
   14   +1.9204278431664164e-03 3.86745997e-02
   15   +1.6236311043156369e-04 1.06551190e-02
   16   +1.6169848838734234e-05 3.63101551e-03
   17   +2.3335885208530865e-06 1.33528264e-03
   18   +2.2323113685847892e-07 3.92729423e-04
   19   +2.3643925512560047e-08 1.38489054e-04
   20   +3.4336046309613836e-09 5.13780443e-05
   21   +3.3791748119353803e-10 1.53024231e-05
   22   +3.6665215859252322e-11 5.35088162e-06
   23   +5.5491090958198071e-12 2.05337085e-06
   24   +6.0703894531925240e-13 6.37320379e-07
   25   +6.5794873580587594e-14 2.21032120e-07
   26   +1.0296983709127453e-14 8.91710827e-08
   27   +1.1946586748855043e-15 2.82257977e-08
   28   +1.1871436550294666e-16 9.36192722e-09
   29   +1.7952328613057100e-17 3.80526821e-09
   30   +1.9961644891499099e-18 1.16319100e-09
   31   +1.9387890670392378e-19 3.76220289e-10
Gradient norm tolerance reached; options.tolgradnorm = 1e-09.
Total time is 1.402321 [s] (excludes statsfun)
>>
```

2. Below are results returned by grals{1,2} and qprecon-RCG, when |M_{ij}|~1e-3. The gr-ls solver (TRCG) is mostly stopped by the "trust region boudary" criterion, making the hence the actual algorithm becomes alternatingly minimize the gr-least-sq subproblems restricted in a "trust region" (of radius \| \partial_G f\| or \| \partial_H f\|). 


```
>> clear; grmf_verify_paramsopt
LRMC: OMEGA sets ready for sampling rates=0.360,
all elements constructed...
The parameters: eps=1.000e-20, max_cg_iter=1000, with solver type (21).
A type 1 (1000 1000 0)
B type 1 (1200 1200 0)
max_cg_iter 1000
max_cg_iter 1000
 loss 2.94481e+06 reg 0 obj 1.4724e+06 rmse 2.610885
GRMF-iter 1 W 0.025737 H 0.041125 walltime 0.066863 loss 184.16 reg 0 obj 92.0802....CG:tron ncg= (1,4) ........... rmse 0.021891
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 2 W 0.057821 H 0.095309 walltime 0.15313 loss 1.56693 reg 0 obj 0.783465....CG:tron ncg= (3,6) ........... rmse 0.002280
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 3 W 0.080221 H 0.18225 walltime 0.26247 loss 0.777559 reg 0 obj 0.388779....CG:tron ncg= (1,14) ........... rmse 0.001405
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 4 W 0.10202 H 0.22522 walltime 0.32724 loss 0.769065 reg 0 obj 0.384532....CG:tron ncg= (1,5) ........... rmse 0.001390
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 5 W 0.12437 H 0.26655 walltime 0.39092 loss 0.765005 reg 0 obj 0.382503....CG:tron ncg= (1,5) ........... rmse 0.001383
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 6 W 0.14722 H 0.30293 walltime 0.45015 loss 0.761065 reg 0 obj 0.380532....CG:tron ncg= (1,4) ........... rmse 0.001377
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 7 W 0.16999 H 0.33517 walltime 0.50516 loss 0.756678 reg 0 obj 0.378339....CG:tron ncg= (1,3) ........... rmse 0.001373
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 8 W 0.19255 H 0.36846 walltime 0.56102 loss 0.751488 reg 0 obj 0.375744....CG:tron ncg= (1,3) ........... rmse 0.001367
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 9 W 0.21524 H 0.40049 walltime 0.61573 loss 0.744758 reg 0 obj 0.372379....CG:tron ncg= (1,3) ........... rmse 0.001362
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 10 W 0.23713 H 0.43247 walltime 0.6696 loss 0.736381 reg 0 obj 0.368191....CG:tron ncg= (1,3) ........... rmse 0.001354
The parameters: eps=1.000e-01, max_cg_iter=10, with solver type (21).
A type 1 (1000 1000 0)
B type 1 (1200 1200 0)
max_cg_iter 10
max_cg_iter 10
 loss 2.94481e+06 reg 0 obj 1.4724e+06 rmse 2.610885
GRMF-iter 1 W 0.025933 H 0.041066 walltime 0.066999 loss 184.16 reg 0 obj 92.0802....CG:tron ncg= (1,4) ........... rmse 0.021891
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 2 W 0.058846 H 0.087414 walltime 0.14626 loss 1.56693 reg 0 obj 0.783465....CG:tron ncg= (3,6) ........... rmse 0.002280
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 3 W 0.081733 H 0.15346 walltime 0.23519 loss 0.801537 reg 0 obj 0.400769....CG:tron ncg= (1,10) ........... rmse 0.001469
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 4 W 0.1038 H 0.2049 walltime 0.30869 loss 0.77596 reg 0 obj 0.38798....CG:tron ncg= (1,7) ........... rmse 0.001419
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 5 W 0.12691 H 0.24015 walltime 0.36706 loss 0.770257 reg 0 obj 0.385129....CG:tron ncg= (1,3) ........... rmse 0.001408
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 6 W 0.15005 H 0.27769 walltime 0.42775 loss 0.765093 reg 0 obj 0.382546....CG:tron ncg= (1,4) ........... rmse 0.001397
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 7 W 0.17309 H 0.31618 walltime 0.48927 loss 0.759936 reg 0 obj 0.379968....CG:tron ncg= (1,4) ........... rmse 0.001389
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 8 W 0.19552 H 0.35374 walltime 0.54925 loss 0.753792 reg 0 obj 0.376896....CG:tron ncg= (1,4) ........... rmse 0.001380
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 9 W 0.21793 H 0.39045 walltime 0.60838 loss 0.746289 reg 0 obj 0.373144....CG:tron ncg= (1,4) ........... rmse 0.001373
---->TRCG stopped as it reaches trust region boundary
---->TRCG stopped as it reaches trust region boundary
GRMF-iter 10 W 0.24058 H 0.42679 walltime 0.66737 loss 0.736517 reg 0 obj 0.368259....CG:tron ncg= (1,4) ........... rmse 0.001363

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% below: output of Qprecon-RCG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 iter                  cost val     grad. norm
    0   +1.5576279304449969e-01 4.65957610e-01
    1   +1.2770990906395921e-02 9.57773324e-02
    2   +2.8752417668166871e-03 3.66018043e-02
    3   +1.7514605927351008e-03 1.56010749e-02
    4   +1.4202023300465571e-03 1.92346965e-02
    5   +6.3294827080349653e-04 2.15530246e-02
    6   +7.7057772493690954e-05 8.31589879e-03
    7   +9.8775346113277147e-06 2.72715207e-03
    8   +7.1502583174576797e-07 7.03112901e-04
    9   +6.0675216496016322e-08 2.21049312e-04
   10   +9.7263888571558795e-09 8.73666667e-05
   11   +9.3690806884311544e-10 2.46570207e-05
   12   +8.5290137271165824e-11 7.81409123e-06
   13   +1.3936783385042012e-11 3.38990397e-06
   14   +1.6752012708866365e-12 1.04956720e-06
   15   +1.4808431585933483e-13 3.16242816e-07
   16   +2.0074691918379564e-14 1.28605841e-07
   17   +3.0949019621696368e-15 4.63503889e-08
   18   +3.1400127457870616e-16 1.43243523e-08
   19   +3.5594335379286879e-17 5.21583936e-09
   20   +5.8109489013306696e-18 2.13196910e-09
   21   +6.3068528318630007e-19 6.52793913e-10
Gradient norm tolerance reached; options.tolgradnorm = 1e-09.
Total time is 0.942459 [s] (excludes statsfun)

```



