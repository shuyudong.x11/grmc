function f = F_LRGeomCG_Lreg(prob,x)
%F Cost function 
% INPUT: assume `prob` has field `L`.

f = 0.5*(x.err'*x.err) + prob.Lreg_beta*trace(prob.Lreg_Lmat*(x.U*diag(x.sigma.^2)*x.U'));

if prob.mu>0
  f = f + prob.mu*norm(1./x.sigma)^2;
end
