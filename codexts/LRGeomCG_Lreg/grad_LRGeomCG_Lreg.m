function g = grad_LRGeomCG_Lreg(prob,x)
% % GRAD   Computes the gradient on the manifold

% %  Computes the gradient at a point x of the cost function on 
% %  the manifold.

% NOTE: add step (0) pre-compute LXV.
% (0)
LXV = prob.Lreg_beta*prob.Lreg_Lmat * (x.U * diag(x.sigma));
% ----------------
do_updateSval(prob, prob.temp_omega, x.err, length(x.err));
prob.temp_omega = prob.temp_omega;

T = prob.temp_omega*x.V;
	% g.M = x.U'*T; 
g.M = x.U'*T + x.U'*LXV; 
	% g.Up = T - x.U*(x.U'*T); 
	% todo: find out extra computations/computational-overhead (in flops): very low below, only copying the matrix. 
g.Up = T - x.U*(x.U'*(T+LXV)) + LXV;
	% g.Vp = prob.temp_omega'*x.U; 
	% note: no change induced; the L-reg acts on the row-index-set and does not change Vp-factor. 
g.Vp = prob.temp_omega'*x.U; 
g.Vp = g.Vp - x.V*(x.V'*g.Vp);


