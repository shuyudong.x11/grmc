clear all;
close all;
clc;
colors;

% try
% cd manopt;
% addpath(pwd);
% cd ..;
% catch me
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define the problem size.
m = 10000;
n = 10000;
r = 10;
true_rank = r;

% Define how many entries are revealed (the actual numbers may vary very
% slightly, see further down).
df = r*(m+n-r);
OS_desired = 3;
k_desired = OS_desired*df;

% Build the true matrix AB with specified singular values,
% in factored form.
% singvals = sqrt(m*n)*ones(r, 1);
% singvals = sqrt(m*n)*(1:r)/(r*(r-1)/2);
% singvals = linspace(1000, 10000, r);
% singvals = sqrt(m*n)*logspace(0, -3, true_rank);
% singvals = sqrt(m*n)*exp(-5*(0:true_rank-1)/(true_rank-1));
singvals = [];
[A B] = buildAB(m, n, true_rank, singvals);

% Compute the condition number of X = AB (regardless of the zero signular
% values of course) : diag(S) contains the sing.vals. of AB.
[Ua Sa Va] = svd(A, 'econ');
[Ub Sb Vb] = svd(B, 'econ');
[U S V] = svd(Sa*Va'*Ub*Sb);
condnum = S(1,1)/S(end,end);

% Pick a mask for the observed entries and compute the true values at those
% mask position. Sometimes k is modified by this.
[mask k] = sprandmask(m, n, k_desired, 5);
cd spmaskmult_backup;
Xm = spmaskmultparallel(A, B, mask);
cd ..;
OS = k/df;
knowledge = k/(m*n);

% Possibly add noise to the observed entries
sigma = .0;
Xm = spfun(@(x) x+sigma*randn(size(x)), Xm);

% Represent the sparse measurement matrix in vector form
[I J X] = find(Xm);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

collect = cell(0);

%%%%%%%% RTRMC COMMON WORK
cd RTRMC-release;
addpath(pwd);

rtrmclambda = 0;
rtrmcproblem = buildproblem(I, J, X, ones(size(X)), m, n, r, rtrmclambda);
rtrmcproblem.A = A;
rtrmcproblem.B = B;
rtrmcproblem.Usol = Ua(:, 1:min(r, true_rank));
% this first call somehow makes the next one faster ... ?
initialguess(rtrmcproblem);
tic_initguess = tic();
U0 = initialguess(rtrmcproblem);
time_initguess = toc(tic_initguess);


%%%%%%%% RTRMC 2 with preconditioner
try
opts.method = 'RTR';
opts.order = 2;
opts.precon = true;
opts.computeRMSE = true;
opts.maxiter = inf;
opts.maxtime = 600;
opts.maxinner = 10000;
opts.tolgradnorm = 1e-9;
% opts.Delta_bar = 1/sqrt(2);
% opts.Delta0 = opts.Delta_bar / 16;
opts.verbosity = 2;
%% -- this first call somehow makes all the next ones faster ... ?
opts.maxtime = .1;
rtrmc(rtrmcproblem, opts, U0);
%% --
opts.maxtime = 600;
[Urtrmc, ~, stats] = rtrmc(rtrmcproblem, opts, U0);
rtrmc2prec.time = [stats.time] + time_initguess;
rtrmc2prec.rmse = [stats.RMSE];
rtrmc2prec.color = COLOR_RTRMC2P;
rtrmc2prec.name = 'RTRMC 2p';
collect{end+1} = rtrmc2prec;
catch me
    disp(me);
end


%%%%%%%% RTRMC 2 classic
try
opts.method = 'RTR';
opts.order = 2;
opts.precon = false;
opts.computeRMSE = true;
opts.maxiter = inf;
opts.maxtime = 150;
opts.maxinner = 500;
opts.tolgradnorm = 1e-9;
% profile clear;
% profile on;
[~, ~, stats] = rtrmc(rtrmcproblem, opts, U0);
% profile off;
% profile report;
rtrmc2.time = [stats.time] + time_initguess;
rtrmc2.rmse = [stats.RMSE];
rtrmc2.color = COLOR_RTRMC2;
rtrmc2.name = 'RTRMC 2';
collect{end+1} = rtrmc2;
catch me
    disp(me);
end


%%%%%%%% RCGMC with preconditioner
try
opts.method = 'CG';
opts.order = 1;
opts.precon = true;
opts.computeRMSE = true;
opts.maxiter = inf;
opts.maxtime = 240;
opts.tolgradnorm = 1e-9;
[~, ~, stats] = rtrmc(rtrmcproblem, opts, U0);
rcgmcprec.time = [stats.time] + time_initguess;
rcgmcprec.rmse = [stats.RMSE];
rcgmcprec.color = COLOR_RCGMCP;
rcgmcprec.name = 'RCGMCp';
collect{end+1} = rcgmcprec;
catch me
    disp(me);
end


%%%%%%%% RCGMC without preconditioner
try
opts.method = 'CG';
opts.order = 1;
opts.precon = false;
opts.computeRMSE = true;
opts.maxiter = inf;
opts.maxtime = 240;
opts.tolgradnorm = 1e-9;
[~, ~, stats] = rtrmc(rtrmcproblem, opts, U0);
rcgmc.time = [stats.time] + time_initguess;
rcgmc.rmse = [stats.RMSE];
rcgmc.color = COLOR_RCGMC;
rcgmc.name = 'RCGMC';
collect{end+1} = rcgmc;
catch me
    disp(me);
end


%%%%%%%% RTRMC 1 classic
try
opts.method = 'RTR';
opts.order = 1;
opts.precon = false;
opts.computeRMSE = true;
opts.maxiter = inf;
opts.maxtime = 100;
opts.maxinner = 10;
opts.tolgradnorm = 1e-9;
[~, ~, stats] = rtrmc(rtrmcproblem, opts, U0);
rtrmc1.time = [stats.time] + time_initguess;
rtrmc1.rmse = [stats.RMSE];
rtrmc1.color = COLOR_RTRMC1;
rtrmc1.name = 'RTRMC 1';
collect{end+1} = rtrmc1;
catch me
    disp(me);
end


%%%%%%%% RTRMC 2 legacy code

% % cd GenRTR;
% % addpath(pwd);
% % cd ..;
% % opts.order = 2;
% % opts.computeRMSE = true;
% % opts.maxouter = 100;
% % opts.maxinner = 500;
% % opts.gradtol = 1e-6;
% % % profile clear;
% % % profile on;
% % [~, ~, stats] = rtrmc(rtrmcproblem, opts, U0);
% % % profile off;
% % % profile report;
% % rtrmc2l.time = cumsum([stats.time]) + time_initguess;
% % rtrmc2l.rmse = [stats.dist];
% % rtrmc2l.color = [.5 0 0];
% % rtrmc2l.name = 'RTRMC 2 legacy';
% % collect{end+1} = rtrmc2l;

cd ..;


%%%%%%%% Ngo & Saad (ScGrass-CG)
try
cd NgoSaad;
dist2sol = @(X, Y) sqrt(sqfrobnormfactors(X, Y, A, B)/(m*n));
[~, ~, ~, hist] = ScGrassMC(mask, X, r, dist2sol, ...
                  'tol', 1e-9,...
                  'maxit', 200,...
                  'grad_type','scaled',...
                  'beta_type','P-R',...
                  'sigma_type','approx',... %'tol_reschg', tol_reschg,...
                  'verbose', 1);
% Their code does not account for initguess computation, but it's the same
% computation, so wa add the same number.
ngosaad.time = hist.time + time_initguess;
ngosaad.rmse = hist.rmsefull;
ngosaad.color = COLOR_SCGRASSCG;
ngosaad.name = 'ScGrass-CG';
collect{end+1} = ngosaad;
catch me
    disp(me);
end
cd ..;


%%%%%%%% GROUSE
try
cd GROUSE;
% these have been tuned
SGconst = 0.5;
passes = 20;
dist2sol = @(X, Y) sqrt(sqfrobnormfactors(X, Y, A, B)/(m*n));
[~, ~, ~, grousetime, grousermse] = ...
              grouse(double(I), double(J), X, m, n, r, SGconst, passes, ...
                     dist2sol, U0);
grouse.time = grousetime + time_initguess;
grouse.rmse = grousermse;
grouse.color = [.8 0 .5];
grouse.name = 'GROUSE';
collect{end+1} = grouse;
catch me
    disp(me);
end
cd ..;


%%%%%%%% LMaFit
try
cd LMaFit-adp;
opts.tol = 1e-9;
opts.maxit = 5000;
opts.print = 1;
opts.rank_max = r;
opts.rank_min = r;
opts.dist2sol = @(X, Y) sqrt(sqfrobnormfactors(X, Y, A, B)/(m*n));
Id = sub2ind([m n], I, J);
if ~exist('lmafit_mc_adp') %#ok<EXIST>
    Run_Me_1st;
end
[~, ~, Outlma] = lmafit_mc_adp(m, n, r, Id', X', opts);
lmafit.time = Outlma.time;
lmafit.rmse = Outlma.dist2sol;
lmafit.color = COLOR_LMAFIT;
lmafit.name = 'LMaFit';
collect{end+1} = lmafit;
catch me
    disp(me);
end
cd ..;


%%%%%%%% qGeomMC (Bamdev)
try
cd qGeomMC;
Run_me_first;

% We will account for this with the same timing as the RTRMC methods, since
% this is essentially the same computations.
[Uqgeom Bqgeom Vqgeom] = svds(Xm, r);
G = Uqgeom*(Bqgeom.^(0.5));
H = Vqgeom*(Bqgeom.^(0.5));

params.beta_type = 'P-R';
params.exact_linesearch = true;
params.sigma_armijo = 1e-4; % Armijo backtracking
params.compute_predictions = true;
params.tol = 1e-18;
params.maxiter = 5000;
model.d1 = m;
model.d2 = n;
model.G = G;
model.H = H;
model.GtG = G'*G; 
model.HtH = H'*H;
model.M = Xm;
data_ls.rows = I;
data_ls.cols = J;
data_ls.entries = X;
data_ls.nentries = k;
dist2sol = @(G, H) sqrt(sqfrobnormfactors(G, H', A, B)/(m*n));
[model_CG_gh_exact, infos_CG_gh_exact] = CG_gh(dist2sol, data_ls, model, params);
qGeomMC.time = infos_CG_gh_exact.iter_time + time_initguess;
qGeomMC.rmse = infos_CG_gh_exact.rmse;
qGeomMC.color = COLOR_QGEOMMC;
qGeomMC.name = 'qGeomMC';
collect{end+1} = qGeomMC;
catch me
    disp(me);
end
cd ..;

%%%%%%%% LRGeom (Bart V.)
try
cd BartVDR;
startup;

probvdr.n1 = m;
probvdr.n2 = n;
probvdr.r = r;
probvdr.Omega_i = I;
probvdr.Omega_j = J;
probvdr.Omega = sub2ind([m n], I, J);
probvdr.m = k;
probvdr.data = X;
probvdr.temp_omega = Xm;
probvdr.mu = 0;
probvdr.use_blas = true;


% We will account for this with the same timing as the RTRMC methods, since
% this is essentially the same computations.
x0vdr = make_start_x(probvdr);

optionsvdr = default_opts();
optionsvdr.maxit = 1000;
optionsvdr.abs_grad_tol = 1e-9;
optionsvdr.verbosity = 1;
optionsvdr.dist2sol = @(X) sqrt(sqfrobnormfactors(X.U*diag(X.sigma), X.V', A, B)/(m*n));

[Xcg, histvdr] = LRGeomCG(probvdr, optionsvdr, x0vdr);

lrgeom.time = cumsum(histvdr(:, 5)) + time_initguess;
lrgeom.rmse = histvdr(:, 6);
lrgeom.color = COLOR_LRGEOM;
lrgeom.name = 'LRGeom';
collect{end+1} = lrgeom;
catch me
    disp(me);
end
cd ..;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
salt = randi(999);
set(0,'defaulttextinterpreter','none');
figure(1);
clf;
for i = length(collect) : -1 : 1
    method = collect{i};
    semilogy(method.time, method.rmse, '.-', 'Color', method.color);
    ht = text(method.time(end), double(method.rmse(end)), [' ' method.name]);
    set(ht, 'Color', method.color);
    set(ht, 'FontSize', 8);
    if i == length(collect)
        hold on;
    end
end
hold off;
logaxislabels('Y', [1e-8 1e-4 1e0 1e2]);
xlabel('Time [s]');
ylabel('RMSE');
title(sprintf('%d x %d, rank %d, OS %d, cond %g, true rank %d', m, n, r, OS, condnum, true_rank));
pbaspect([3 1 1]);
box off;

% Set the font size for all texts appearing in the current figure.
all_axes = findall(gcf, 'Type', 'axes');
for haxes = all_axes
	set([haxes ; findall(haxes, 'Type', 'text')], 'FontSize', 8);
end
set(findall(gcf, 'Type', 'text'), 'FontSize', 8);
drawnow;


figname = sprintf('extendedScenario1salt%03d', salt);
laprint(gcf, figname, 'width', 12, 'scalefonts', 'off', 'asonscreen', 'on', 'keepfontprops', 'on');
tex2pdf([figname '.tex'], ['thesis/' figname '.pdf']);
print('-dpng', ['thesis/' figname '.png']);
saveas(gcf, ['thesis/' figname '.fig'], 'fig');
