classdef Solver < handle
    properties
        manifold;
        cost;
        grad;
        hess;
        lsstepsize_initialguess; 
        statsfun;
        stopfun;
        preconditioner;
        params_opt;
        output;
        scores;
        optlog;
    end
    properties (Constant)
        NAMES_ALGO = {'GRALS';...
                      'GRALS1';...
                      'GRALS2';...
                      'RSD_MANOPT';...
                      'RCG_MANOPT';...
                      'RCGprecon_MANOPT'; ... 
                      'RSD'; ...
                      'RCG';...
                      'AltMin1';...
                      'AltMin2'} ; 
        NAMES_LS = {'lsArmijo'; 'lsFree'; 'lsBB'}; 
        NAMES_SS0 = {'linemin'; 'one'; 'lipschitz'}; 
        default_opt =   struct('method_init', 'M0', ...
                            'maxiter', 800, ...
                            'maxtime', 120, ...
                            'rcg_usePrecon', false,...
                            'grmf_maxiter', 100,...
                            'grmf_maxiter_cg', 30,...
                            'grmf_eps', 1e-6,...
                            'tolgradnorm', 1e-9, ...
                            'stepsize0_type', 'exact', ...  % {one, exact, lipschitz, BB} 
                            'ls_backtrack', true,...
                            'ls_force_decrease', false,...
                            'minstepsize', 1e-20,...
                            'useRand', false, ...
                            'storedepth', 5, ...
                            'compute_statsPerIter', true,...
                            'verbosity', 2);

    end
    methods (Static)
        [alpha0, itersdb] = stepsize_init_lipschitz(manifold, costfun, X, d, itersdb)
    end
    methods
        function self = Solver(manifoldname, mcinfo, params_pb, L, params_opt)
            funhandles = GRLRMC.construct_pb(manifoldname, mcinfo, params_pb, L);
            self.manifold = funhandles.man;
            self.cost = funhandles.costfun;
            self.grad = funhandles.gradfun;
            self.statsfun = funhandles.statsfun;
            if isfield(funhandles, 'hessfun')
                self.hess = funhandles.hessfun;
            end
            if isfield(funhandles, 'stopfun')
                self.stopfun = funhandles.stopfun;
            end
             if isfield(funhandles, 'lsfun')
                self.lsstepsize_initialguess = funhandles.lsfun;
            end
            if isfield(funhandles, 'precon') 
                self.preconditioner = funhandles.precon;
            end

            self.params_opt = params_opt;
        end

        function self = loadnew_paramsopt(self, params_opt)
            self.params_opt = params_opt;
        end
        function self = loadnew_funhandles(self, funhandles)
            
            self.manifold = funhandles.man;
            self.cost = funhandles.costfun;
            self.grad = funhandles.gradfun;
            self.statsfun = funhandles.statsfun;
            if isfield(funhandles, 'hessfun')
                self.hess = funhandles.hessfun;
            else
                self.hess = [];
            end
            if isfield(funhandles, 'lsfun')
                self.lsstepsize_initialguess = funhandles.lsfun;
            else
                self.lsstepsize_initialguess = [];
            end
            if isfield(funhandles, 'precon') 
                self.preconditioner = funhandles.precon;
            else
                self.preconditioner = [];
            end
        end
        function self = load_newfun(self, fun, name_fun)
            if ismember(name_fun,{'cost', 'grad', 'hess', ...
                                  'lsstepsize_initialguess',...
                                  'statsfun'}) 
                self.(name_fun) = fun;
            else
                fprintf('no matching function name for the input function handle\n');
                return;
            end
        end
        function X0 = initialization(self, method_init)
            % INPUT:
            % - method_init: (a) a string indicating the initialization method; (b) a point X (given by a certain algorithm).
            if ischar(method_init)
                switch method_init
                    case 'random'
                        %U = randn(self.data.dims(1),self.params_pb.rank);
                        %S = randn(self.params_pb.rank,self.params_pb.rank);
                        %V = randn(self.data.dims(2),self.params_pb.rank);
                        %[U,S,V] = svds(U*S*(V'),self.params_pb.rank); 
                        P = randn(self.data.dims(1),self.params_pb.rank);
                        Q = randn(self.data.dims(2),self.params_pb.rank);
                        [U,S,V] = svds(P*(Q'),self.params_pb.rank); 
                        C = mean(self.mcinfo.PXtar.^2);
                        S = S* sqrt(C/self.params_pb.rank);
                    case 'M0_unbalanced'
                        fa = 5; 
                        [U, S, V] = svds(sparse(double(self.mcinfo.I),...
                                                double(self.mcinfo.J),...
                                                self.mcinfo.PXtar,...
                                                self.mcinfo.size_M(1),...
                                                self.mcinfo.size_M(2)), ...
                                        self.params_pb.rank);
                        U = U * fa; V = V / fa; 
                    case 'M0'
                    % default init method: 
                    [U, S, V] = svds(sparse(double(self.mcinfo.I),...
                                            double(self.mcinfo.J),...
                                            self.mcinfo.PXtar,...
                                            self.mcinfo.size_M(1),...
                                            self.mcinfo.size_M(2)), ...
                                    self.params_pb.rank);
                    % otherwise
                end
                name_manifold = self.manifold.name();
                switch name_manifold
                  case {'GH_qprecon', 'GH_qleftinv', 'GH_qleftinvExa', 'GH_qpreconExa',...
                        'GH_qpreconLeqk', 'GH_qleftinvLeqk'}  
                    sqrtS = diag(sqrt(diag(S)));
                    X0    = struct('L', U*sqrtS, 'R', V*sqrtS);
                   case {'GH_Euc', 'GH_EucCruderls', 'GH_EUC'}
                    sqrtS = diag(sqrt(diag(S)));
                    X0    = struct('G', U*sqrtS, 'H', V*sqrtS);
                    % X0    = struct('G', U*sqrtS, 'H', V*sqrtS, 'U',U, 'S',S, 'V',V);
                  case 'USV_embeded' % GRLRMC.NAMES_MAN{2}
                    X0    = struct('U', U, 'S', S, 'V', V);
                  otherwise
                    error('initialization in other manifold structures not implemented yet \n');
                end
            elseif isstruct(method_init)
                % this is when method_init = X (a matrix product variable)
                X0 = method_init;
            else 
                error('The input argument method_init is not compatible...\n');
            end
        end
        function [X, stats, self] = solve_rsd_manopt(self, method_init, varargin)
            if ischar(method_init)
                % override by the option in params_opt (this is set at the
                % instantiation of self, via the key string `methods_init`). 
                method_init = self.params_opt.method_init;
            end
            X0 = self.initialization(method_init); 
            self.optlog.X0 = X0;
            methodname = Solver.NAMES_ALGO{3}; %RSD_MANOPT

            optproblem = struct('Xtar', self.Xstar, 'Xtar_r', self.Xstar_r,...
                              'M', self.manifold, 'cost', self.cost, 'grad', self.grad);
            if ~isempty(self.hess)
                optproblem.hess = self.hess;
            end
            if ~isempty(self.lsstepsize_initialguess)
                optproblem.linesearch = self.lsstepsize_initialguess;
            end
            if ~isempty(self.preconditioner) && self.params_opt.rcg_usePrecon 
                optproblem.precon = self.preconditioner;
                methodname = 'RSDprecon_MANOPT';
            end

            options_manopt          = self.params_opt;
            options_manopt.statsfun = self.statsfun;
            % note (#1209): add+ 1 more user-defined function in
            % `options_manopt` via `self.stopfun`, which is to be added via 
            % `cls:grlrmc/func:construct_pb()`
            if ~isempty(self.stopfun)
                options_manopt.stopfun = self.stopfun;
            end
            [X, ~, stats] = steepestdescent(optproblem, X0, options_manopt) ;          
            self.optlog.(methodname) = stats;
            % % note (#328,2019): the following procedure is no longer needed. 
            % self.collect_results(X, stats, 'methodname', methodname, varargin{:});
        end 
        function [X, stats, self] = solve_rcg_manopt(self, method_init, varargin)
            if ischar(method_init)
                % override by the option in params_opt (this is set at the
                % instantiation of self, via the key string `methods_init`). 
                method_init = self.params_opt.method_init;
            end
            X0 = self.initialization(method_init); 
            self.optlog.X0 = X0;
            % note (#702): add to `self.params_opt` a function handle `linesearch`, then the linesearch 
            methodname = Solver.NAMES_ALGO{2};
            % funtion will be `linsearch_hint.m`.
            optproblem = struct('Xtar', self.Xstar, 'Xtar_r', self.Xstar_r,...
                              'M', self.manifold, 'cost', self.cost, 'grad', self.grad);
            if ~isempty(self.hess)
                optproblem.hess = self.hess;
            end
            if ~isempty(self.lsstepsize_initialguess)
                optproblem.linesearch = self.lsstepsize_initialguess;
            end
            if ~isempty(self.preconditioner) && self.params_opt.rcg_usePrecon 
                optproblem.precon = self.preconditioner;
                methodname = 'RCGprecon_MANOPT';
            end

            options_manopt          = self.params_opt;
            options_manopt.statsfun = self.statsfun;
            if ~isempty(self.stopfun)
                options_manopt.stopfun = self.stopfun;
            end
            [X, ~, stats] = conjugategradient(optproblem, X0, options_manopt) ;          
            self.optlog.(methodname) = stats;
        end 
        function [X, stats, self] = solve_rtr_manopt(self, method_init, varargin)

            if nargin < 2
                method_init = 'M0';
            end
            methodname = 'RTR_MANOPT'; %Solver.NAMES_ALGO{4};
            X0 = self.initialization(method_init);
            self.optlog.X0 = X0;
            % note (#702): add to `self.params_opt` a function handle `linesearch`, then the linesearch 
            % funtion will be `linsearch_hint.m`.
            optproblem = struct('Xtar', self.Xstar, 'Xtar_r', self.Xstar_r,...
                                'M', self.manifold, 'cost', self.cost, 'grad', self.grad);
            if ~isempty(self.hess)
                optproblem.hess = self.hess;
            end
            if ~isempty(self.lsstepsize_initialguess)
                optproblem.linesearch = self.lsstepsize_initialguess;
            end
            % if ~isempty(self.preconditioner)  
            %     optproblem.precon = self.preconditioner;
            % end
             options_manopt          = self.params_opt;
            options_manopt.statsfun = self.statsfun;
            [X, ~, stats] = trustregions(optproblem, X0, options_manopt) ;          
            self.optlog.(methodname) = stats;
            self.collect_results(X, stats, 'methodname', methodname, varargin{:});
        end 
        [newx, newf, lsstats] = linesearch_Armijo(self, x, d, f0, ...
                                                  df0, options, store, stepsize0)
        [X, stats, self] = solve_RGD_lsArmijo(self, method_init, varargin)
        [X, stats, self] = solve_RGD_lsFree(self, method_init, varargin)
        [X, stats, self] = solve_RCG_lsFree(self, method_init, varargin)

        % Check initial step size: computed by linesearch_initialpt
        function [h, info] = check_initialstepsize_sd(self, x)
        % Input: 
        %           x, dir, alpha_init, manifold.
        % Output: 
        %           A 1-D curve of (t, f(x(t)), where x(t) = R_x(t.dir), for
        %           t=[0, 2alpha_init]. Also show markers on the x-axis for the
        %           3 roots found by exact line minimization.
            problem=struct('M', self.manifold, 'cost', self.cost, 'grad', self.grad);
            if ~isempty(self.hess)
                problem.hess = self.hess;
            end
            if ~isempty(self.lsstepsize_initialguess)
                problem.linesearch = self.lsstepsize_initialguess;
            end
            % problem = self.optproblem; 
            if nargin < 2
                x = problem.M.rand();
            end
            % Compute the value f0 at f and directional derivative at x along d.
            storedb = StoreDB();
            xkey = storedb.getNewKey();
            f0 = getCost(problem, x, storedb, xkey);
            temp = getGradient(problem, x, storedb, xkey); 
            dir = problem.M.lincomb(x, -1, temp); 
            h(1) = figure();
            checkgradient(problem, x, dir); 
            if isfield(problem, 'hess')
               h(3) = figure();
               % checkhessian(problem, x, dir);
               checkhessian(problem);
            end
            store = storedb.getWithShared(xkey);
            [alpha0, store] = problem.linesearch(x, dir, store); 
            % Compute the value of f at points on the geodesic (or approximation
            % of it) originating from x, along direction d, for stepsizes in a
            % range = [0, 2*alpha0]|norm_d|.
            ts = linspace(0, 2, 51)*alpha0;
            value = zeros(size(ts));
            for i = 1 : length(ts)
                y = problem.M.exp(x, dir, ts(i));
                ykey = storedb.getNewKey();
                value(i) = getCost(problem, y, storedb, ykey);
            end           
            % And plot it.
            h(2) = figure();
            plot(ts, value); hold on;
            % plot([0,roots], [f0,f0+dflin_roots], 'k+');
            plot(ts(26), value(26), 'r^');
            info = struct('ts', ts, 'fts', value);
        end
    end
end

