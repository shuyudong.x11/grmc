% This is a method of the class Solver: 

function [stepsize, newx, lsstats] = linesearch_Armijo(self, x, d, ...
                                        f0, df0, options, store, stepsize0)
% This function implements the generic backtracking linesearch algorithm using
% the Armijo rule. 

    % Allow omission of the key, and even of storedb.
    if ~exist('key', 'var')
        if ~exist('storedb', 'var')
            storedb = StoreDB();
        end
        key = storedb.getNewKey();
    end

    % Backtracking default parameters. These can be overwritten in the
    % options structure which is passed to the solver.
    default_options.ls_contraction_factor = .5;
    default_options.ls_suff_decr = 1e-4;
    default_options.ls_max_steps = 25;
    default_options.ls_backtrack = true;
    default_options.ls_force_decrease = true;
    
    if ~exist('options', 'var') || isempty(options)
        options = struct();
    end
    options = mergeOptions(default_options, options);
    
    contraction_factor = options.ls_contraction_factor;
    suff_decr = options.ls_suff_decr;
    max_ls_steps = options.ls_max_steps;
    
    % Obtain an initial guess at alpha from the problem structure.
    alpha = stepsize0; 
    
    % Make the chosen step and compute the cost there.
    newx = self.manifold.retr(x, d, alpha);
    % % Since it is a new key, the getCost.m function in manopt/linesearch*.m
    % % will actually bring an empty store for computing cost. This is
    % equivalent to directly computing the cost. 
    newf = self.cost(newx, []); 
    cost_evaluations = 1;
    
    % Backtrack while the Armijo criterion is not satisfied
    while options.ls_backtrack && newf > f0 + suff_decr*alpha*df0
        
        % Reduce the step size,
        alpha = contraction_factor * alpha;
        
        % and look closer down the line
        newx = self.manifold.retr(x, d, alpha);
        newf = self.cost(newx, []); 
        cost_evaluations = cost_evaluations + 1;
        
        % Make sure we don't run out of budget
        if cost_evaluations >= max_ls_steps
            break;
        end
        
    end
    
    % If we got here without obtaining a decrease, we reject the step.
    if options.ls_force_decrease && newf > f0
        alpha = 0;
        newx = x;
        newkey = key;
        newf = f0; %#ok<NASGU>
    end
    
    % As seen outside this function, stepsize is the size of the vector we
    % retract to make the step from x to newx. Since the step is alpha*d:
    norm_d = self.manifold.norm(x, d);
    stepsize = alpha * norm_d;
    
    % Return some statistics also, for possible analysis.
    lsstats.costevals = cost_evaluations;
    lsstats.stepsize = stepsize;
    lsstats.alpha = alpha;


end



