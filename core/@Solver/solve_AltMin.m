function [x, info, self] = solve_AltMin(self, method_init, varargin)
% This function implements the Alternating Minimization (AltMin) algorithm.
% INPUT
% (1) method_init: two types of inputs are possible,
%     - A. String such as 'M0', 'random' etc. The method for producing an
%          initial point x. 
%     - B. Variable x. A point x is directly given as an initial point. See
%          Solver/initialization.m
% OUTPUT
% (1) x: the last iterate satisfying one of the stopping criterion.  
% (2) info: iteration-related informatin, such as gradnorm, ncgG,ncgH, etc. 

% % Verify that the problem description is sufficient for the solver.
% if ~isa(self.cost, 'function_handle') % ~canGetCost(problem)
%     warning('Solver: ', ...
%             'No cost provided. The algorithm will likely abort.');
% end
% if ~isa(self.grad, 'function_handle') %~canGetGradient(problem) && ~canGetApproxGradient(problem)
%     % Note: we do not give a warning if an approximate gradient is
%     % explicitly given in the problem description, as in that case the
%     % user seems to be aware of the issue.
%     warning('Solver: ', ...
%            ['No gradient provided. Using an FD approximation instead (slow).\n' ...
%             'It may be necessary to increase options.tolgradnorm.\n' ...
%             'To disable this warning: warning(''off'', ''manopt:getGradient:approx'')']);
% end
% Set local defaults here.
% localdefaults.minstepsize = 1e-10;
localdefaults.maxiter = 1000;
localdefaults.tolgradnorm = 1e-6;
% Merge user-defined options into local defaults (the 2nd input arg.
% overrides the 1st one), see manopt/core/mergeOptions.m.  
% When this function is called `paramsopt is     
% already set via Tester.parseArgs('opt',varargin{:})  in Tester() or run_tester().
options = mergeOptions(localdefaults, self.params_opt); 
options.stopfun = self.stopfun; 

timetic = tic();

% Initialization. 
x         = self.initialization(method_init); 
[grad, store] = self.grad(x,[]); 
gradnorm  = self.manifold.norm(x, grad);

% At any point, iter is the number of fully executed iterations so far.
iter = 0;

% Save stats in a struct array info and preallocate.
stats = savestats();
info(1) = stats;
info(min(10000, options.maxiter+1)).iter = [];

if options.verbosity >= 2
    fprintf(' iter\t      RMSE val\t    grad. norm\n');
end

% Start iterating until stopping criterion triggers.
while true

    % Display iteration information.
    if options.verbosity >= 2
        fprintf('%5d\t%+.8e\t%.8e\n', iter, stats.RMSE, gradnorm);
    end
    % Start timing this iteration
    timetic = tic();
    
    % Run standard stopping criterion checks
    [stop, reason] = stoppingcriterion(self, x, options, info, iter+1);
    
    if stop
        if options.verbosity >= 1
            fprintf([reason '\n']);
        end
        break;
    end

    [x, stats_G] = solve_grls_G(self, x, options); 
    [grad, ~] = self.grad(x,[]); 

    [x, stats_H] = solve_grls_H(self, x, options); 
    [grad, store] = self.grad(x,[]); 
    gradnorm  = self.manifold.norm(x, grad);
    
    % iter is the number of iterations we have accomplished.
    iter = iter + 1;

    % Log statistics for freshly executed iteration.
    stats = savestats(); 
    info(iter+1) = stats;
end

info = info(1:iter+1);

if options.verbosity >= 1
    fprintf('Total time is %f [s] (excludes statsfun)\n', ...
            info(end).time);
end
   
    function stop = check_stopcriterion(self, x, options, info, last) 
    % Routine in charge of checking stopping criterions 
    % The stopping criterion in consideration are:
    % a. iteration budget,
    % b. gradient norm,
    % c. accuracy parameter for each of the least-squares subproblems. 
        stop = 0;
        reason = '';
        stats = info(last);

        % Allotted iteration count exceeded
        if isfield(stats, 'iter') && isfield(options, 'maxiter') && ...
           stats.iter >= options.maxiter
            reason = sprintf('Max iteration count reached; options.maxiter = %g.', options.maxiter);
            stop = 4;
            return;
        end
        
        % Target gradient norm attained
        if isfield(stats, 'gradnorm') && isfield(options, 'tolgradnorm') && ...
           stats.gradnorm < options.tolgradnorm
            reason = sprintf('Gradient norm tolerance reached; options.tolgradnorm = %g.', options.tolgradnorm);
            stop = 2;
            return;
        end

        % Check whether the possibly user defined stopping criterion
        % triggers or not.
        if isfield(options, 'stopfun')
            userstop = options.stopfun(self, x, info, last);
            if userstop
                reason = 'User defined stopfun criterion triggered; see options.stopfun.';
                stop = 6;
                return;
            end
        end
    end

    function stats = savestats()
    % Routine in charge of collecting the current iteration stats
          stats.iter = iter; 
          stats.cost = nan;
          stats.gradnorm = gradnorm;
          stats.linesearch = [];
          if iter == 0
              stats.time = toc(timetic);
              stats.ncgGPerI = 0;
              stats.ncgHPerI = 0;
              stats.cgG_relres = nan;
              stats.cgH_relres = nan;
              stats.cgG_flag = nan;
              stats.cgH_flag  = nan;
          else
              stats.time = info(iter).time + toc(timetic);
              stats.ncgGPerI = stats_G.cg_ncg;
              stats.ncgHPerI = stats_H.cg_ncg;
              stats.cgG_relres = stats_G.cg_relres;
              stats.cgH_relres = stats_H.cg_relres;
              stats.cgG_flag = stats_G.cg_flag;
              stats.cgH_flag   = stats_H.cg_flag;
          end
          stats = self.statsfun([], x, stats, store) ; 
     end
end

