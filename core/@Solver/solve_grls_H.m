function [x, stats, self] = solve_grls_H(self, x, options)
% This function models the Graph-regularized Least-squares problem.
% Some information about the function pcg: 
% [x,flag] = pcg(A,b,...) also returns a convergence flag.
% Flag (Convergence) 
% 0 pcg converged to the desired tolerance tol within maxit iterations.
% 1 pcg iterated maxit times but did not converge.
% 2 Preconditioner M was ill-conditioned.
% 3 pcg stagnated. (Two consecutive iterates were the same.)
% 4 One of the scalar quantities calculated during pcg became too small or too large to continue computing.
% Whenever flag is not 0, the solution x returned is that with minimal norm residual computed over all the iterations. No messages are displayed if the flag output is specified.
% 
% [x,flag,relres] = pcg(A,b,...) also returns the relative residual norm(b-A*x)/norm(b). If flag is 0, relres <= tol.
% [x,flag,relres,iter] = pcg(A,b,...) also returns the iteration number at which x was computed, where 0 <= iter <= maxit.
% [x,flag,relres,iter,resvec] = pcg(A,b,...) also returns a vector of the residual norms at each iteration including norm(b-A*x0).
% 
% INPUT
% (1) method_init: two types of inputs are possible,
%     - A. String such as 'M0', 'random' etc. The method for producing an
%          initial point x. 
%     - B. Variable x. A point x is directly given as an initial point. See
%          Solver/initialization.m
% OUTPUT
% (1) x: the last iterate satisfying one of the stopping criterion.  
% (2) info: iteration-related informatin, such as gradnorm, ncgG,ncgH, etc. 

    % Verify that the problem description is sufficient for the solver.
    if ~isa(self.cost, 'function_handle') % ~canGetCost(problem)
        warning('Solver: ', ...
                'No cost provided. The algorithm will likely abort.');
    end
    if ~isa(self.grad, 'function_handle') %~canGetGradient(problem) && ~canGetApproxGradient(problem)
        % Note: we do not give a warning if an approximate gradient is
        % explicitly given in the problem description, as in that case the
        % user seems to be aware of the issue.
        warning('Solver: ', ...
               ['No gradient provided. Using an FD approximation instead (slow).\n' ...
                'It may be necessary to increase options.tolgradnorm.\n' ...
                'To disable this warning: warning(''off'', ''manopt:getGradient:approx'')']);
    end
    mcinfo = self.mcinfo; 
    % Set parameters
    tol     = options.grmf_eps ;
    maxit   = options.grmf_maxiter_cg ;  
    
    % Initial guess s0 = vec(x.H')
    temp = x.H';
    s0 = temp(:); 
    % Verify dimensions (not necessary, can be removed later) 
    k = size(x.H, 2); 
    if k ~= numel(s0) / mcinfo.size_M(2)
        error('Dimension of s=vec(H^T) is not compatible with H..\n'); 
    end
    % Compute the vector b = vec(G'* P_\Omega(M)) 
    b_mat = x.G' * sparse(double(mcinfo.I), double(mcinfo.J), mcinfo.PXtar,...
                          mcinfo.size_M(1), mcinfo.size_M(2));
    b = b_mat(:); 
    % Define A by the function handle afun  
    Theta = self.params_pb.alpha_c*speye(self.data.dims(2))...
            + self.params_pb.Lreg_betac*self.L.Lc;
    A = @(s) afun(s, x.G, mcinfo, Theta, k, mcinfo.size_M(2)); 

    % solve the grls subproblem by using pcg (precon-cg), 
    [s, flag, relres, iter, resvec] = pcg_2(A, b, tol, maxit,[],[], s0); 

    % Save outputs from pcg to stats. 

    % Return solution
    x.H = reshape(s, [k, mcinfo.size_M(2)])'; 
    stats = savestats(); 

    function Ax = afun(s, G, mcinfo, Lap, k, m)
    % This function returns Ax = A*x, where A is the Hessian of the
    % following subproblem (GRLS),
    
        % Compute B*vec(H'), returns a vector of size k*n as is s=vec(G'). 
        BVec = comp_AOmegaVecH(s, G, mcinfo.I, mcinfo.J); 

        % Compute L\otimes vec(G'), whose unvec is G'*Lap.
        LapH = reshape(s,[k,m])*Lap;  % reshape(..) here takes only 0.0003sec for the setting k = 20, n=10000. 
        Ax = BVec + LapH(:);
    end

    function stats = savestats()
    % Routine in charge of collecting the current iteration stats
    % Outputs of pcg to save: flag, relres, iter. 
         stats.cg_ncg     = iter;
         stats.cg_relres  = relres;
         stats.cg_flag    = flag;
    end
     
end


