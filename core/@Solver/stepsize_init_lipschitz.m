% This is a static function of the class Solver: 
function [alpha0, itersdb] = stepsize_init_lipschitz( x, d, itersdb, store, manifold )

% information of the two latest consecutive iterates are stored in itersdb 

% Lip_est = abs(itersdb(1).fobj - itersdb(2).fobj ) / manifold.dist(itersdb(1).x, itersdb(2).x);
% alpha0 = 1/ Lip_est; 
alpha0 = manifold.dist(itersdb(1).x, itersdb(2).x) /...
         manifold.dist(itersdb(1).rgrad, itersdb(2).rgrad) ;

% saveguard to bound the amplitude of alpha0: 
if length(itersdb(1).stepsize0) > 100 
    alpha0 = min(alpha0, max(itersdb(1).stepsize0)); 
end
% this procedure will likely to let alph0 stablize and remain fixed after a
% certain number of iterations. 

% save this to itersdb(1).
itersdb(1).stepsize0(end+1) = alpha0;

end


