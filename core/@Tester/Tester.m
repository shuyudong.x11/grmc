classdef Tester < GRLRMC 

    properties
        tid; 
        data;
        table_hyperparams;
        table_hp_multiphase;
        info_methods;
        range_samplrates; 
        opts_tester;
        opts_Lmat;
        params_data;
        MCINFOS;
        status_runTester;
        storeCV_3dmat;
        resultsCV_opByMethods;
        resultsCVgagno_opByMethods;
        resPerIterMC_3dCellArrays;
        resPerIterGRMC_3dCellArrays;
        resPerI_fixedp_srByHpByTeByMethod;
        resPerI_multip_srByHpByTeByMethod;
        scoresGRMC_4dmat;
        scoresMC_4dmat;
        scores_fixedp_srByHpByMethodBySctype;
        scores_multip_srByHpByMethodBySctype; 
        selecHP_fixedp_srByMethod;
        selecHP_multip_srByMethod;
    end

    properties (Constant)
        DIR_FIGS = './figs';
        N_METHODS = 10; 
        DEFAULT_infoMethods2 = struct('list_man', {GRLRMC.NAMES_MAN}, ...
                                      'list_alg', {Solver.NAMES_ALGO}, ...
                                      'list_ls',  {Solver.NAMES_LS}, ...
                                      'list_ss0',  {Solver.NAMES_SS0}, ...
                                      'ids_man',            [1 3 4], ... % the input is a 1xN array, N being the number of methods to test in total
                                      'ids_alg',            [1 2 3], ...
                                      'ids_ls',             [1 1 2], ...
                                      'ids_ss0',            [1 1 1], ...
                                      'willRun2',            [1 1 0], ...
                                      'willRun2_multiphase', [0 0 1], ...
                                      'doCrossval2',         [0 0 0]);        
        % Default_infoMethods is outdated (though functioning with same outdated
        % functions) and is replaced by ~infoMethods2 above. 
        DEFAULT_infoMethods = struct('name', {{ 'GRALS'; ...
                                                'GH_Euc-RCG_MANOPT'; ...
                                                'GH_Euc-RCGprecon_MANOPT'; ...
                                                'GH_Euc-RCGCruderls_MANOPT';...
                                                'USV_embeded-RCG_MANOPT'; ...
                                                'GH_qprecon-RCG_MANOPT'; ...
                                                'GH_qleftinvExa-RCG_MANOPT';...
                                                'GH_Euc-RSD_MANOPT'; ...
                                                'GH_qprecon-RSD_MANOPT'; ...
                                                'GH_qleftinvExa-RSD_MANOPT'...
                                                }}, ...
                                    'willRun',          [1;1;1;0;0;0;0;0;0;0],...
                                    'willRun_multiphase',  [0;0;0;0;0;1;1;0;0;0],...
                                    'doCrossval',       [0;0;1;0;0;0;0;0;0;0],...
                                    'cvscore',          Inf(Tester.N_METHODS,1),... 
                                    'cvscore_gagno',    Inf(Tester.N_METHODS,1),... 
                                    'jstar_params_gagno', ones(Tester.N_METHODS,1), ...
                                    'jstar_paramsMC',   ones(Tester.N_METHODS,1), ...
                                    'jstar_params',     zeros(Tester.N_METHODS,1),...
                                    'score_gridsel', Inf(Tester.N_METHODS,1), ...
                                    'jstar_gridsel', zeros(Tester.N_METHODS,1));
        DEFAULT_rdat =	 struct('use_submatrix', false,...
                                'dims_submatrix', [300,600]);
        DEFAULT_gdat =	 struct('gdatSizes', [300,300],...
								'gdatType_g', 0,... 
								'gdatParam_g', nan,...
								'gdatType_Z', 'graph-lowrank',... 
								'gdatType_specfun', 'tikhonov',... 
                                'gdatType_specfun_doTruncate', false, ...
								'gdatParam_specfun',1,...
								'gdatParam_targetKappa', 100,...
								'gdatParam_scale', 0.1,...
								'gdatRank', 10,...
								'gdatNoise', [0, 0]);
        DEFAULT_rangeSamplrates = struct('sampl_rate', linspace(0.1,0.4,5));
        DEFAULT_rank            = struct('rank',10);
        DEFAULT_buildLmat = struct( 'methodname', {{ 'epsNN-GaussianKernel'; ...
                                                'epsNN-Euclidean'; ...
                                                'kNN-GaussianKernel'; ...
                                                'kNN-Euclidean' }}, ...
                                    'willUse', [1;0;0;0],...
                                    'fromSideinfo', [1;1;1;1], ...
                                    'hatM0_rank', 20*ones(4,1), ...
                                    'sparsity',  [0.08;0.08;nan;nan],...
                                    'sigma',  [1e0; 1e0; nan; nan],...
                                    'kNN_k', [nan;nan;20;20] );
        DEFAULT_runTester    = struct('do_randomsearch',    true,...
                                        'alpha_r',          [-9;-2],...
                                        'Lreg_betar',       [-9;0],...
                                        'Lreg_gammar',      [-3;3],...
                                        'alpha_c',          [-9;-2],...
                                        'Lreg_betac',       [0;0], ...
                                        'betar_mul',        [0;3], ...
                                        'rank',             [10;10], ...
                                        'hp_nconfigs',      floor(4^3/3),...
                                        'crossval_K',       3,...
                                        'crossval_maxiter', 800,...
                                        'crossval_average4CVscore', 'median',...
                                        'repeatTests_maxiter', 800,...
                                        'repeatTests_tolGrad', 1e-14,...
                                        'maxiter_perPhase', 400, ...
                                        'maxiter_fixedp',   800, ...
                                        'n_epoch',          600, ...
                                        'n_repeatTests',    10); 
       

    end

	methods (Static)
        function [mann, algon] = parse_manalgo_name(manalgoname)
            ind = find(manalgoname=='-');
            switch manalgoname 
            case 'GRALS'
                % Only possible if algoname is 'grmf':
                mann = 'GH_Euc'; % 'GH_EUC'; 
                algon = 'GRALS';
            otherwise
                mann = manalgoname(1:(ind-1));
                algon = manalgoname((ind+1):end);
            end
        end
        function [table] = genTABLE(sets)
            if nargin < 2
                keys = 'nan';
            end
            if ~iscell(sets) && isstruct(sets)
                fds = fieldnames(sets);
                for i = 1 : length(fds)
                    temp{i} = sets.(fds{i});
                end
            end
            sets = temp;
            % Return the cartesian product table with the swapped keys, sets
            c = cell(1, numel(sets));
            [c{:}] = ndgrid( sets{:} );
            table = cell2mat( cellfun(@(v)v(:), c, 'UniformOutput',false) );
        end        
        function G = gengraph_from(graphType, m, graphParam, varargin)
            opts = struct();
            switch graphType
                case 'erdos_renyi'
                    G = gsp_erdos_renyi(m, graphParam(1));
                case 'random_k'
                    G = gsp_random_regular(m, graphParam(1));
                case 'sensor'
                    G = gsp_sensor(m);
                case 'community'
                    G = gsp_community(m);
                case 'minnesota'
                    G = gsp_minnesota(1);
            end
            [U, Lambda] = eig_ordered( full(G.L), 'ascend');
            Lambda(1) = 0; % in case the first eigenvalue ->0 but not 0.
            G.eigU = U;
            G.eigLam = Lambda;
            if ~isfield(G, 'type')
                G.type = graphType;
            end
        end
        function [U, specValues, param] = gdata_specFilter(G, params)
        % Build a g-spectral filter from graph G
            type_specfun = params.gdatType_specfun ;
            do_truncate  = params.gdatType_specfun_doTruncate ;
            if ~isfield(G, 'eigLam')
                [U, Lambda] = eig_ordered(full(G.L), 'ascend');
                eigLam(1) = 0; % just to ensure numerically Lambda(1) = 0 (always true).
            else
                U = G.eigU;
                eigLam = G.eigLam;
            end
            switch type_specfun
                case 'pseudoinv' % GMRF X~N(\bar{x}, L^\dagger)
                    specFunc = @(lambda, l) 1./sqrt(lambda.^l);
                    param = params.gdatParam_specfun(1);
                case 'diffusion' % spectral filtering on Gaussian vectors: diffusion
                    specFunc = @(lambda, t) exp(-t*lambda/2);
                    param = 2*log(params.gdatParam_targetKappa) /...
                    			eigLam(params.gdatRank);
                case 'tikhonov' % spectral filtering on Gaussian vectors: Tikhonov
                    specFunc = @(lambda, Lreg_beta) 1./sqrt(1+Lreg_beta*lambda);
                    param = (params.gdatParam_targetKappa^2 - 1) /...
                             (eigLam(params.gdatRank)+params.gdatParam_targetKappa^2*eigLam(2));
                    % param = (params.gdatParam_targetKappa^2 - 1) /...
                    %         (eigLam(params.gdatRank));
            end
            specValues = specFunc(eigLam, param);
            if params.gdatType_specfun_doTruncate
                specValues(params.gdatRank+1:end) = 0;
            end
            specValues(isinf(specValues)) = 0; % only active in case "pseudoinv"
        end
        function data = load_gdata(G, Z, Z_clean, opts )
            if nargin < 4
                opts = [];
                % gen_opts = struct('seename', 'gdat_');
            end
            if ~isempty(opts)
                fds = fieldnames(opts);
                for ii = 1 : length(fds)
                    if strcmp(fds{ii}(1:min(4,length(fds{ii}))),'gdat')
                        name_opt = fds{ii};
                        gen_opts.(name_opt) = opts.(name_opt);
                    end
                end
            else
                gen_opts = nan;
            end
            wref        = half_vectorization(full(double(G.W))); 
            G.w         = sparse(wref/norm(wref,2));
            data        = struct('name', sprintf('synthetic-%s',G.type), 'mat', Z, ...
                                 'mat_clean', Z_clean, 'dims', size(Z), ...
                                 'graph', G, 'gen_opts', gen_opts);
        end
        function data = gen_gdata(params_data)
        % The parameters needed for this function are stored in the 2nd input argument:
        % - gdatType_g: indicates the type of the graph. 
        % - gdatSizes: 1d array of size 2. 
        % - gdatNoise: 1d array of size 2. 
        %   - gdatNoise(1) = 0 or 1, to indicate whether the noise is to be
        %   added to the input tensor.  
        %   - gdatNoise(2) stores the value of the noise level, in terms of
        %   standard variation (sigma < 1) of the Gaussian noise model or
        %   the SNR (assumed to be >1). The SNR measurement is recommended since 
        %   it is a ratio and is a rather standard way for measuring the
        %   noise level. 
            G = Tester.gengraph_from(params_data.gdatType_g,...
                                     params_data.gdatSizes(1), ...
                                     params_data.gdatParam_g);
            % targetKappa is an option in gdatParam_specfun(). 
            [U, specValues, param] = Tester.gdata_specFilter(G, params_data);

            % In case where the "param_specfun" is determined by the 
            % target condition number "param_targetKappa":
            params_data.gdatParam_specfun(end+1) = param; 
            nb_samples = params_data.gdatSizes(2);

            switch params_data.gdatType_Z
                case 'graph-fullrank'
                    M = randn(G.N, nb_samples);
                    Z = U * diag(specValues) * M;
            	case 'graph-lowrank'
                % As in [Rao et al. '15], model with Z=AMB^T, where M has a
                % low rank. Generate a low-rank random Gaussian matrix M=
                % P\Sigma Q^T. 
		            rank_data = params_data.gdatRank;
		            Sigma = rand(rank_data,1);
		            M = (orth(randn(G.N, rank_data)) * ...
		            	diag(Sigma))*...
		            	orth(randn(nb_samples,rank_data))'; 
                    Z = U * diag(specValues) * M;
            	%case 'LR-bandlimited' %'normal'
            	%	M = randn(G.N, nb_samples);
                %    specValues(params_data.gdatRank+1 : end) = 0;
                %    Z = U * diag(specValues) * M;
                % Enables "gdatType='graph-agnostic'": Z=M; 
                case 'graph-agnostic'
                    rank_data = params_data.gdatRank;
                    Z = randn(G.N, rank_data)*randn(rank_data, nb_samples);
                    G.L = sparse(G.N,G.N,0);
                otherwise
            		fprintf('Warning: no matching type for gdatType_Z\n');
            		return;
            end
            scale = params_data.gdatParam_scale; 
            fa = (scale/ mean(abs(Z(:)))) ; % |Z_{ij}| ~ scale  
            Z = Z * fa ; 
            if ~isfield(params_data, 'gdatNoise') % ~.gdatNoise = (1/0, sigma)
                params_data.gdatNoise = [1, 1e-2];
            end
            if params_data.gdatNoise(2) > 1 
                % This is the case where the noise parameter is in
                % SNR(dB).
                SNRdB = params_data.gdatNoise(2);
                noise_sigma = norm(Z,'fro')/sqrt(prod(size(Z)))/10^(SNRdB/20);
            else
                noise_sigma = params_data.gdatNoise(2);
            end
            if params_data.gdatNoise(1)
                Z_clean = Z;
            else
                Z_clean = nan;
            end
            Z = Z + params_data.gdatNoise(1)*...
            		noise_sigma*...
            		randn(G.N, nb_samples);
            data = Tester.load_gdata(G, Z, Z_clean, params_data ); % gen_opts: no longer needed, allinfo c.f. <params_data.gdat_>
        end
        function data = load_data(name_dataset, varargin)
            opts = Tester.parseArgs('rdat', varargin{:});
            data = struct();
            raw = loaddata(name_dataset);
            switch name_dataset
                case 'pwsmooth'
                    data.mat = raw.M;
                case 'ecaFRGECH'
                    data.mat = double(raw.mat);
                case 'usps'
                    data.mat = im2double(reshape(raw,[256,11000])');
                case {'elecT2000', 'traffic', 'electricity'}
                    data.mat = raw;
                case {'lmafitML100k', 'lmafitML1M', 'jester_1'}
                    Ot = find(raw.Msub);
                    Otr = setdiff(find(raw.M), Ot);
                    data = struct('mat', raw.M, 'dims',size(raw.M),...
                                  'Omega_t', Ot, 'Omega_tr',Otr);
                case 'mgcnnML100k'
                    data = struct('mat', raw.M, 'dims',size(raw.M),...
                                  'Omega_t', find(raw.Otest), 'Omega_tr',find(raw.Otraining));

            end
            data.mat_clean = nan; 
            data.dims = size(data.mat);
            data.name = name_dataset;
            if any(strcmp(name_dataset, {'ecaFRGECH', 'usps', ...
                                         'elecT2000', 'traffic',...
                                         'electricity'})) ...
            && opts.use_submatrix
                dims = opts.dims_submatrix;
                data.dims = dims; 
                data.mat = data.mat(1:dims(1),1:dims(2));            
            end
        end
		function mcinfos = gen_MCINFOS(Xtar, sampl_rates)
        % This function generates the input data for matrix completion according
        % to a series (by default) of sampling rate values.
            mcinfos = {};
			for i = 1 : length(sampl_rates)
				mcinfos{i} = GRLRMC.genOMEGA_MC(Xtar, sampl_rates(i));
			end
            fprintf('LRMC: OMEGA sets ready for sampling rates=%.3f, \n',...
                    sampl_rates);
		end
        function params = parseArgs(param_name, varargin)
		    p = GRLRMC.build_parser(Tester.DEFAULT_gdat);
		    p = GRLRMC.build_parser(Tester.DEFAULT_rdat, p);
            p = GRLRMC.build_parser(Tester.DEFAULT_buildLmat, p);
            p = GRLRMC.build_parser(Tester.DEFAULT_rangeSamplrates, p);
            p = GRLRMC.build_parser(Solver.default_opt, p);
            p = GRLRMC.build_parser(Tester.DEFAULT_runTester, p);
            p = GRLRMC.build_parser(Tester.DEFAULT_infoMethods, p);
            p = GRLRMC.build_parser(Tester.DEFAULT_infoMethods2, p);
            parse(p, varargin{:});
            switch param_name
            case 'opt'
                fds = fieldnames(Solver.(sprintf('default_%s',param_name)));
            otherwise
                fds = fieldnames(Tester.(sprintf('DEFAULT_%s',param_name)));
            end
		    for i = 1 : length(fds)
		    	params.(fds{i}) = p.Results.(fds{i});
		    end
		end
        function scorePerIters = getscores_fromMCscores(mcscores, name_score, KEYS)
        % Get recovery score by the name of the recovery score in
        % GRLRMC.m/KEYS_MCscores. 
            if nargin < 3
                KEYS = GRLRMC.KEYS_MCscores;
            end
            idx=find(strcmp(name_score, KEYS));
            scorePerIters = mcscores(idx,:);
        end
        function [Wsp, eps] = buildW_sparsify(W, sparsity)
        % This function compute eps such that the empirical cumulative
        % distribution of (W(i,j)) at eps equals (1-sparsity), that is,  
        % p((i,j): W[i,j] < eps) = 1-sparsity.
        % The output eps will serve as the threshold value for whether an
        % adjacency coefficent of W is kept or truncated to zero. 

        % Turn diagonal entries = 0, since we do not consider self-loops
        % (self-similarity is not interesting to us).
            W = W - diag(diag(W));
        % Compute the empirical cdf function 
            [cdf, w] = ecdf(W(:));
        % Find eps = ecdf^{-1}(1-sparsity), we target cdf=1-sparsity with a
        % tolerance radius of 1e-6 (if there are several candidates in the
        % bin of [cdf +- 1e-6], we take the one in the middle. 
        % Find the target bin of radius 2*tol (tol=1e-6) containing the wanted eps:
            tol = 1e-7; ids = [];
            while isempty(ids) 
                tol = 10*tol;
                ids = find( abs(cdf - (1-sparsity))<tol );
            end
        % Take the value in the middle of the targeted bin:
            len = numel(ids);
            eps = w(ids(ceil(len/2)));
        % Return the sparse graph adjacency matrix
            Wsp = sparse(W.*(W>=eps));
        end

        function [W_sp, eps, sigma] = buildW_epsNN_GaussianKer(featMat, opts)
        % Build a graph adjacency matrix using the epsilon-Nearest-Neighborhood
        % (eps-NN) model. 
            sparsity = opts.sparsity;
            Z = pdist2(featMat, featMat);
            sigma = var(Z(:))/5;
            [W_sp, eps] = Tester.buildW_sparsify(exp(-Z.^2/sigma), sparsity);
        end

        function Lmat = computeLmat_fromW(W, opts)
        % Get the graph Laplacian matrix from the given adjacency matrix. 
            d = sum(W,1);
            if isfield(opts, 'type_graphLaplacian') && ...
               strcmp(opts.type_graphLaplacian,'normalized')
                Lmat = sparse(eye(size(W,1)) - diag(sqrt(d))*W*diag(sqrt(d)));
            else
                Lmat = sparse(diag(d)) - W;
            end
        end
        function [L, t] = buildLmat_fromData(feat, opts, varargin)
        % This function builds a graph Laplacian matrix from data. 
            fprintf('\n.....Start building Lmat from data with method="%s"... ', opts.methodname); 
            t0=tic;
            switch opts.methodname 
                case 'epsNN-GaussianKernel'
                    [Wr , eps_r, sigma]= Tester.buildW_epsNN_GaussianKer(feat.feat_r, opts);
                    % [Wc, eps_c] = Tester.buildW_epsNN_GaussianKer(feat.feat_c, opts);
                    Wc = sparse(size(feat.feat_c,1), size(feat.feat_c,1));
                    opts.eps_r = eps_r;
                    opts.sigma_r = sigma;
                case 'kNN-Euclidean'
                % Example: use pdist2(_,_, 'euclidean', 'Smallest', k), c.f.
                % [here](https://nl.mathworks.com/help/stats/pdist2.html). 
                    error('The method %s for building graph Laplacian matrices is not available yet.\n', opts.methodname);
                case 'kNN-GaussianKernel'
                % Example: use pdist2(_,_, 'euclidean', 'Smallest', k), c.f.
                % [here](https://nl.mathworks.com/help/stats/pdist2.html). 
                    error('The method %s for building graph Laplacian matrices is not available yet.\n', opts.methodname);
                otherwise
                    error('The method %s for building graph Laplacian matrices is not available.\n', opts.methodname);
            end
            t = toc(t0);
            fprintf('Done: '); toc(t0);
            L = struct('Lr', Tester.computeLmat_fromW(Wr, opts), ...
                       'Lc', Tester.computeLmat_fromW(Wc, opts),...
                       'Wr', Wr, ...
                       'feat_samplrate', feat.feat_samplrate,...
                       'opts_buildLmat', opts); 
        end
        function data = feed_data(name_dat, varargin)
		    if isempty(name_dat)
		    	name_dat = 'synthetic';
		    end
		    if ischar(name_dat)
			    switch name_dat
			        case 'synthetic'
                    % Parser for varargin: parse all arguments and then 
                        params_data = Tester.parseArgs('gdat', varargin{:});
			            data = Tester.gen_gdata(params_data);
			        otherwise %'traffic'
			            data = Tester.load_data(name_dat, varargin{:});
			    end
			else
				error('dataset name not valid, abort ..\n');
			end
            [ur,sr,vr] = svds(data.mat, Tester.DEFAULT_rank.rank);
            data.mat_r = ur * sr * (vr'); 
        end
        function s2dmat = gen_RSddimSamples(intervals, ns)
        % INPUT:
        %       (1) intervals: 2d array of size [2 x d]: each column
        %       contains [a_min;a_max].
        %       d=#hyperparameters + 1 ("rank value").
        %       (2) ns: number of samples to generate
        % OUTPUT:
        %       (1) s2dmat: 2d array of size [ns x d]
            s2dmat = zeros(ns, size(intervals,2));
            dims_box = [-1, 1]*intervals;
            for j  = 1 : size(intervals, 2)
                s2dmat(:,j) = intervals(1,j)+rand(ns,1)*dims_box(j);
            end
        end
        function [table, opts] = gen_tableHp3_(varargin)
        % Generate a set of parameters (alpha, beta_r, beta_c). 
            opts   = Tester.parseArgs('runTester', varargin{:});
            if opts.do_randomsearch
               % input: intervals ([4x2] matrix) in log scale 
                interv1 = struct2array(...
                            struct('alpha',opts.alpha_r,...
                                   'Lreg_gammar', opts.Lreg_gammar,...
                                   'Lreg_gammac', [-Inf;-Inf],...
                                   'rank', opts.rank) );
                ntotal = opts.hp_nconfigs;
                % The size of o2darray is (ntotal_f, 4), the 4 cols are
                % 'alpha', 'Lreg_gammar', 'Lreg_betac', 'rank'.
                o2darray = [Tester.gen_RSddimSamples(interv1, ceil(0.75*ntotal))];
                % Sampling from [-Inf; -Inf] gives NaN instead of -Inf, so we
                % need to set Nan back to -Inf:
                o2darray(isnan(o2darray)) = -Inf;

                n_comp = floor(0.25*ntotal) ; 
                o2darray = [o2darray; ...
                            [o2darray(1:n_comp,1),-Inf(n_comp,2),o2darray(1:n_comp,4)]
                           ]; 
                % % if a value is nonpositive, it is in logscale. 
                % i_logscale = find(o2darray<=0);  
                % o2darray(i_logscale) = 10.^(o2darray(i_logscale));
                grid0_5d = [zeros(1,3), opts.rank(1)];
                o2darray = [grid0_5d; [10.^o2darray(:,1:3), o2darray(:,4)]]; 
                ntotal_f = size(o2darray,1);

                % alpha_r = alpha_c = alpha, which is in o2darray(:,1),
                % beta_r  = alpha*gamma_r, 
                % beta_c  = 0, we only consider row-wise graph
                % regularization. 
                table = ...
                array2table([[1:ntotal_f]', o2darray(:,1), o2darray(:,1), ...
                            o2darray(:,1).*o2darray(:,2),...
                            o2darray(:,1).*o2darray(:,3),...
                            o2darray(:,4)],...
                'VariableNames',{'id','alpha_r','alpha_c','Lreg_betar','Lreg_betac','rank'});
            else
                % grid search
            end
        end
        function [table, opts] = gen_tableHp3(varargin)
        % Generate a set of parameters (alpha, beta_r, beta_c). 
            opts   = Tester.parseArgs('runTester', varargin{:});
            if opts.do_randomsearch
               % input: intervals ([4x2] matrix) in log scale 
                interv1 = struct2array(...
                            struct('alpha',opts.alpha_r,...
                                   'Lreg_gammar', opts.Lreg_gammar,...
                                   'Lreg_gammac', [-Inf;-Inf],...
                                   'rank', opts.rank) );
                interv2 = struct2array(...
                            struct('alpha',opts.alpha_r,...
                                   'Lreg_gammar', [-Inf;-Inf],...
                                   'Lreg_gammac', [-Inf;-Inf],...
                                   'rank', opts.rank) );
                ntotal = opts.hp_nconfigs;
                % The size of o2darray is (ntotal_f, 4), the 4 cols are
                % 'alpha', 'Lreg_gammar', 'Lreg_betac', 'rank'.
                o2darray = [Tester.gen_RSddimSamples(interv1,floor(3*ntotal/4)); ...
                            Tester.gen_RSddimSamples(interv2,ceil(ntotal/4))];
                % Sampling from [-Inf; -Inf] gives NaN instead of -Inf, so we
                % need to set Nan back to -Inf:
                o2darray(isnan(o2darray)) = -Inf;

                % % if a value is nonpositive, it is in logscale. 
                % i_logscale = find(o2darray<=0);  
                % o2darray(i_logscale) = 10.^(o2darray(i_logscale));
                grid0_5d = [zeros(1,3), opts.rank(1)];
                o2darray = [grid0_5d; [10.^o2darray(:,1:3), o2darray(:,4)]]; 
                ntotal_f = size(o2darray,1);

                % alpha_r = alpha_c = alpha, which is in o2darray(:,1),
                % beta_r  = alpha*gamma_r, 
                % beta_c  = 0, we only consider row-wise graph
                % regularization. 
                table = ...
                array2table([[1:ntotal_f]', o2darray(:,1), o2darray(:,1), ...
                            o2darray(:,1).*o2darray(:,2),...
                            o2darray(:,1).*o2darray(:,3),...
                            o2darray(:,4)],...
                'VariableNames',{'id','alpha_r','alpha_c','Lreg_betar','Lreg_betac','rank'});
            else
                % grid search
            end
        end
        function [table, opts] = gen_tableHyperparams(varargin)
            opts   = Tester.parseArgs('runTester', varargin{:});
            if opts.do_randomsearch
                % o2darray1 = Tester.gen_RSddimSamples(intervals, ns);
                % input: intervals ([5x2] matrix) =
                interv1 = struct2array(...
                            struct('alpha_r',opts.alpha_r,...
                                   'alpha_c', opts.alpha_c,...
                                   'Lreg_betar', opts.Lreg_betar,...
                                   'Lreg_betac', [0;0],...
                                   'rank', opts.rank) );
                interv2 = struct2array(...
                            struct('alpha_r',opts.alpha_r,...
                                   'alpha_c', opts.alpha_c,...
                                   'Lreg_betar', [0;0],...
                                   'Lreg_betac', [0;0],...
                                   'rank', opts.rank) );
                 interv3 = struct2array(...
                            struct('alpha_r',opts.alpha_r,...
                                   'alpha_c', [0;0],...
                                   'Lreg_betar', opts.Lreg_betar,...
                                   'Lreg_betac', [0;0],...
                                   'rank', opts.rank) );
                 interv4 = struct2array(...
                            struct('alpha_r', [0;0],...
                                   'alpha_c', [0;0],...
                                   'Lreg_betar', opts.Lreg_betar,...
                                   'Lreg_betac', [0;0],...
                                   'rank', opts.rank) );
                grid0_5d = [zeros(1,4), opts.rank(1)];
                ntotal = opts.hp_nconfigs;
                o2darray = [grid0_5d; ...
                            Tester.gen_RSddimSamples(interv1,floor(ntotal/4)); ...
                            Tester.gen_RSddimSamples(interv3,ceil(ntotal/4)); ...
                            Tester.gen_RSddimSamples(interv4,ceil(ntotal/2)) ];
                ntotal_f = size(o2darray,1);
                i_logscale = find(o2darray<0);  
                o2darray(i_logscale) = 10.^(o2darray(i_logscale));

                table = ...
                array2table([[1:ntotal_f]',o2darray],...
                'VariableNames',{'id','alpha_r','alpha_c','Lreg_betar','Lreg_betac','rank'});
            else
                % grid search
            end
        end
		function [table_hyperparams, opts_tester] = ...
        gen_tableHyperparams_RSfromGrid(varargin)
        % note (#1128): 
            opts_tester   = Tester.parseArgs('runTester', varargin{:});
            sampler = @(grid,n)...
            grid(randsample(size(grid,1),min(n,size(grid,1))), :);
            if opts_tester.do_randomsearch
            % Generate points with the uniform distribution from a d-dimensional box.
                grid1_5d = Tester.genTABLE(struct('alpha_r',opts_tester.alpha_r,...
                                   'alpha_c', opts_tester.alpha_c,...
                                   'Lreg_betar', opts_tester.Lreg_betar,...
                                   'Lreg_betac', 0,...
                                   'rank', opts_tester.rank));

                grid2_5d = Tester.genTABLE(struct('alpha_r',opts_tester.alpha_r,...
                                   'alpha_c', opts_tester.alpha_c,...
                                   'Lreg_betar', 0,...
                                   'Lreg_betac', 0,...
                                   'rank', opts_tester.rank));

                grid3_5d = Tester.genTABLE(struct('alpha_r',opts_tester.alpha_r,...
                                   'alpha_c', 0,...
                                   'Lreg_betar', opts_tester.Lreg_betar,...
                                   'Lreg_betac', 0,...
                                   'rank', opts_tester.rank));

                grid4_5d = Tester.genTABLE(struct('alpha_r',0,...
                                   'alpha_c', 0,...
                                   'Lreg_betar', opts_tester.Lreg_betar,...
                                   'Lreg_betac', 0,...
                                   'rank', opts_tester.rank));
                
                grid0_5d = [zeros(1,4), opts_tester.rank];
                ntotal = opts_tester.hp_nconfigs;
                o2darray = [grid0_5d; ...
                            sampler(grid1_5d,ceil(ntotal/2)); ...
                            sampler(grid2_5d,ceil(ntotal/6)); ...
                            sampler(grid3_5d,ceil(ntotal/6)); ...
                            sampler(grid4_5d,ceil(ntotal/6)) ];
                ntotal_f = size(o2darray,1);

                table_hyperparams = ...
                array2table([[1:ntotal_f]',o2darray],...
                'VariableNames',{'id','alpha_r','alpha_c','Lreg_betar','Lreg_betac','rank'});
            else
                % grid search
            end
        end
        function tt = gen_infoMethods2(opts_im2)
        % Turn the struct opts_infoMethods2 into a table (Nx~) of methods that will be
        % tested. 
        % Now the output is backwards compatible with the output of
        % gen_infoMethds.m.  
            if nargin < 1
                opts_im2 = Tester.DEFAULT_infoMethods2;
            end
            names_man = {opts_im2.list_man{opts_im2.ids_man}}'; 
            names_alg = {opts_im2.list_alg{opts_im2.ids_alg}}'; 
            types_ls  = {opts_im2.list_ls{opts_im2.ids_ls}}';
            if numel(opts_im2.ids_ss0) ~= numel(opts_im2.ids_ls)
                % This happens if 'ids_ss0' is not set by the user in the
                % scripts and equals the default setting. Then we just set all
                % type_ss0 to 'exact', for all methods to test (selected by the
                % user) listed in names_{algo, man}.   
                opts_im2.ids_ss0 = ones(1, numel(opts_im2.ids_ls)); 
            end
            types_ss0  = {opts_im2.list_ss0{opts_im2.ids_ss0}}';
            tt = table(names_man, names_alg, types_ls, types_ss0, opts_im2.willRun2', ...
                       opts_im2.willRun2_multiphase', opts_im2.doCrossval2', ...
                       'VariableNames',...
                       {'name_man','name_algo', 'type_ls', 'type_ss0', 'willRun', 'willRun_multiphase',...
                        'doCrossval'}); 
        end
        function [info_methods] = gen_infoMethods(opts_infoMethods)
        % Generate a table of information for methods, based on
        % Tester.DEFAULT_infoMethods. 
            if nargin < 1
                opts_infoMethods = Tester.DEFAULT_infoMethods;
            end
            n_methods =size(opts_infoMethods.name,1) ;
            for i = 1 : n_methods
                [name_mans{i}, name_algo{i}] = Tester.parse_manalgo_name(opts_infoMethods.name{i});
            end
            info_methods = [table(name_mans', name_algo', 'VariableNames',...
                                  {'name_man','name_algo'}), ...
                            struct2table(opts_infoMethods)];
        end
        % Generate time string. 
        function timeStr = gen_timeStr()
           	tmps= datetickstr(now,'hhMM_ddmmm_');
            timeStr = tmps{1};
        end
        function name = render_plotableMethodname(pb, id_m)
        % This function helps making the method names readable. 
        % The updated version (#510) is backward compatible since the
        % fields in the info_methods property remain the same. 
            methodname = sprintf('%s: %s', ...
                         pb.info_methods(id_m, :).name_man{1},...
                         pb.info_methods(id_m, :).name_algo{1}); 
            methodname = strrep(methodname, '_', '-'); 
            % "GH-" is redundant now in the legend since all the methods work with the two-factor product space
            methodname = strrep(methodname, 'GH-', ''); 
            methodname = strrep(methodname, 'qprecon', 'Qprecon'); 
            methodname = strrep(methodname, 'qleft', 'Qleft'); 

            name = strrep(methodname, '-MANOPT', ''); 
            if sum(ismember('Exa', name)) == 3 || sum(ismember('GRALS',name))==5
                name = strrep(name, 'Exa:', ' (b):'); 
            else
                name = strrep(name, ':', ' (a):'); 
            end

            if ismember('type_ls', pb.info_methods.Properties.VariableNames )
               if ~strcmp(pb.info_methods(id_m,:).type_ls{1}, 'lsArmijo')
                    name = sprintf('%s %s', name,...
                           pb.info_methods(id_m,:).type_ls{1});
               end
            end
            if ismember('type_ss0', pb.info_methods.Properties.VariableNames )
               if ~strcmp(pb.info_methods(id_m,:).type_ss0{1}, 'linemin')
                    name = sprintf('%s stepsize-%s', name,...
                                    pb.info_methods(id_m,:).type_ss0{1});
               end
            end
        end    
        % Functions for producing various types of figures. 
        funit = comp_flops_unit(pb, SR)
        [array2d,Fu] = get2dArray_resultsPerIter(pb, stats, i_sr, rid_im)
        produce_txtTesterId(pb, sid,DIR)
        produce_saveFigures(sid)
        
        [h, curr ] = produce_resPerI_TrVsTe(pp, i_sr, ihp_fix, ihp_mul, x_type,  i_te) 
        [hh, cur] = produce_figTrVsTe(pp, type, i_te, x_type)
        
        output       = get_mcscores_sr_hp_meth_te(pb, type_reg, name_sc)
        [curs, tabs] = extract_tabRecVsSR(pp, name_sc)
        tabs         = extract_tabRecrVsSR(pp, name_sc)
        [h, tabs]    = produce_figRecVsSR(pp, name_sc, type_y)
        tabs         = produce_tabsMCscore_meth_model(pp, name_sc) 
 
        [h, cur] = produce_fig512(pp, type, i_te, x_type, statsname )
        curves = extract_resPerI_(pp, i_samplrate, i_hp_fix, i_hp_mul, i_te, xIsTime, name_stats)        
        curves = extract_resPerI_2(pp, i_samplrate, ihp_fix, ihp_mul, i_te, x_type, name_stats)
        [h, cur] = produce_resPerI_(pp, i_sr, ihp_fix, ihp_mul, name_stats, x_type,  i_te) 
        sid=save_res2matfile(pp) 

        function h = plot_resultsPerIter(curves, showLabelst) 
        % Plot curves with Y-axis in log-scale, each curve is a struct of fields:
        % x, y, labelx, labely, methodname
            if nargin < 2
                showLabelst = false;
            end
            h = figure();
            flgs = {}; 
            for i = 1 : numel(curves)
                plot(curves(i).x, curves(i).y); 
                if showLabelst 
                    flgname = sprintf('%s %s', curves(i).methodname,...
                                  curves(i).statsname); 
                else
                    flgname = sprintf('%s %s', curves(i).methodname,...
                                  curves(i).statsname); 
                end
                if ~any(strcmp(flgs, flgname))
                    flgs{end+1} = flgname;
                end
                hold on;
            end
            set(gca,'YScale','log','FontSize', 16);
            xlabel(curves(1).labelx);
            ylabel(curves(1).labely);
            legend(flgs);
            hold off;
        end
        function h = gramm_resPerI_2(curves, methodnames)
        % INPUT:
        % (1) curves: an array of structs produced by
        %     Tester/extract_resPerI_2.m
        % (2) methodnames (optional): a cell array of methods to show. 
            if nargin < 2
                methodnames = {};
            end
            if ~isdir(Tester.DIR_FIGS)
               DIR_FIGS = '~/Dropbox/DBucl/temporary/matfiles';
            else
               DIR_FIGS = Tester.DIR_FIGS;
            end
            xx = []; yy = []; labels = {};
            show_allmethods = isempty(methodnames); 
            for i = 1 : numel(curves)
                name = curves(i).methodname;
                % Either we only show methods present in methodnames, or we
                % show all methods recorded in curves if methodnames is empty: 
                if any(strcmp(name, methodnames)) || show_allmethods  
                    len = numel(curves(i).x); 
                    xx = [xx; curves(i).x'];
                    yy = [yy; curves(i).y'];
                    labels(end+1:end+len,1) = {name };
                end
            end
            clear g; 
            g = gramm('x',xx, 'y', yy, 'color', labels); 
            g.axe_property('YScale','log');
            g.stat_summary('type','95percentile', 'geom','line');

            figure(); 
            g.draw();
            % figname = sprintf('Gramm_resPerI_%s', ...
            %                  Tester.gen_timeStr());
            % g.export('file_name', figname,...
            %          'export_path', sprintf('%s/',DIR_FIGS),...
            %          'file_type','svg'); 
            
            % Alternatively, we can plot the curves in g using bulit-in plot
            % by collecting the label (color), x, y information. 
            % The number of curves produced is numel(g.results.color). 
            for i = 1 : numel(g.results.color)
                cur_m(i) = struct('labelx', curves(1).labelx, ...
                                  'labely', curves(1).labely,...
                                  'x', g.results.stat_summary(i).x,...
                                  'y', g.results.stat_summary(i).y,...
                                  'methodname', g.results.color{i});    
            end
            h = Tester.plot_resultsPerIter(cur_m);
        end
        function results = saveRes(pb)
            % Save 
            % data, L, table_hyperparams, 
            % scores_fixedp_srByHpByMethodBySctype;
            % scores_multip_srByHpByMethodBySctype; 
            % resPerI_fixedp_srByHpByTeByMethod;
            % resPerI_multip_srByHpByTeByMethod;
            results = struct('dataname', pb.data.name, ...
                            'graphL', pb.L, ...
                            'tab_hp', pb.table_hyperparams,...
                            'sc_fix',...
                            pb.scores_fixedp_srByHpByMethodBySctype,...
                            'sc_mul',...
                            pb.scores_multip_srByHpByMethodBySctype,...
                            'resPerI_fix',...
                            {pb.resPerI_fixedp_srByHpByTeByMethod},...
                            'resPerI_mul',...
                            {pb.resPerI_multip_srByHpByTeByMethod}); 

           save(sprintf('%s_%s_results.mat', pb.data.name, Tester.gen_timeStr()), 'results');
        end
	end
	methods
		function self = Tester(name_data, varargin)
		% Instantiate experiment with default settings to 
			if nargin < 1
				name_data = 'synthetic';
			end
            % (1) return data, L, from the given name `name_data`
            data = Tester.feed_data(name_data, varargin{:});
            range_samplrates    = Tester.parseArgs('rangeSamplrates', varargin{:});
			mcinfos = Tester.gen_MCINFOS(data.mat, range_samplrates.sampl_rate);
            % (2) construct ` opts_tester, table_hyperparams`:
            % [table_hyperparams, opts_tester] = Tester.gen_tableHyperparams(varargin{:});
            [table_hyperparams, opts_tester] = Tester.gen_tableHp3(varargin{:});

            self = self@GRLRMC('some-manifold', data.mat, mcinfos{1});
            self.params_opt = Tester.parseArgs('opt',varargin{:});
			self.data = data;
            self.MCINFOS = mcinfos;
			self.range_samplrates = range_samplrates.sampl_rate;
            % (1b) feed graph information either from synthetic model or built from data:
            self.feed_Lmat([], varargin{:});
			self.opts_tester = opts_tester;
            self.table_hyperparams = table_hyperparams;
            
            % (3) construct `info_methods`: 
            [self.info_methods] = Tester.gen_infoMethods(...
                                  Tester.parseArgs('infoMethods', varargin{:}) );
            self.resultsCV_opByMethods = nan(numel(self.range_samplrates), ...
                                             size(self.info_methods,1),...
                                             size(table_hyperparams,2)+1);
            self.resultsCVgagno_opByMethods = nan(numel(self.range_samplrates), ...
                                             size(self.info_methods,1),...
                                             size(table_hyperparams,2)+1);
            % (4) construct containers for results (including: crossval, resultsPerIter, MCscores).
            self.resPerIterMC_3dCellArrays = ...
                cell(numel(self.range_samplrates),...
                     opts_tester.n_repeatTests,...
                     sum(self.info_methods.willRun));
            self.resPerIterGRMC_3dCellArrays = ...
                cell(numel(self.range_samplrates),...
                     opts_tester.n_repeatTests,...
                     sum(self.info_methods.willRun));
            self.storeCV_3dmat = nan(numel(self.range_samplrates),...
                                     sum(self.info_methods.doCrossval), ...
                                     size(table_hyperparams,1));
            self.scoresGRMC_4dmat = nan(numel(self.range_samplrates),...
                                        sum(self.info_methods.willRun),...
                                        numel(self.KEYS_MCscores), ...
                                        opts_tester.n_repeatTests);
            self.scoresMC_4dmat = nan(numel(self.range_samplrates), ...
                                      sum(self.info_methods.willRun),...
                                      numel(self.KEYS_MCscores), ...
                                      opts_tester.n_repeatTests);
		end
        function refresh_Lmat(self, mcinfo, varargin)
        % This function refreshes the graph Laplacian matrix when
        % self.data.name is NOT synthetic, the dataset does not contain side
        % information and when `mcinfo` has a different `sampl_rate` than the
        % one with which `self.L` was constructed:
            if ~strcmp(self.data.name, 'synthetic')
                if ~self.opts_Lmat.fromSideinfo % && ~isfield(self.data, 'SIDEINFO')
                    if self.L.feat_samplrate ~= mcinfo.sampl_rate
                        self.feed_Lmat(mcinfo);
                    end
                end
            end
        end
        function feed_Lmat(self, mcinfo, varargin)
        % This function feeds the graph Laplacian matrix to GRLRMC.L by
        % constructing the matrix from data that is only available on Omega. 
        % INPUT: 
        %       mcinfo: input data of the matrix completion problem. 
            if isempty(mcinfo)
                mcinfo = self.mcinfo;
            end
			switch self.data.name(1:3)
			    case 'syn'
					self.L = struct('Lr', self.data.graph.L, 'Lc',...
                                    sparse(self.data.dims(2),self.data.dims(2),0));
                    self.L.time_buildLmat = 0;
                    self.opts_Lmat.fromSideinfo = true; 
                % all other cases => real-world data:
			    otherwise % real data such as 'traffic, MovieLens'
                    table_opts = struct2table(Tester.parseArgs('buildLmat', varargin{:}));
                    i_method = find(table_opts(:,'willUse').willUse);
                    opts = table2struct(table_opts(i_method,:));
                    if isfield(self.data, 'SIDEINFO') 
                    % note: This is the idea of constructing graph Laplacian
                    %       matrices from "side information" [Rao et al. 2015].
                    %       Attention: "SIDEINFO" is *not* available for datasets
                    %       currently available to us. Collecting SIDEINFO can be
                    %       a time consuming data science task, python packages
                    %       such as pandas, spark are often used (e.g. NYC taxi
                    %       data) to extract datasets online and then processing
                    %       the dataset properties to build side information. No
                    %       precise knowledge available yet. This procedure is
                    %       *not* available in the code 
                    %       [exp-grmf-nips15](https://github.com/rofuyu/exp-grmf-nips15)
                    %       by [Rao et al. 2015] but only described in the paper.
                    %       => Hence this case is not active for now.
                        feat_r = data.SIDEINFO.feat_r;
                        feat_c = data.SIDEINFO.feat_c;
                    elseif opts.fromSideinfo 
                        hatM0 = self.data.mat;  
                        if issparse(hatM0)
                        % This is the case with MovieLens Datasets 
                            [U,S,V] = svds(hatM0, opts.hatM0_rank); 
                            feat_r = U*S;
                            feat_c = V*S;
                        else
                        % This is the case with the Traffic Dataset 
                            opts.hatM0_rank = -1; % this means hatM0 has the same rank as the original data matrix
                            feat_r = hatM0; 
                            feat_c = hatM0';
                        end
                    else
                    % Construct feature matrices from rank-r SVD of M0: 
                        hatM0 = sparse(double(mcinfo.I),...
                                         double(mcinfo.J),...
                                         mcinfo.PXtar,...
                                         mcinfo.size_M(1),...
                                         mcinfo.size_M(2)); 
                        [U, S, V] = svds(hatM0, opts.hatM0_rank); 
                        feat_r = U*S;
                        feat_c = V*S;
                    end
                    [self.L, t] = ...
                    Tester.buildLmat_fromData( struct('feat_r', feat_r, ...
                                                      'feat_c', feat_c,...  
                                                      'feat_samplrate', mcinfo.sampl_rate),...
                                                opts, varargin{:} );
                    % Include the time for building the graph
                    % Laplacian matrix. 
                    self.L.time_buildLmat = t;
                    self.opts_Lmat = opts; 
			end
        end
        function scores_sol = compute_MCscores_sol(self, X_sol, mcinfo)
        % OUTPUT: an array of size (K,1), K = |GRLRMC.KEYS_MCscores_|
        % note: the fields in X_sol depends on `solver/initialization()` 
            if isfield(X_sol,'G') 
                G = X_sol.G; 
                H = X_sol.H; 
            elseif isfield(X_sol, 'L')
                G = X_sol.L; 
                H = X_sol.R; 
            elseif isfield(X_sol, 'U')
                G = X_sol.U*Xsol.S; 
                H = X_sol.V; 
            end
            Xsol_tr = spmaskmult(G, H', mcinfo.I, mcinfo.J);
            Xsol_te = spmaskmult(G, H', mcinfo.Itest, mcinfo.Jtest);
            Xsol_all = [Xsol_tr; Xsol_te]; 

            sc_tr = compute_scores_(Xsol_tr-mcinfo.PXtar, mcinfo.PXtar);
            sc_te = compute_scores_(Xsol_te-mcinfo.PXtar_t, mcinfo.PXtar_t);
            sc = compute_scores_(Xsol_all-[mcinfo.PXtar; mcinfo.PXtar_t],...
                                          [mcinfo.PXtar; mcinfo.PXtar_t]);
            relErr = self.compute_resPerIter_relErr(sc(1), mcinfo);
            if any(isnan(self.data.mat_clean))
                X_clean = [mcinfo.PXtar;mcinfo.PXtar_t];            
                 
            else
                X_clean = [spmask(self.data.mat_clean, mcinfo.I, mcinfo.J);...
                           spmask(self.data.mat_clean, mcinfo.Itest,...
                                  mcinfo.Jtest)];            
            end
            SNR = self.computeSCORES_SNR([Xsol_tr; Xsol_te], X_clean);
            scores_sol = [sc; sc_tr; sc_te; relErr; SNR];
            
            function res = compute_scores_(Delta, Xtar)
            % From 1 to 5: RMSE, MSE, NRMSE, ND, MAPE
                MSE = mean(Delta(:).^2); temp2 = abs(Xtar(:));
                temp3 = abs(Delta(:));

                res = [sqrt(MSE);...
                        MSE;...
                        sqrt(MSE)/(mean(temp2));...
                        sum(temp3)/sum(temp2);...
                        mean(temp3./ temp2) ];
            end
        end
		function mcinfo = get_mcinfo(self, sampl_rate)
			if nargin < 2
                sampl_rate = self.params_pb.sampl_rate;
            end
            iarg 	= find(self.range_paramspb.sampl_rate == sampl_rate);
			if ~isempty(iarg)
				mcinfo 	= self.MCINFOS{iarg}; 
			else
				fprintf('Abort: the given sampling rate does not match any of the stored ones.\n');
			end
		end
        function ntotal = compute_ntotalTests(self)
        % Compute the total nb of tests for a given experimental setting. 
        % INPUT: 
        %       (1) Number of sampling rates
        %       (2) Number of regularization parameter configurations
        %       (3) K-fold cross validation and number of repeated tests
        %       (4) Number of algorithms to test
        % OUTPUT:
        %       (1) Total nb of tests 
        %
        % Note: If an algorithm termintes in t1 (seconds) then the number of
        %       hours needed for testing this algorithm is: tt = @(t1, I,J,K,nt)
        %       t1*(I*(J*K+nt))/3600.0. Let 
        %       t1 = 24s~3200iters for Euclidean-CG and I=8,J=25,K=3,nt=20. Then
        %       tt = 5hrs for this one algorithm. 
            ntotal = numel(self.range_samplrates) *...
                     ( sum(self.info_methods(:,'doCrossval').doCrossval)*...
                        size(self.table_hyperparams,1)*self.opts_tester.crossval_K +...
                       sum(self.info_methods(:,'willRun').willRun)*...
                        self.opts_tester.n_repeatTests );
        end
        function mcinfos = crossval_genTrainvalsets(self, i_samplrate )
            K = self.opts_tester.crossval_K;
            for ii = 1 : K
                 mcinfos{ii} = ...
                 GRLRMC.genOMEGA_MC(self.data.mat, self.range_samplrates(i_samplrate));
                 sz_val = min(numel(mcinfos{ii}.Itest),...
                              floor(numel(mcinfos{ii}.I)/(K-1)));
                 mcinfos{ii}.Itest = mcinfos{ii}.Itest(1:sz_val);
                 mcinfos{ii}.Jtest = mcinfos{ii}.Jtest(1:sz_val);
                 mcinfos{ii}.PXtar_t = mcinfos{ii}.PXtar_t(1:sz_val);
            end
        end
        crossval_select(self, i_samplrate)
        crossval_runKfold(self, mcinfos_cv, table_paramspb)
        
        function stats_ = append_newstats(self, stats, newstats)
            if ~isempty(stats)
                time0 = stats(end).time;
                for s = 1 : numel(newstats)
                    newstats(s).time = newstats(s).time + time0;
                end
                stats_ = [stats, newstats];
            else
                stats_ = newstats;
            end
        end
        scoreByMethods = run_fromListMethods(self, mcinfo, table_paramspb,...   
                                            is_forCrossval, is_MC, varargin)
        run_repeatTests(self, table_paramspb)
        
        % ===================Multi-phase regularization=========================
        [table, opts] = gen_tableHp_multiphase(self, varargin)
        [table] = run_tester_multiphase(self, varargin)
        % ====================================================================== 

        run_tester_(self, tab_hp, varargin)
        [res_fixedp, res_multip, fu] = run_tester_2(self, tab_hp, varargin) 

        function run_tester(self, varargin)
            J = size(self.table_hyperparams, 1); 
            for i = 1 : length(self.range_samplrates)
                self.status_runTester.i_samplrate = i;
                table_paramspb = [array2table(repmat([i, self.range_samplrates(i)], [J,1]),...
                                        'VariableNames',{'pid', 'sampl_rate'}),...
                                  self.table_hyperparams];
                % L needs to be updated according to `self.MCINFOS{samplrate_i}` 
                self.refresh_Lmat(self.MCINFOS{i}, varargin{:});
                % Generate K different validation sets
                Kmcinfoi_cv = self.crossval_genTrainvalsets(i);
                if Kmcinfoi_cv{1}.sampl_rate ~= self.range_samplrates( i );
                    error('{Omega_k} for CV has a wrong sampling rate \n');
                end
                % Start evaluating cvscores for each of the |J| hyperparam settings.
                self.set_params_opt('maxiter', self.opts_tester.crossval_maxiter);
                % Included (j==1) for crossval, instead of j = 2 : J 
                for j = 1 : J 
                    self.status_runTester.j_params = j;
                    self.crossval_runKfold(Kmcinfoi_cv, table_paramspb);
                end

                self.crossval_select(i); 
                self.set_params_opt('tolgradnorm',...
                                    self.opts_tester.repeatTests_tolGrad);
                self.run_repeatTests(table_paramspb);
            end
        end 

        % -------- functions for visualization --------------
        function subplot_squared(self, hfig)
            % Extract axes handles of all subplots from the figure
            axesHandles = findobj(get(hfig,'Children'), 'flat','Type','axes');
            % Set the axis property to square
            axis(axesHandles,'square');
        end
        function h = plot_Lmat(self, do_save)
            if nargin < 2
                do_save = true;
            end
            h(1) = figure();
            subplot(121); imagesc(self.L.Lr);
            subplot(122); spy(self.L.Lr);
            self.subplot_squared(h(1));
            figname{1} = 'Lmat';
            if isfield(self.L, 'Wr')
                h(2) = figure();
                subplot(121); imagesc(self.L.Wr);
                subplot(122); spy(self.L.Wr);
                self.subplot_squared(h(2));
                figname{2} = 'Wmat';
            end
            for i = 1 : length(h)
                if do_save
                    saveas(h(i), sprintf('%s/%sfigure_%s', Tester.DIR_FIGS, self.gen_timeStr(), figname{i}));
                    saveas(h(i), sprintf('%s/%sfigure_%s', Tester.DIR_FIGS, self.gen_timeStr(),figname{i}),'epsc');
                end
            end
        end
        function [output, h] = plot_scores2dtable(self, name_z, do_save)
            if nargin < 2
                name_z = 'RMSE';
            end
            if nargin < 3
                do_save = false;
            end
            res = self.buildtable_fromEvals(name_z);

            xx = self.range_paramspb.sampl_rate;
            yy = self.range_paramspb.Lreg_betar;
            for i = 1 : length(res)
                zz.data = res{i}.table([1,8],:);
                %for j = 1 : size(res{i}.table,2)
                %    zz.data(2,j) = min(res{i}.table(2:end,j));
                %end
                zz.dataFormat = {'%.3e'};
               if do_save
                % latextable = latexTable(struct('data',[zz], 'dataFormat',{'%.3e'} ))
                latextable = latexTable(zz);
               end
             end
            output = struct(name_z, res);
        end
        function [output, h] = plot_scores2dmap(self, name_z, do_save)
            if nargin < 2
                name_z = 'RMSE';
            end
            if nargin < 3
                do_save = false;
            end
            res = self.buildtable_fromEvals(name_z);

            xx = self.range_paramspb.sampl_rate;
            yy = self.range_paramspb.Lreg_betar;
            for i = 1 : length(res)
                zz = -log10(res{i}.table) ;
                % zz = flipud( -log10(res_rmse{i}.table) );
                h(i) = figure(); 
                imagesc(xx, yy, zz); caxis([0,6]);  
                set(gca,'Ydir','reverse');
                xlabel('sample rate');
                ylabel('beta (graph-based reg.');
                methodname = res{i}.meta.name;
                methodname(methodname=='_')= '-';
                title(sprintf('RMSE scores: %s', methodname));
                colormap(gray);
                if do_save
                    saveas(h(i), sprintf('%s/%sfigure_%s%d', Tester.DIR_FIGS, self.gen_timeStr(),name_z, i));
                    saveas(h(i), sprintf('%s/%sfigure_%s%d', Tester.DIR_FIGS, self.gen_timeStr(),name_z, i), 'epsc');
                end
             end
            output = struct(name_z, res);
        end
        function [output, h] = plot_scoresVsSamplrate(self, name_z, do_save)
            if nargin < 2
                name_z = 'RMSE';
            end
            if nargin < 3
                do_save = false;
            end
            res = self.buildtable_fromEvals(name_z);

            xvals = self.range_paramspb.sampl_rate;
            % yy = self.range_paramspb.Lreg_betar;
            I1 = 1; I2 = 2 : numel(self.range_paramspb.Lreg_betar);
            h =  figure();
            % set(h,'DefaultAxesLineStyleOrder',{'+-.','o-','^-.','*-','-.','x-','s-.',...
            %        'd-','v-.','>-','<-.','p-','h-.'});
            flags = cell(1,2*length(res));
            hold all;
            for i = 1 : length(res)
                oid = res{i}.meta.name;
                oname = oid; oname(oname=='_') = '-';
                output.(oid).xvals = xvals;
                output.(oid).flags = {sprintf('%s (MC)', oname),sprintf('%s (GR-MC)', oname) };
                output.(oid).yvals(1,:) = res{i}.table(I1,:);
                for j = 1 : numel(xvals)
                    output.(oid).yvals(2,j) = min(res{i}.table(I2,j));
                end
                semilogy(xvals, output.(oid).yvals(1,:), '-.'); 
                semilogy(xvals, output.(oid).yvals(2,:)); 
                flags{2*i-1} = output.(oid).flags{1};
                flags{2*i} = output.(oid).flags{2};
            end
            xlabel('sample rate');
            ylabel(name_z); set(gca, 'Yscale','log');
            title(sprintf('%s vs sample rate', name_z));
            legend(flags,'Location', 'Southwest');
            axis([0, xvals(end), 1e-10, 1e0]);
            hold off;
            if do_save
                saveas(h, sprintf('%s/%sfigure_%sVSsamplrate', Tester.DIR_FIGS, self.gen_timeStr(),name_z));
                saveas(h, sprintf('%s/%sfigure_%sVSsamplrate', Tester.DIR_FIGS, self.gen_timeStr(),name_z),'epsc');
            end
        end
        function saveobj_2matfile(self)
            % use saveobj:
            pp = saveobj(self);
            filename = sprintf('%stests.mat',self.gen_timeStr() ); 
            save(filename, 'pp');
            clear pp;
        end
        function results = savetables_2matfile(self)
            results = struct('evals', self.table_evals, 'paramspb', self.table_paramspb,...
                            'optlog', self.tables_optlog, 'data', self.data, 'mcinfos', {self.MCINFOS});
            save(sprintf('%s_results.mat',self.gen_timeStr()), 'results');
            
        end
        
	end
end

