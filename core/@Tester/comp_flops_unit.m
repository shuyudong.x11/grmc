function fu = comp_flops_unit(pb, SR) 

m = pb.data.dims(1);
n = pb.data.dims(2);
r = pb.params_pb.rank;
mn = m*n;
% all quantities are expressed in (m*n flops)
nzo     = round(SR*m*n)/mn;
nzor    = round(SR*m*n)*r/mn;
nnzlr   = (nnz(pb.L.Lr)+ nnz(pb.L.Lc)+ m+n)*r/mn;
mr  = m * r /mn;
nr  = n * r /mn;
mr2 = m*r*r /mn;
nr2 = n*r*r /mn;
cr3  = r^3/3 /mn;
elems = [nzor, nnzlr, nzo, mr2, nr2, mr, nr, cr3]; 

% coefficients
cGRALS_nzor = 2 ;
cGRALS_nnzlr= 2 ;
cGRALS_mr   = 10 ;
cGRALS_nr   = 10 ;

cRCG_linemin  = [12, 6, 12, 12, 12, 22, 22, 2] ;
cRSD_linemin  = [12, 6, 12, 4,  4,  7,  7, 2] ;
cESD_linemin  = [12, 6, 12, 0,  0,  7,  7, 0] ;
cECG_linemin  = [12, 6, 12, 0,  0,  20,  20, 0] ;
cRSD_one  = nan; %[12, 6, 12, 4,  4,  7,  7, 2] ;

cFeval_a = [2, 2, 3, 0, 0, 2, 2, 0];
cFeval_b = [2, 2, 3, 0, 0, 2, 2, 0];

fu = struct('elems', elems, 'unit', mn,...
            'nzor', nzor, ...
            'nnzlr', nnzlr, 'nzo', nzo, ...
            'mr2', mr2, 'mr', mr, 'nr',nr,...
            'cGRALS_nzor', cGRALS_nzor,...
            'cGRALS_nnzlr', cGRALS_nnzlr,...
            'cGRALS_mr', cGRALS_mr,...
            'cGRALS_nr', cGRALS_nr,...
            'cRSD', cRSD_linemin,...
            'cRCG', cRCG_linemin,...
            'cESD', cESD_linemin,...
            'cECG', cECG_linemin,...
            'cFeval_a', cFeval_a,...
            'cFeval_b', cFeval_b);

end
