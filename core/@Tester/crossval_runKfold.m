function crossval_runKfold(self, mcinfos_cv, table_paramspb)
        % INPUT: 
        % (1) j_hyperparam: index of the hyperparam set in table_hyperparams.
        % (2) mcinfos_cv: a cell array of K structs of type "struct('I',_,'J',_,'Itest',_...)" like 
        %     "self.MCINFOS{1}"
        % OUTPUT: 
        % (1) self.storeCV_3dmat(status_runTester.i_samplrate, ~.j_hyperparams, 1:|i_methods|)
            self.set_params_opt('maxiter', self.opts_tester.crossval_maxiter);
            fav = str2func(self.opts_tester.crossval_average4CVscore);
            K = self.opts_tester.crossval_K;
            i_samplrate = self.status_runTester.i_samplrate;
            j_params = self.status_runTester.j_params;
            % scores_val (on val (named "I,J_test") entries): a 2d array of size( K x 
            % |n_doCVmethods|);
            scores_val = nan(K, sum(self.info_methods.doCrossval));

            for ii = 1 : K
                scores_val(ii,:) = self.run_fromListMethods(mcinfos_cv{ii}, table_paramspb, true);
                % the last input argument indicates whether this `is_forCrossval`
            end
            self.storeCV_3dmat(i_samplrate,:, j_params) = fav(scores_val, 1); 
            % => a 1d array of size (1x|n_activemethods|)
end

