function crossval_select(self, i_samplrate)
    % INPUT: 
    %  (1) storeCV_3dmat: a 3d matrix of size
    %      |samplrates|x|CVmethods|x|hyperparams|. Example: 
    %      storeCV_3dmat(1,2,20) is the cv score of the parameter setting
    %      "20" of the CVed method "2". The 1st set of parameters is always
    %      a all-zero valued vector, see function `Tester/gen_tableHyperparams()`.
    % OUTPUT:
    %  (1) info_methods: column 'jstar_params', 'cvscore'.
    %  (2) resultsCV_opByMethods: one row containing the chosen
    %      paramter setting and the corresponding CV score (RMSE test).

    % (a) find the methods CVed:
    ids_doCV = find(self.info_methods(:,'doCrossval').doCrossval);
    % (b) find the rest of active methods (the ones that are active but not CVed):
    ids_rest = find(self.info_methods(:,'willRun').willRun - ...
                    self.info_methods(:,'doCrossval').doCrossval);
    rg_gagnostic  = find(self.table_hyperparams.Lreg_betar==0);
    for ii  = 1 : length(ids_doCV) 
        % note (#114.b): included parameset(j=1) for crossval.
        [score, jstar] = min(self.storeCV_3dmat(i_samplrate, ii, 1:end));

        [scoreb, jstarb] = min(self.storeCV_3dmat(i_samplrate, ii, rg_gagnostic));
        if isnan(score)
            % throw error:
            error('Cross-validation score for method: %s is invalid (NaN). \n', ...
                  self.info_methods(ids_doCV(ii),'name').name{1});
        end
        self.info_methods(ids_doCV(ii),'cvscore').cvscore = score;
        self.info_methods(ids_doCV(ii),'jstar_params').jstar_params = jstar; 
        self.resultsCV_opByMethods(i_samplrate, ids_doCV(ii), :) = ...
             [table2array(self.table_hyperparams( jstar, : )), score];
        % (todo): 1. add property `resultsCVgagno_opByMehtods`; 2. add
        % variablename `{cvscore,jstar_params}_gagno` to `info_methods`
        % self.info_methods(ids_doCV(ii),'cvscore_gagno').cvscore = scoreb;
        self.info_methods(ids_doCV(ii),:).cvscore_gagno = scoreb;
        %self.info_methods(ids_doCV(ii),'jstar_params_gagno').jstar_params =...
        self.info_methods(ids_doCV(ii),:).jstar_params_gagno =...
                                                    rg_gagnostic(jstarb); 
        self.resultsCVgagno_opByMethods(i_samplrate, ids_doCV(ii), :) = ...
             [table2array(self.table_hyperparams( jstar, : )), score];
        for jj = 1 : length(ids_rest)
            % note (#1202): the following line depends on how `name_man` is produced 
            % according to the naming of each method, c.f. func:parse_manalgo_name()`
            if ...
                strcmp(self.info_methods(ids_doCV(ii),'name_man').name_man,...
                       self.info_methods(ids_rest(jj),'name_man').name_man) 
                % && ~(self.info_methods(ids_rest(jj),'jstar_params').jstar_params)
                % copy cols 'cvscore, jstar*, alpha_{r,c},gamma_{r,c}'
                self.info_methods(ids_rest(jj),'cvscore').cvscore = nan;
                self.info_methods(ids_rest(jj),'jstar_params').jstar_params = jstar;
                self.resultsCV_opByMethods(i_samplrate, ids_rest(jj),:) = ...
                            [table2array(self.table_hyperparams( jstar, : )), score];
            end
        end
    end
end

