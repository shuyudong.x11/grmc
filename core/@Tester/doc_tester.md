This is a documentation file for the classes Tester.m, GRLRMC.m and Solver.m.


### class GRLRMC
DEFAULT_resPerIter_labelnames: 
  {'niter', 'time', 'RMSE_t',...
  'RMSE', 'RMSE_tr', 'relErr', ...
  'gradnorm', 'cost', 'ncgPerI',...
  'fevalsPerI', 'flopsAccu'};

### class Tester
`get_mcscores_sr_hp_meth_te()`: a function to get MC scores for each sampling rate (SR), parameter setting (HP), method (meth) and test (te), from the properties `resPerI_{fixed,multi}p_srByHpByTeBySctype`. 

`produce_txtTesterId.m`: a function to produce text files containing key information about the current tester 
  - data
  - SRs
  - HPs
  - methods
to identify the experiment. This function is called by `Tester/produce_fig*.m`. 

`save_res2matfile.m`: a function to save results into a mat-file. 



#### Build and then feed graph Laplacians to GRLRMC
**Case 1: synthetic data** 
*At instantiation*: 
1. Tester/Tester()=>
2. feed_Lmat: get self.data.graph -> Lself.L 

**Case 2: real data without side information**
*At instantiation*: 
1. Tester/Tester() => 
2. feed_Lmat: build (feat_r,feat_c) from mcinfo => 
3. buildLmat_fromData() -> self.L

*When mcinfo changes*: 
1. Tester/run_tester_real(): if mcinfo changes =>  
2. refresh_Lmat => 
3. feed_Lmat: same as step 2. above 

**Case 3: real data with side information** 
*At instantiation*: 
1. Tester/Tester()=>
2. feed_Lmat: get (feat_r,feat_c) from self.data.SIDEINFO => 
3. buildLmat_fromData() -> self.L 

### Datasets
1. MovieLens100k
2. Traffic

3. Jester-1, [details here](https://goldberg.berkeley.edu/jester-data/). 
 - note: jester_1.mat contains the transpose of jester-1, that is, a data matrix of size 100x23983. A graph Laplacian is built for the dimension of "jokes" (instead of "users"), of size 100x100. 

### Experimental results
Table
- Tester/produce_tabsMCscore_meth_model(pp, name_sc). For each sampling rate, a table of method by problem model is produced. 

