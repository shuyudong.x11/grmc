function curves = extract_resPerI_(self, i_samplrate, i_hp_fix, i_hp_mul, i_te, xIsTime, name_stats)
% Extract from returned by Tester/run_tester_.m. The name of stats recorded per
% iteration is by default the folllowing.  
% DEFAULT_resPerIter_labelnames = {'niter', 'time', 'RMSE_t',...
%                                 'RMSE', 'RMSE_tr', 'relErr', ...
%                                 'gradnorm', 'cost'};
% INPUT:
% (1) resPerI_{fixedp,multip}_srByHpByTeByMethod, 
%      where each cell(`resultsPerIter`) contains a 2d array of size [6
%      x niter]. The 6 row indices correspond to {niter, time, rmse_t, rmse, relErr,
%      gradnorm}; for `method=GRALS`, each cell contains a 2d array of
%      size [3x niter], each row index is {niter, time, rmse_t} respectively.
% OUTPUT:
%      (1) curves: 1d cell array of size 1x|methods_willRun|. 
   if nargin < 6
       name_stats = 'RMSE_t';
   end
   rowid  = GRLRMC.get_rowid_resPerIter(name_stats);
   switch name_stats
       case 'cost'
           labely = 'Objective function';
       case 'RMSE'
           labely = 'RMSE';
       case 'RMSE_tr'
           labely = 'RMSE';
       case 'RMSE_t'
           labely = 'RMSE';
       case 'relErr'
           labely = 'Relative Error';
       case 'gradnorm'
           labely = 'Gradient norm';
   end
   
   sources_mul = self.resPerI_multip_srByHpByTeByMethod ; 
   sources_fix = self.resPerI_fixedp_srByHpByTeByMethod ; 
   % (#506.todo) p2: mark x-axis by the number of epochs run through
   % the whole data. For our algorithms, 1 iteration corresponds to 1
   % epoch (computing S= P_\Omega( GH' - M ), which is the data-related
   % term when computing the gradient). For GRALS, 1 iteration consists
   % of 2xn epochs in maximum (to verify if there is early stop when
   % a certain tol_cg is obtained), where n is the `maxiter_cg` for each of 
   % the two alternating inner-loops. 
   if xIsTime
       labelx = 'Time (Sec.)';
       rowid_x = 2;
   else
       labelx = 'Iteration';
       rowid_x = 1;
   end
   % Ntotal = self.opts_tester.n_repeatTests *...
   % self.opts_tester.repeatTests_maxiter; %% depending on the choice of id_Te: in case it is 
   n_tests = size(sources_mul, 3); 
   switch i_te
   %% - 'all': return the curves all the repeated tests, 
   %% - '1': the curve of the 1st test,
   %% - 'random': a randomly chosen one from the n_repeatedTests, 
   %% - 'mean': a curve built from the result of a regression of all
   %%           curves  
   %% ==> id_te = a range of indices or a single index 
       case 'all'
            id_te = 1 : n_tests; 
       case '1'
            id_te = 1; 
       case 'random'
            id_te = randsample(n_tests,1); 
       otherwise
            error('The option for i_te in extract_resPerI_() is not valid'); 
   end
        
   %% In all cases, every curve is uniquely identified by its field `methodname`: 
   curves = struct('labelx', nan, 'labely',nan,...
                   'x', nan, 'y', nan,...
                   'methodname', ...
                   'null' );    
   ids_m_fix = find(self.info_methods.willRun);
   ids_m_mul = find(self.info_methods.willRun_multiphase); 

   for ite = 1 : numel(id_te)
       for im = 1 : numel(ids_m_fix)
           res_fix = sources_fix{i_samplrate,i_hp_fix,id_te(ite),im};
           
           y = res_fix(rowid,:);
           x = res_fix(rowid_x,:);
           curves(end+1) = struct( 'labelx', labelx, 'labely',labely,...
                                   'x', x, 'y', y,...
                                   'methodname', ...
                                   Tester.render_plotableMethodname(self, ids_m_fix(im)) );    
       end
       for im = 1 : numel(ids_m_mul)
           res_mul = sources_mul{i_samplrate,i_hp_mul,id_te(ite),im};
           
           y = res_mul(rowid,:);
           x = res_mul(rowid_x,:);
           curves(end+1) = struct( 'labelx', labelx, 'labely',labely,...
                                   'x', x, 'y', y,...
                                   'methodname', sprintf('%s (Mul)', ...
                                   Tester.render_plotableMethodname(self, ids_m_mul(im))) );
       end
   end
end

