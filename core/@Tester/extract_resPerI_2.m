
function curves = extract_resPerI_2(pp, i_samplrate, ihp_fix, ...
                                 ihp_mul, i_te, x_type, name_stats)
% Extract from returned by Tester/run_tester_.m. The name of stats recorded per
% iteration is by default the folllowing.  
% DEFAULT_resPerIter_labelnames = {'niter', 'time', 'RMSE_t',...
%                                 'RMSE', 'RMSE_tr', 'relErr', ...
%                                 'gradnorm', 'cost'};
% INPUT:
% (1) resPerI_{fixedp,multip}_srByHpByTeByMethod, 
%      where each cell(`resultsPerIter`) contains a 2d array of size [6
%      x niter]. The 6 row indices correspond to {niter, time, rmse_t, rmse, relErr,
%      gradnorm}; for `method=GRALS`, each cell contains a 2d array of
%      size [3x niter], each row index is {niter, time, rmse_t} respectively.
% (2) i_samplrate: index of the sampling rate
% (3) ihp_fix: an 1d array of indices of parameter sets in the table_hp 
% (4) ihp_mul: an 1d array of indices of parameter sets in the table_hp 
% OUTPUT:
%      (1) curves: 1d cell array of size 1x|methods_willRun|. 
   if nargin < 6
       x_type = 'time';
   end
   if nargin < 7
       name_stats = 'RMSE_t';
   end
   len_ihpfix = sum(pp.info_methods.willRun); 
   len_ihpmul = sum(pp.info_methods.willRun_multiphase); 
   if numel(ihp_fix) == 1 
       % When this happens, it is only possible when extract_resPerI_2.m is called
       % directly by the user, instead of any class function calling it: 
       ihp_fix = repmat(ihp_fix, [1, len_ihpfix]);
       ihp_mul = repmat(ihp_mul, [1, len_ihpmul]);
   end
   rowid  = GRLRMC.get_rowid_resPerIter(name_stats);

   switch name_stats
       case 'cost'
           labely = 'Objective function';
           labelst = 'function value';
       case 'RMSE'
           labely = 'RMSE';
           labelst = 'RMSE';
       case 'RMSE_tr'
           labely = 'RMSE';
           labelst = 'RMSE (Train)';
       case 'RMSE_t'
           labely = 'RMSE';
           labelst = 'RMSE (Test)';
       case 'relErr'
           labely = 'Relative Error';
           labelst = 'relErr';
       case 'gradnorm'
           labelst = 'Gradient Norm';
           labely = 'Gradient norm';
   end
   
   sources_mul = pp.resPerI_multip_srByHpByTeByMethod ; 
   sources_fix = pp.resPerI_fixedp_srByHpByTeByMethod ; 
   % In all cases, every curve is uniquely identified by its field `methodname`: 
   curves = struct('labelx', nan, 'labely',nan,...
                   'x', nan, 'y', nan,...
                   'statsname', 'null',...
                   'methodname', 'null' );    
   ids_m_fix = find(pp.info_methods.willRun);
   ids_m_mul = find(pp.info_methods.willRun_multiphase); 
   switch i_te
       case {'all', 'mean'}  
       % Return the curves all the repeated tests, 
            id_te = 1 : n_tests; 
       case '1'
       % Return the curve of the 1st test,
            id_te = 1; 
       case 'random' 
       % Not used. This type is essentially equivalent to '1' when n_tests > 1.
            id_te = randsample(n_tests,1); 
       otherwise
            error('The option for i_te in extract_resPerI_() is not valid'); 
   end
   % (#506.todo) p2: mark x-axis by the number of epochs run through
   % the whole data. For our algorithms, 1 iteration corresponds to 1
   % epoch (computing S= P_\Omega( GH' - M ), which is the data-related
   % term when computing the gradient). For GRALS, 1 iteration consists
   % of 2xn epochs in maximum (to verify if there is early stop when
   % a certain tol_cg is obtained), where n is the `maxiter_cg` for each of 
   % the two alternating inner-loops. 
   switch x_type
   case 'flopsCum'
       labelx = 'Computational cost ((mn) FLOPS)';
       rowid_x = GRLRMC.get_rowid_resPerIter('flopsCum'); 
       converter_ours = @(values) values ; % the converter is not needed, hence is identity
       converter_grals = @(values) values; 
   case 'time'
       labelx = 'Time (Sec.)';
       rowid_x = 2;
       converter_ours = @(values) values ; 
       converter_grals = @(values) values; 
      t0 = [];
      for im = 1 : numel(ids_m_fix)
           res_fix = sources_fix{i_samplrate,1,id_te(1),im};           
           t0 = [t0; res_fix(rowid_x,1)];
      end
      time0 = mean(t0); 
   case 'iter'
       labelx = 'Iteration';
       rowid_x = 1;
       converter_ours = @(values) values ; 
       converter_grals = @(values) values; 
   case 'epoch'
       % This is the only case where converter is not identity. 
       % In this case, our algorithms and GRALS do not have
       % the same converter, since GRALS has inner loops, each inner
       % loop corresponding to one epoch (one full pass over the data). 
       % For ours: #epoch = 2*(#iter - 1). 
       % For GRALS: #epoch = 2*n_CG * (#iter -1). This is certainly 
       % the case when the accuracy parameter in its LS subproblems, 
       % with input args "-e eps", is set by eps=0. 
       labelx = 'Epoch (Number of passes over data)';
       rowid_x = 1;
       converter_ours = @(values) 2*(values - 1); 
       converter_grals = @(values) 2*pp.params_opt.grmf_maxiter_cg *(values - 1); 
   end
   n_tests = size(sources_mul, 3); 



   for ite = 1 : numel(id_te)
       for im = 1 : numel(ids_m_fix)
           if strcmp(pp.info_methods(ids_m_fix(im),:).name_algo{1}(1:3), 'GRA')
               converter = converter_grals; 
           else
               converter = converter_ours;
           end
           res_fix = sources_fix{i_samplrate,1,id_te(ite),im};
           
           y = res_fix(rowid,:);
           x = converter( res_fix(rowid_x,:) ) ;
           if strcmp(x_type, 'time')
              x = x + time0 - x(1); % align the starting time (time before running iter=1).
           end 
           curves(end+1) = struct( 'labelx', labelx, 'labely',labely,...
                                   'x', x, 'y', y,...
                                   'statsname', labelst, ...
                                   'methodname', sprintf('%s (MC)', ...
                                   Tester.render_plotableMethodname(pp, ids_m_fix(im))) );    
           % Each method may have a different ihp_fix value, in any case it is
           % stored in ihp_fix(im), the length of ihp_fix is equal to
           % numel(ids_m_fix), which is the number of algorithms tested for
           % fixe-param GRMC. If the indices in ihp_fix are the same one, then
           % it means we return curves of different algorithms on the exactly
           % same GRMC problem, with the same parameter set. 
           res_fix = sources_fix{i_samplrate,ihp_fix(im),id_te(ite),im};
           
           y = res_fix(rowid,:);
           x = converter( res_fix(rowid_x,:) ) ;
           if strcmp(x_type, 'time')
              x = x + time0 - x(1); % align the starting time (time before running iter=1).
           end 
           curves(end+1) = struct( 'labelx', labelx, 'labely',labely,...
                                   'x', x, 'y', y,...
                                   'statsname', labelst, ...
                                   'methodname', ...
                                   Tester.render_plotableMethodname(pp, ids_m_fix(im)) );    
       end
       for im = 1 : numel(ids_m_mul)
           if strcmp(pp.info_methods(ids_m_mul(im),:).name_algo{1}(1:3), 'GRA')
               converter = converter_grals; 
           else
               converter = converter_ours;
           end
           res_mul = sources_mul{i_samplrate,ihp_mul(im),id_te(ite),im};
           
           y = res_mul(rowid,:);
           x = converter( res_mul(rowid_x,:) ) ;
           if strcmp(x_type, 'time')
              x = x + time0 - x(1); % align the starting time (time before running iter=1).
           end 
           % x = res_mul(rowid_x,:);
           curves(end+1) = struct( 'labelx', labelx, 'labely',labely,...
                                   'x', x, 'y', y,...
                                   'statsname', labelst, ...
                                   'methodname', sprintf('%s (Mul)', ...
                                   Tester.render_plotableMethodname(pp, ids_m_mul(im))) );
       end
   end
   curves = curves(2:end); 
end

