function tabs = extract_tabRecrVsSR(pp, name_sc )
% Produce one table of Recovery Error as a function of the
% sampling rate for the three types of problem models: MC, GRMC (Mul), and
% MMMF. 

% INPUT to produce one group of a triple of curves:  
%   (1) The method whose results are used to produce the curves. 

if nargin < 2
    name_sc = 'RMSE_t';
end
switch name_sc
    case 'RMSE'
        labely = 'RMSE';
    case 'RMSE_t'
        labely = 'RMSE';
    case 'relErr'
        labely = 'Relative Error';
end

%% constants  
ID_SC = pp.get_rowid_MCscores(name_sc); 
TYPES_PB = {'MC', 'GRMC', 'MMMF'}; 
% The following RANGE_ is the range of indices of the parameter settings
% corresponding to each of the three problem models. 
RANGE_IHP_byPB{1} = [1]; % paramter settings of MC is always (0,0,0), in the first row of the table_hyperparams.
RANGE_IHP_byPB{2} = find(pp.table_hyperparams(:,'Lreg_betar').Lreg_betar>0); % those parameter settings such that \beta_r > 0.  
RANGE_IHP_byPB{3} = find((pp.table_hyperparams(:,'Lreg_betar').Lreg_betar==0).*...
                         (pp.table_hyperparams(:,'alpha_r').alpha_r>0));

LEN_ALGOFIX = sum(pp.info_methods.willRun); 
IMS_ALGOFIX = find(pp.info_methods.willRun); 
LEN_ALGOMUL = sum(pp.info_methods.willRun_multiphase); 
IMS_ALGOMUL = find(pp.info_methods.willRun_multiphase); 

SCORES_F = Tester.get_mcscores_sr_hp_meth_te(pp, 'fixed', name_sc ); % pp.scores_fixedp_srByHpByMethodBySctype;
SCORES_M = Tester.get_mcscores_sr_hp_meth_te(pp, 'multi', name_sc ); % pp.scores_multip_srByHpByMethodBySctype;
SRs = pp.range_samplrates; 
sz1 = numel(SRs);
NTE = size(SCORES_F, 4); 

%% Select the best parameter for each pb (MC, GRMC, MMMF). 
tabs = [];
for i = 1 : numel(TYPES_PB)
    type_pb = TYPES_PB{i}; 
    range_ihp   = RANGE_IHP_byPB{i};
    sz2 = numel(range_ihp);
    for im = 1 : LEN_ALGOFIX
        % The index of the current algorithm in pp.info_methods is:
        % IMS_ALGOFIX(im).
        name_method = Tester.render_plotableMethodname(pp, IMS_ALGOFIX(im)); 
        %%%%%%% find out the best score (among all parameter settings)
        % The index of the current algorithm in SCORES_F is: im. 
        temp = reshape(SCORES_F(:, range_ihp, im, :), [sz1, sz2, NTE]); 
        sucr = zeros(sz1, sz2);
        for ii=1:sz1
            for jj=1:sz2
                sucr(ii,jj) = sum(temp(ii,jj,:) <= 1e-12)/NTE;
            end
        end
        sucr_bysr = max(sucr,[],2);
        % scs_f = median(temp, 3); 
        % [scs_bySR, ihps] = min(scs_f, [], 2); 
        
        tt = struct('x', SRs, 'y', sucr_bysr, ...
                    'labelx', 'Sampling Rate', 'labely', labely,...
                    'type_pb', type_pb,...
                    'statsname', name_sc,...
                    'name_method', name_method,...
                    'methodname', sprintf('%s (%s)',type_pb,name_method));

        tabs = [tabs; tt]; 
    end
    for im = 1 : LEN_ALGOMUL
        % The index of the current algorithm in pp.info_methods is:
        % IMS_ALGOFIX(im).
        name_method = Tester.render_plotableMethodname(pp, IMS_ALGOMUL(im)); 
        %%%%%%% #614.todo: to modify 
        % The index of the current algorithm in SCORES_F is: im. 
        temp = reshape(SCORES_M(:, range_ihp, im, :), [sz1, sz2, NTE]); 
        sucr2 = zeros(sz1, sz2);
        for ii=1:sz1
            for jj=1:sz2
                sucr2(ii,jj) = sum(temp(ii,jj,:) <= 1e-12)/NTE;
            end
        end
        sucr2_bysr = max(sucr2,[],2);
        % scs_m = median(temp, 3); 
        % [scs_bySR, ihps] = min(scs_m, [], 2); 
        %%%%%%% #614.todo: to modify 
        tt = struct('x', SRs, 'y', sucr2_bysr, ...
                    'labelx', 'Sampling Rate', 'labely', labely,...
                    'type_pb', type_pb,...
                    'statsname', name_sc,...
                    'name_method', sprintf('%s (Mul)', name_method),...
                    'methodname', sprintf('%s (%s (Mul))',type_pb, name_method));

        tabs = [tabs; tt]; 
    end
end


end






