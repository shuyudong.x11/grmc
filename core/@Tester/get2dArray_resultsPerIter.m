function [array2d, Fu] = get2dArray_resultsPerIter(pb, stats, i_sr, rid_im)
% INPUT:
% 1) stats, i_sr: index of the current SR, rid_im: index of the current method.
% 2) Information in pb.
%    Solver.NAMES_ALGO:
%    {'GRALS'           }
%    {'GRALS1'          }
%    {'GRALS2'          }
%    {'RSD_MANOPT'      }
%    {'RCG_MANOPT'      }
%    {'RCGprecon_MANOPT'}
%    {'RSD'             }
%    {'RCG'             }
%    DEFAULT_resPerIter_labelnames includes by order of row index 
%      {'niter', 'time', 'RMSE_t',...
%      'RMSE', 'RMSE_tr', 'relErr', ...
%      'gradnorm', 'cost', 'ncgPerI',...
%      'fevalsPerI', 'flopsCum'};
% OUTPUT:
% 1) a 2d array of size |resPerIter_labelnames|x niters.

   n_feat = length(GRLRMC.DEFAULT_resPerIter_labelnames) ; 
   rid_flops        = pb.get_rowid_resPerIter('flopsCum');
   rid_fevalsPerI   = pb.get_rowid_resPerIter('fevalsPerI');
   rid_ncgGPerI      = pb.get_rowid_resPerIter('ncgGPerI');
   rid_ncgHPerI      = pb.get_rowid_resPerIter('ncgHPerI');
   if size(stats, 2) > 1
       % An array of structs of size 1xniter. This is returned by Manopt solvers
       len = numel(stats);
       array2d = nan(n_feat, len); 
       % Initialize 'fevalsPerI' with 0.
       array2d(rid_fevalsPerI,1:len) = 0;
       for i = 2 : n_feat
           key = GRLRMC.DEFAULT_resPerIter_labelnames{i}; 
           if isfield(stats(end), key)
               array2d(i,:) = [stats.(key)];
           else
               switch key
               case 'fevalsPerI'
                   for t=2:len
                   if ~isempty(stats(t).linesearch)
                       array2d(i,t) = stats(t).linesearch.costevals; 
                       % this is the number of times the objective function is evaluated.
                   end
                   end
               end
           end
       end
       % Number of iterations finished at each iterate. 
       array2d(1,:) = [1:len]-1; % 'niter'
       % Compute accumulative flops counts based on constants (m,n,r,SR,L) 
       % and 1 variable (fevalsPerI, which are 0s when the algo is lsFree).
       if ~isfield(stats, 'ncgGPerI')
           [array2d(rid_flops,:), Fu] = comp_flopsCum(pb, array2d(rid_fevalsPerI,:), i_sr, rid_im);
       else
           % This is when stats is produced by the method AltMin in
           % @Solver/solve_AltMin.m. 
           [array2d(rid_flops,:), Fu] = comp_flopsCum(pb, array2d([rid_ncgGPerI,rid_ncgHPerI],:), i_sr, rid_im);
       end
   else
       % This is returned by runGRMF.
       % The actual length of the stats can be smaller than the maxiter budget set in runGRMF, and it can be 
       % obtained by finding the index of the last non-zero in RMSE_t(iter). 
       
       % len = numel(stats.time);
       len = max(find(stats.RMSE_t)); 
       array2d = nan(n_feat, len); 
       % Initialize 'ncgG/HPerI' with 0.
       array2d(rid_ncgGPerI,1:len) = 1;
       array2d(rid_ncgHPerI,1:len) = 1;
       for i = 2 : n_feat
           key = GRLRMC.DEFAULT_resPerIter_labelnames{i}; 
           if isfield(stats, key)
               array2d(i,:) = stats.(key)(1:len);
           end
       end
       % Number of iterations finished at each iterate. 
       array2d(1,:) = [1:len]-1;
       % Compute accumulative flops counts based on 
       % constants (m,n,r,SR,L) and 1 variable (ncgPerI).
       [array2d(rid_flops,:), Fu] = comp_flopsCum(pb, array2d([rid_ncgGPerI,rid_ncgHPerI],:), i_sr, rid_im);

   end
   if isfield(pb.L, 'time_buildLmat') 
       rowid_time = GRLRMC.get_rowid_resPerIter('time'); 
       array2d(rowid_time, :) = array2d(rowid_time,:) +...
                                pb.L.time_buildLmat ;
   end

   function [flopsCum, Fu] = comp_flopsCum(pb, varPerI, i_sr, rid_im)
   % Compute the accumulative flop counts required until the iteration t, for t=0,1,..
   % INPUT:
   % (3) rid_im: the row index of the current method in the table pb.info_methods.
   % OUTPUT:
   % (1) flopsCum. 
        len = size(varPerI,2); % note: the number of completed iterations niter = len-1.
        cPerI = [1:len]-1; % nb of completed iterations
        
        % Compute the numerical constants in the formula of flops counts required by each unit iteration for GRALS, RSD, RCG. 
        Fu = Tester.comp_flops_unit(pb, pb.range_samplrates(i_sr));
        
        % Find out costs for evaluating once the obj function, this may not add
        % to the final flopsCum, if fevalsPerI are 0s. 
        name_man = pb.info_methods(rid_im,:).name_man{1} ; 
        if strcmp(name_man(end-2:end),'Exa')
        % Although problem 8a and 8b have different feval costs, they have 
        % the exact same dominat terms.
            Ffeval = Fu.cFeval_b*Fu.elems';
        else
            Ffeval = Fu.cFeval_a*Fu.elems';
        end
        switch pb.info_methods(rid_im,:).name_algo{1}
            case {'GRALS', 'GRALS1', 'GRALS2', 'AltMin1', 'AltMin2'}
                % This corresponds to the flopsCum of GRALS. 
                % Let F_GRALS be the number of flops required for each CG iteration. 
                % F_GRALS is a constant that is defined in Eq.(xx) in paper-grmc. 
                % Each ALS iteration takes ncgPerIx F_unit flops, see Eq.(xx). And
                % flopsCum = cumsum(ncgPerI) * F_GRALS. Here the cumsum is defined as 
                % cumsum(ncgPerI)[t] = sum(ncgPerI(1:t)), for t=1,.., length-of-varPerI. 
                cumG = cumsum(varPerI(1,:)) ; 
                cumH = cumsum(varPerI(2,:)) ; 
                cumCoef_nzor = Fu.cGRALS_nzor*(cumG + cumH + cPerI);
                cumCoef_nnzlr = Fu.cGRALS_nnzlr*(cumG + cumH);
                cumCoef_mr  =  Fu.cGRALS_mr*(cumG);
                cumCoef_nr  =  Fu.cGRALS_nr*(cumH);
                flopsCum = cumCoef_nzor * Fu.nzor + ...
                           cumCoef_nnzlr * Fu.nnzlr +...
                           cumCoef_mr * Fu.mr +...
                           cumCoef_nr * Fu.nr; 
            case {'RCG_MANOPT','RCG'} 
                switch pb.info_methods(rid_im,:).name_man{1}
                case {'GH_Euc','GH_EUC'} 
                    Fu_const = Fu.elems*Fu.cECG'; 
                otherwise
                    Fu_const = Fu.elems*Fu.cRCG'; 
                end
                
                % varPerI is fevalsPerI, which are 0 if algo is lsFree.
                % Each iteration takes 1xF_unit + n_LSxF_feval flops, see Eq.(xx).
                flopsCum = Fu_const*cPerI+Ffeval*cumsum(varPerI); 
            case {'RSD_MANOPT', 'RSD'} 
                switch pb.info_methods(rid_im,:).name_man{1}
                case {'GH_Euc','GH_EUC'} 
                    Fu_const = Fu.elems*Fu.cESD'; 
                otherwise
                    Fu_const = Fu.elems*Fu.cRSD'; 
                end
                
                % varPerI contains values of fevalsPerI, which are 0 if algo is lsFree.
                % Each iteration takes 1xF_unit + n_LSxF_feval flops, see Eq.(xx).
                flopsCum = Fu_const*cPerI+Ffeval*cumsum(varPerI);
        end
   end

end

