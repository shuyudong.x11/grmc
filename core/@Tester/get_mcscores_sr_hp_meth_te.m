function output = get_mcscores_sr_hp_meth_te(pb, type_reg, name_sc)
    % Get a 4d array of MCscores by (SR, HP, METH, TE).
    % SR is a sampling rate;
    % HP is a parameter setting;
    % METH is method;
    % TE is one test with the above three indices fixed. 
    % INPUT: self.resPerI_*_srByHpByTeByMethod
    if nargin < 2
        type_reg = 'fixed';
    end
    if nargin < 3
        name_sc = 'RMSE_t'; % the other possibility is relErr 
    end
    switch type_reg 
        case 'fixed'
            resPerI = pb.resPerI_fixedp_srByHpByTeByMethod;
        otherwise
            resPerI = pb.resPerI_multip_srByHpByTeByMethod;
    end
    rowid = pb.get_rowid_resPerIter(name_sc);
    szs = size(resPerI); 
    output = nan(szs(1),szs(2), szs(4),szs(3)); 
    for i = 1 : szs(1)
        for j = 1 : szs(2)
            for t = 1 : szs(3)
                for im = 1 : szs(4)
                temp = resPerI{i,j,t,im}; 
                output(i,j,im,t) = temp(rowid,end); 
                end
            end
        end
    end
end

