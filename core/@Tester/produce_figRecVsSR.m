function [hh, tabs] = produce_figRecVsSR(pp, name_sc, type_y) 
% Produce one figure of Recovery Error as a function of the
% sampling rate for the three types of problem models: MC, GRMC (Mul), and
% MMMF. 

% INPUT to produce one group of a triple of curves:  
%   (1) The method whose results are used to produce the curves. 
%   (2) Option for name_sc: type of recovery score. Options: RMSE, RMSE_t,
%       relErr. 
%   (3) Option for type_y: this can be either sucess rate or recovery score
%       measured by RMSE or SNR, depending on the type of experimental
%       setting.   

if nargin < 2
    name_sc = 'RMSE_t';
end
if nargin < 3
    type_y = 'score'; 
end
if ~isdir(Tester.DIR_FIGS)
    DIR_FIGS = '~/Dropbox/DBucl/temporary/matfiles';
else
    DIR_FIGS = Tester.DIR_FIGS;
end
switch type_y
case 'score'
    tabs = Tester.extract_tabRecVsSR(pp, name_sc);
    fidh = 'recVsSR';
otherwise
    % 'rsuccess' % rate of successful recoveries
    tabs = Tester.extract_tabRecrVsSR(pp, name_sc);
    fidh = 'sucrVsSR'; 
end
% Use Tester/plot_resultsPerIter(curves) to 
% plot curves with Y-axis in log-scale, each curve is a struct of fields:
% x, y, labelx, labely, methodname

hh = Tester.plot_resultsPerIter(tabs); 

if ~isprop(pp,'tid')
    tid = pp.gen_timeStr();
elseif isempty(pp.tid )
    tid = pp.gen_timeStr();
else
    tid= pp.tid; 
end
sid = sprintf('%s_%s_%s_%dSRs_%s', ...
              pp.data.name, fidh, name_sc, length(pp.range_samplrates),...
                       tid ); 
                       % pp.gen_timeStr() ) ; 
Tester.produce_saveFigures(sid); 
Tester.produce_txtTesterId(pp, sid);

end

