function [hh, cur] = produce_figTrVsTe(pp, type, i_te, x_type)
% Produce one figure for each sampling rate. Each figure contains curves of
% algorithms for solving the fixed-param GRMC and the multiphase GRMC problem. 

if nargin <2 
    type = 1; 
end
if nargin < 3
    i_te = '1'; 
end
if nargin < 4
    x_type = 'iter'; 
end
if ~isdir(Tester.DIR_FIGS)
    DIR_FIGS = '~/Dropbox/DBucl/temporary/matfiles';
else
    DIR_FIGS = Tester.DIR_FIGS;
end
id_sc = pp.get_rowid_MCscores('RMSE_t'); 

switch type
    case 1
        % This means we choose the parameter on which fixed-param GRMC
        % achieved the best score among all other parameter sets. 
        scores_f = pp.scores_fixedp_srByHpByMethodBySctype; 
        scores_m = scores_f; 

    case 2
        % This means we choose the parameter on which multiphase GRMC
        % achieved the best score among all other parameter sets. 
        scores_m = pp.scores_multip_srByHpByMethodBySctype; 
        scores_f = scores_m; 
    case 3
        scores_f = pp.scores_fixedp_srByHpByMethodBySctype; 
        scores_m = pp.scores_multip_srByHpByMethodBySctype; 
end
% The lengths of (ihp_fix, ihp_mul) are the number of algorithms tested for
% fixed-param GRMC and multiphase GRMC respectively.
len_ihpfix = size(scores_f,3); 
len_ihpmul = size(scores_m,3); 
len_tabhp = size(scores_f,2) ; % this is equal to the number of parameter settings

% Close all possible previous figure handles since the
% subfunction produce_saveFigures() saves all figure handles later. 
close all; 
cur = [];
for i = 1 : length(pp.range_samplrates)
    % Find out (ihp_fix, ihp_mul).  
    % The parameter selection excludes ihp=1, the totally non-regularized
    % problem, since it will always be collected and shown in figures. 
    sc_f = reshape(scores_f(i,2:end,:,id_sc), [len_tabhp-1,len_ihpfix]);
    sc_m = reshape(scores_m(i,2:end,:,id_sc), [len_tabhp-1,len_ihpmul]);
    [~, ihps_fix] = min(sc_f, [], 1 ) ; 
    [~, ihps_mul] = min(sc_m, [], 1 ) ; 
    % The results are ihps_{fix,mul}+1, since the ihp=1 was excluded in the min
    % function: 
    if type~= 3 
        ihp_fix = majority_vote(ihps_fix+1)*ones(1,len_ihpfix); 
        ihp_mul = majority_vote(ihps_mul+1)*ones(1,len_ihpmul); 
    else
        ihp_fix = ihps_fix + 1;
        ihp_mul = ihps_mul + 1; 
    end
    if ~isempty(ihp_fix)
        pp.selecHP_fixedp_srByMethod(i,:) = ihp_fix; 
    end
    if ~isempty(ihp_mul)
        pp.selecHP_multip_srByMethod(i,:) = ihp_mul; 
    end
    % Note: produce_resPerI_.m returns 1 figure for whatever lengths of ihp_fix,
    % ihp_mul are, and the figure contains all algortihms tested. 
    [h, cu] = Tester.produce_resPerI_TrVsTe(pp, i, ihp_fix, ihp_mul, x_type, i_te); 
    hh{i} = h;
    cur = [cur; cu]; 
end

if ~isprop(pp,'tid')
    tid = pp.gen_timeStr();
elseif isempty(pp.tid )
    tid = pp.gen_timeStr();
else
    tid= pp.tid; 
end
sid = sprintf('%s_resPerI%s_TrVsTe_%dSRs_%s_selHP%d', ...
               pp.data.name, x_type, length(pp.range_samplrates),...
               tid, type) ; 

Tester.produce_saveFigures(sid); 
Tester.produce_txtTesterId(pp, sid);

    function str = array2string(ihps)
       str = '' ; 
       for l = 1 : numel(ihps)
           str = [str, int2str(ihps(l))]; 
       end
    end
    function iarg = majority_vote(iargs)
        % INPUT: 
        % (1) iargs: an array of 1xn, where n is the number of algorithms tested, each
        %     has a favorate index of the parameter set. 
        % 
        uu = unique(iargs); 
        if ~isempty(uu)
            for l = 1 : numel(uu)
                oc(l) = sum(iargs == uu(l)); 
            end
            [~, ioc] = max(oc); 
            iarg = uu(ioc); 
        else
            iarg = 1;
        end
    end

end

