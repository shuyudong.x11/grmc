function [h, cur ] = produce_resPerI_(pp, i_sr, ihp_fix, ihp_mul, name_stats, x_type,  i_te) 
% Produce results per iteration for fixedparam and multiphase methods. 
% For each sampling rate, produce one figure containing curves by the 
% algorithms tested. 
% INPUT:
% (1) ihp_fix: index of the parameter set in table_Hyperparams for fixed-param GRMC. 
% (2) ihp_mul: index of the parameter set in table_Hyperparams for multiphase GRMC. 
% (3) name_stats: name of the per-iteration stats, which is one of the
%     following,
%     - RMSEt:      RMSE score on test entries of X(iter) 
%     - gradnorm:   Norm of the gradient of f at X(iter) 
%     - RMSE:       RMSE score on all entries of X(iter) 
%     - RMSEtr:     RMSE score on revealed entries of X(iter) 
%     - fobj:       Value of f(X(t)) 
% OUTPUT:
% (1) h: array of figure handles, each figure handle presents results of one
%     sampling rate.  
% (2) cur: 1d cell array of size  

   if nargin < 5
       name_stats = 'RMSE_t';
   end
   if nargin < 6
       x_type = 'iter'; % other possible options: time, epoch. 
   end
   if nargin < 7
       i_te = '1'; 
   end
   if ~isdir(Tester.DIR_FIGS)
       DIR_FIGS = '~/Dropbox/DBucl/temporary/matfiles';
   else
       DIR_FIGS = Tester.DIR_FIGS;
   end
   if i_sr(1) < 0
       i_sr = pp.range_samplrates;
   end
   % Convert the numbers in ihp_{fix,mul} into one single string
   str_fix = array2string(ihp_fix); 
   str_mul = array2string(ihp_mul); 
   for i = 1 : numel(i_sr)
        cur(i,:) = ...
        Tester.extract_resPerI_2(pp, i_sr(i), ihp_fix, ihp_mul, i_te, x_type, name_stats);
        % Depending on the type of i_te:
        switch i_te
        case {'1', 'random'} 
            h(i) = pp.plot_resultsPerIter(cur);
            % saveas( h(i), sprintf('%s/resPerI%s_%s_sr%d_%s_ihpfix%s_ihpmul%s', ...
            %                    DIR_FIGS, x_type, name_stats, i_sr(i),...
            %                    pp.gen_timeStr(), str_fix, ... 
            %                    str_mul ) );
            % saveas( h(i), sprintf('%s/resPerI%s_%s_sr%d_%s_ihpfix%s_ihpmul%s', ...
            %                    DIR_FIGS, x_type, name_stats, i_sr(i),...
            %                    pp.gen_timeStr(), str_fix, ... 
            %                    str_mul), 'epsc' );
        case {'mean', 'all'}
            % Plot the average curve of n repeated tests for each method.  
            % When every curve is composed of c_k={(i, y_i^k): i=1,..,n}, where i
            % is the number of iterations or epochs, then the average curve is
            % ave_c = {(i, nanmean((y_i^k)_k))}. We use the package gramm for
            % this purpose. 
            h(i) = pp.gramm_resPerI_2(cur); 
            % saveas(h(i), ...
            % sprintf('%s/resPerI%s_%s_sr%d_%s_ihpfix%s_ihpmul%s_meancur', ...
            %         DIR_FIGS, x_type, name_stats, i_sr(i),...
            %         pp.gen_timeStr(), str_fix, ... 
            %         str_mul ) );
            % saveas(h(i), ...
            % sprintf('%s/resPerI%s_%s_sr%d_%s_ihpfix%s_ihpmul%s_meancur', ...
            %          DIR_FIGS, x_type, name_stats, i_sr(i),...
            %          pp.gen_timeStr(), str_fix, ... 
            %          str_mul), 'epsc' );
        end
   end

   function str = array2string(ihps)
       str = '' ; 
       for l = 1 : numel(ihps)
           str = [str, int2str(ihps(l))]; 
       end
   end
end

