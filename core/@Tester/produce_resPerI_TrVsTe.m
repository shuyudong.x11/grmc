function [h, curr ] = produce_resPerI_TrVsTe(pp, i_sr, ihp_fix, ihp_mul, x_type,  i_te) 
% Produce results per iteration for fixedparam and multiphase methods. 
% For each sampling rate, produce one figure containing curves by the 
% algorithms tested. 
% INPUT:
% (1) ihp_fix: index of the parameter set in table_Hyperparams for fixed-param GRMC. 
% (2) ihp_mul: index of the parameter set in table_Hyperparams for multiphase GRMC. 
% (-) name_stats: name of the per-iteration stats, which is one of the
%     following,
%     - RMSEt:      RMSE score on test entries of X(iter) 
%     - RMSEtr:     RMSE score on revealed entries of X(iter) 
% OUTPUT:
% (1) h: array of figure handles, each figure handle presents results of one
%     sampling rate.  
% (2) cur: 1d cell array of size  

   if nargin < 5
       x_type = 'iter'; % other possible options: time, epoch. 
   end
   if nargin < 6
       i_te = '1'; 
   end
   if i_sr(1) < 0
       i_sr = pp.range_samplrates;
   end
   for i = 1 : numel(i_sr)
        cur(i,:) = ...
        Tester.extract_resPerI_2(pp, i_sr(i), ihp_fix, ihp_mul, i_te, ...
                                 x_type, 'RMSE_t');
        cur_te(i,:) = ...
        Tester.extract_resPerI_2(pp, i_sr(i), ihp_fix, ihp_mul, i_te,...
                                 x_type,'RMSE_tr');        
        % Depending on the type of i_te:
        switch i_te
        case {'1', 'random'} 
            h(i) = pp.plot_resultsPerIter([cur(i,:), cur_te(i,:)], true);
        case {'mean', 'all'}
            % Plot the average curve of n repeated tests for each method.  
            % When every curve is composed of c_k={(i, y_i^k): i=1,..,n}, where i
            % is the number of iterations or epochs, then the average curve is
            % ave_c = {(i, nanmean((y_i^k)_k))}. We use the package gramm for
            % this purpose. 
            h(i) = pp.gramm_resPerI_2([cur(i,:), cur_te(i,:)]); 
        end
   end
   curr = [cur, cur_te]; 
end


