function produce_saveFigures(sid)

% sid here should be something like:
% sid = sprintf('resPerI%s_TrVsTe_%dSRs_%s', x_type, length(pp.range_samplrates),pp.gen_timeStr() ) ; 
if ~isdir(Tester.DIR_FIGS)
    DIR_FIGS = '~/Dropbox/DBucl/temporary/matfiles';
else
    DIR_FIGS = Tester.DIR_FIGS;
end
dir_name = sprintf('%s/%s', DIR_FIGS, sid); 
if isdir(dir_name)
    warning('The subfolder %s already exists!\n', dir_name); 
else
    mkdir(dir_name); 
    if isdir(dir_name)
        warning('The subfolder %s is created now!\n', dir_name); 
    else
        error('The subfolder %s does NOT exist!\n', dir_name); 
    end
end
% Create a text file 
fid = fopen(sprintf('%s/list.txt', dir_name), 'wt' );

% Get the handles to all figures 
figlist=findobj('type','figure');
for i=1:numel(figlist)
    relpath = sprintf('%s/figure%d', sid, figlist(i).Number) ;
    saveas(figlist(i), sprintf('%s/%s', DIR_FIGS, relpath), 'fig');
    % saveas(figlist(i),fullfile(dir_name,[timeStr,'figure' int2str(figlist(i).Number)]), 'epsc');
    % Next, append the current file path into the text file:
    fprintf(fid, '%s\n', relpath);
end
fprintf(fid, '\n # subfolder name: %s\n', sid);
fclose(fid);

end

