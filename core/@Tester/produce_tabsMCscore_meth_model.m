function tabs = produce_tabsMCscore_meth_model(pp, name_sc) 
% Produce one table of Recovery Error for each method and each problem model
% (MC, MMMF, GRMC), at each sampling rate.  

if nargin < 2
    name_sc = 'RMSE_t';
end
if ~isdir(Tester.DIR_FIGS)
    DIR_FIGS = '~/Dropbox/DBucl/temporary/matfiles';
else
    DIR_FIGS = Tester.DIR_FIGS;
end
fidh = 'MCsc_MethByModel';

[~,tabs] = Tester.extract_tabRecVsSR(pp, name_sc);

if ~isprop(pp,'tid')
    tid = pp.gen_timeStr();
elseif isempty(pp.tid )
    tid = pp.gen_timeStr();
else
    tid= pp.tid; 
end
sid = sprintf('%s_%s_%s_%dSRs_%s', ...
              pp.data.name, fidh, name_sc, length(pp.range_samplrates),...
              tid); 
dir_name = sprintf('%s/%s', DIR_FIGS, sid); 
%% create a subfolder with the name given above
if isdir(dir_name)
    warning('The subfolder %s already exists!\n', dir_name); 
else
    mkdir(dir_name); 
    if isdir(dir_name)
        warning('The subfolder %s is created now!\n', dir_name); 
    else
        error('The subfolder %s does NOT exist!\n', dir_name); 
    end
end

for ii = 1 : numel(tabs)
    writetable(tabs{ii}, sprintf('%s/tableSR%d.txt', dir_name,ii),...
               'WriteRowNames',true);
end

end


