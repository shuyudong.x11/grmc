function produce_txtTesterId(pb, sid, DIR)
if nargin < 3
    DIR = Tester.DIR_FIGS;
end
if ~isdir(DIR)
    DIR = '~/Dropbox/DBucl/temporary/matfiles';
end
dir_name = sprintf('%s/%s', DIR, sid); 
if isdir(dir_name)
    fprintf('Adding new text file to %s ..\n', dir_name); 
else
    error('The subfolder %s does NOT exist!\n', dir_name); 
end

% Create a text file 
% fid = fopen(sprintf('%s/id_tester.txt', dir_name), 'w' );
% fclose(fid);
if strcmp(pb.data.name(1:3), 'syn')
    writetable(struct2table(pb.data.gen_opts), sprintf('%s/tester_data.txt', dir_name));
else
    writetable(struct2table(pb.L.opts_buildLmat), sprintf('%s/tester_%s.txt', dir_name,...
                                                           pb.data.name));
end
writetable(array2table(pb.range_samplrates), sprintf('%s/tester_SRs.txt', dir_name));

writetable(pb.table_hyperparams, sprintf('%s/tester_HPs.txt', dir_name));
if ~isempty(pb.selecHP_fixedp_srByMethod)
    fid = fopen(sprintf('%s/tester_HPs.txt', dir_name),'a');
    fprintf(fid,'\nParameter selection results per (Sample rate, Method) for fixed-params GRMC:\n'); 
    append_text(fid, pb.selecHP_fixedp_srByMethod); 
    fclose(fid); 
end
if ~isempty(pb.selecHP_multip_srByMethod)
    fid = fopen(sprintf('%s/tester_HPs.txt', dir_name),'a');
    fprintf(fid,'\nParameter selection results per (Sample rate, Method) for multiphase GRMC:\n'); 
    append_text(fid, pb.selecHP_multip_srByMethod); 
    fclose(fid); 
end

writetable(pb.info_methods, sprintf('%s/tester_methods.txt', dir_name));

% This records in particular the method for producing the initial point (G0,H0):
writetable(struct2table(pb.params_opt), sprintf('%s/tester_paramsopt.txt', dir_name));

% Record the function names in the call stack.
% st = dbstack;
% st(1).name % The function's name
% st(2).name % The function caller's name (parent). 
% % If numel(st) > 2, 
% st(3).name % The function name that called the function caller (grand-parent)
fid = fopen(sprintf('%s/script_callstack.txt', dir_name),'a');
st = dbstack; 
for ii = 1 : min(3,numel(st)) 
    fprintf(fid,'%s,\n', st(ii).name);
end
fclose(fid); 

    function append_text(fid, dat)
        for i = 1  : size(dat,1)
            fprintf(fid,'%d,', dat(i,:));
            fprintf(fid,'\n');
        end        
    end






end


