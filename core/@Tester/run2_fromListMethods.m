function scoreByMethods = run2_fromListMethods(self, mcinfo, table_paramspb,...
                                              is_forCrossval, is_MC, varargin)
% note (#314): difference with run_fromListMethods():
% removed functionality related to crossval. 
%
    if nargin < 5
        % note: this option is only set to true in `func: run_repeatTests()`
        is_MC = false;
    end
    rowid_RMSEt = ...
    find(strcmp('RMSE_t',self.DEFAULT_resPerIter_labelnames));
        len = sum(self.info_methods.willRun);
        ids_m = find(self.info_methods.willRun);
        
        scoreByMethods = nan(len, numel(self.KEYS_MCscores)); 
    for im = 1 : len % each method s.t. info_methods(:,'willRun') is true:
        if ~is_MC
            jstar = 2;
        else
            jstar = 1;
        end
        self.refresh_GRLRMC(self.info_methods(ids_m(im),'name_man').name_man{1},...
                            mcinfo, table_paramspb(jstar,:), []);
        % now we can run the method:
        has_results = true;
        switch self.info_methods(ids_m(im),'name_algo').name_algo{1} %name_algo
           case 'RSD_MANOPT'
                [X, stats] = self.solve_rsd_manopt('someinit',varargin{:});
           case 'RCG_MANOPT'
                [X, stats] = self.solve_rcg_manopt('someinit',varargin{:});
           case 'RCGprecon_MANOPT'
                self.params_opt.rcg_usePrecon = true;
                if isempty(self.preconditioner) 
                    error('Error: no preconditioner is provided with the pb class!\n');
                end
                [X, stats] = self.solve_rcg_manopt('someinit', varargin{:});
                self.params_opt.rcg_usePrecon = false;
           case 'GRALS'
                [X, stats] = self.runGRMF('someinit', varargin{:});                        
           case 'somealgo-MC' 
                % such as `RTRMC, LMaFit` etc.
                if self.params_pb.Lreg_betar == 0 && self.params_pb.Lreg_betac == 0 
                    [X, stats] = self.runGRMF('someinit', varargin{:});% otherwise
                else
                    has_results = false;
                end
        end

            if has_results
            % see Tests/runtest() and Solver.collect_results() for
            % `self.scores`
                scoreByMethods(im,:) = self.compute_MCscores_sol(X, mcinfo);
                %scoreByMethods(im,:) = table2array(self.scores(1,:));
            % warning (#1202): make sure that `self.status_runTester` is only modified by 
            % `run_tester()` and `run_repeatTests()`
                res_2darray = self.get2dArray_resultsPerIter(stats);
                if is_MC
                    self.resPerIterMC_3dCellArrays{...
                        self.status_runTester.i_samplrate,...
                        self.status_runTester.i_repeatTests,...
                        ids_m(im)} = res_2darray; 
                else
                    self.resPerIterGRMC_3dCellArrays{...
                        self.status_runTester.i_samplrate,...
                        self.status_runTester.i_repeatTests,...
                        ids_m(im)} = res_2darray; 
                end
            else
                error('Something is wrong in `tester/func:run_fromListMethods()`');
            end
    end %for method in "activemethods" 
end

