function scoreByMethods = run_fromListMethods(self, mcinfo, table_paramspb,...
                                              is_forCrossval, is_MC, varargin)
% note (#622): This function is deprecated. 
    % input: (+) do_storeStatsPerIter (logical value) 

    % note (#1202): make sure that `solver.params_opt` is already refreshed using the 
    %               following line (somewhere before this
    %               function is called). => see `func:run_tester()`.
    if nargin < 5
        % note: this option is only set to true in `func: run_repeatTests()`
        is_MC = false;
    end
    % rowid_RMSEt = 3;
    rowid_RMSEt = ...
    find(strcmp('RMSE_t', self.DEFAULT_resPerIter_labelnames));
    if is_forCrossval
        len = sum(self.info_methods.doCrossval);
        ids_m = find(self.info_methods.doCrossval);
        scoreByMethods = nan(1, len);
        self.info_methods(ids_m,'jstar_params').jstar_params = ...
                    repmat(self.status_runTester.j_params,[len,1]);
    else
        len = sum(self.info_methods.willRun);
        ids_m = find(self.info_methods.willRun);
        
        scoreByMethods = nan(len, numel(self.KEYS_MCscores)); 
    end
    for im = 1 : len % each method s.t. info_methods(:,'willRun') is true:
        if ~is_MC
            jstar = self.info_methods(ids_m(im), 'jstar_params').jstar_params;
        else
            jstar = self.info_methods(ids_m(im),'jstar_paramsMC').jstar_paramsMC;
        end
        self.refresh_GRLRMC(self.info_methods(ids_m(im),'name_man').name_man{1},...
                            mcinfo, table_paramspb(jstar,:), []);
        % now we can run the method:
        has_results = true;
        switch self.info_methods(ids_m(im),'name_algo').name_algo{1} %name_algo
           case 'RSD_MANOPT'
                [X, stats] = self.solve_rsd_manopt('someinit',varargin{:});
           case 'RCG_MANOPT'
                [X, stats] = self.solve_rcg_manopt('someinit',varargin{:});
           case 'RCGprecon_MANOPT'
                self.params_opt.rcg_usePrecon = true;
                if isempty(self.preconditioner) 
                    error('Error: no preconditioner is provided with the pb class!\n');
                end
                [X, stats] = self.solve_rcg_manopt('someinit', varargin{:});
                self.params_opt.rcg_usePrecon = false;
           case 'GRALS'
                [X, stats] = self.runGRMF('someinit', varargin{:});                        
           case 'somealgo-MC' 
                % such as `RTRMC, LMaFit` etc.
                if self.params_pb.Lreg_betar == 0 && self.params_pb.Lreg_betac == 0 
                    [X, stats] = self.runGRMF('someinit', varargin{:});% otherwise
                else
                    has_results = false;
                end
        end

        if is_forCrossval % do_storeStatsPerIter
        % the SCORE is 'rmse_t' (is the validation score when `is_forCrossval`, since "_t 
        % (or _test)" refers to the validation set, c.f. `func: crossval_genTrainvalsets()` 
        % ), which is situated at the `i_row=2` in `res_2darray`, c.f. `func:
        % get2dArray_resultsPerIter()`  
            if has_results
                res_2darray = self.get2dArray_resultsPerIter(stats);
                scoreByMethods(im) = res_2darray(rowid_RMSEt, end);
            else
                scoreByMethods(im) = Inf;
            end
        else
            if has_results
            % see Tests/runtest() and Solver.collect_results() for
            % `self.scores`
                % note (#316): to replace the next line by: 
                scoreByMethods(im,:) = self.compute_MCscores_sol(X, mcinfo);
                % scoreByMethods(im,:) = table2array(self.scores(1,:));
            % warning (#1202): make sure that `self.status_runTester` is only modified by 
            % `run_tester()` and `run_repeatTests()`
                res_2darray = self.get2dArray_resultsPerIter(stats);
                if is_MC
                    self.resPerIterMC_3dCellArrays{...
                        self.status_runTester.i_samplrate,...
                        self.status_runTester.i_repeatTests,...
                        ids_m(im)} = res_2darray; 
                else
                    self.resPerIterGRMC_3dCellArrays{...
                        self.status_runTester.i_samplrate,...
                        self.status_runTester.i_repeatTests,...
                        ids_m(im)} = res_2darray; 
                end
            else
                error('Something is wrong in `tester/func:run_fromListMethods()`');
            end
        end
   end %for method in "activemethods" 
end

