function run_repeatTests(self, table_paramspb)
% INPUT: 
% (1) table_paramspb: c.f. func:run_tester().
% (+) i_sr <- self.status_runTester (containning current state
%     of 'i_samplrate', 'j_hyperparam', etc.)
    self.set_params_opt('maxiter', self.opts_tester.repeatTests_maxiter);
    i_sr = self.status_runTester.i_samplrate;
    for ii = 1 : self.opts_tester.n_repeatTests
        self.status_runTester.i_repeatTests = ii;
        % note (before #1202): the GRLRMC problem is entirely and uniquely determined 
        %                      (Omega, alpha_{r,c}, gamma_{r,c}, rank) inside `func:
        %                      run_fromListMethods()`
        % the output argument of the following func is a 2d array of
        % size [|n_activemethods|x |KEYS_MCscores|]
        mcinfoii = GRLRMC.genOMEGA_MC(self.data.mat,...
                                      self.range_samplrates(i_sr));                
        self.scoresGRMC_4dmat(i_sr, :, :, ii) = ...
        self.run_fromListMethods(mcinfoii, table_paramspb, false );
        self.scoresMC_4dmat(i_sr, :, :, ii) = ...
        self.run_fromListMethods(mcinfoii, table_paramspb, false, true );
        
    end
end

