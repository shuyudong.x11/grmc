function run_tester_(self, tab_hp, varargin)
% Notes: This function provides results for each of the J given paremeter
% settings. A single test refers to running one GRMC algorithm till its termination
% (for some stopping criterion, see @Solver.m and its methods therein). The total
% number of tests to run depends on:
% - I: Number of different sampling rates (|Omega|/mn) to test. 
% - J: Number of parameter settings. 
% - n: Number of tests to repeat for a fixed sampling rate and a parameter
%      setting.
% - m1: Number of algorithms to test for fixed-parameter GRMC (see Algorithm
%       1-3, GRALS). 
% - m2: Number of algorithms to test for multiphase GRMC (see Scheme 1, paper) 
% The number of all tests is N_tot=I*J*n*(m1+m2). The total time needed to finish is 
% T(algorithm)*N_tot. To reduce this number, one can try
%       (a) reduce J, the number of parameter settings. If one runs a
%           cross-validation procedure before calling this function, it is
%           acceptable to just provide the best parameter setting chosen by CV,
%           in this case J=1 for each sampling rate; Otherwise, one
%           can try reduce the maxiter parameter to reduce T(algorithm) in some
%           preliminary tests, and narrow down the range of parameter values to choose
%           from. 
%       (b) set n=1, this is acceptable in preliminary tests when the ranges of
%           parameter values are not clear. 
% INPUT: 
% (1) table_params: a nx3 table (or an plain nx3 array) of parameter configurations. 
%     Each row should contain: 
%     alpha  ..................a single value. 
%     beta_r ….................a single value. 
%     beta_c ….................a single value. 
% (2) varargin: the optional input arguments are parsed by @Tester/parseArgs().         
% 
% Each algorithm returns [X, stats], see @Solver/Solver.m. There are
% N_tot such results. X is the completed matrix and is assessed by several
% matrix completion scores (e.g. RMSE) and is stored in a 
% stats is a 2d array of size (n_type_stats,
% n_iters) and is stored in a 4-d cell array, indexed by (i,j,n,id_m). See
% the list below. 
% 
% OUTPUT: 
% (1) resPerI_fixedp_srByHpByTeByMethod: a 4d cell array. 
% (2) resPerI_multip_srByHpByTeByMethod: a 4d cell array. 
 

if isempty(tab_hp)
    [self.table_hyperparams, self.opts_tester] = ...
    self.gen_tableHp3(varargin{:}); 
    tab_hp = self.table_hyperparams; 
else
    % when tab_hp is not empty, it is still possible to have optional
    % arguments other than those for hyperparameters. 
    self.opts_tester   = Tester.parseArgs('runTester', varargin{:});
end
self.params_opt     = Tester.parseArgs('opt',varargin{:});
self.info_methods   = Tester.gen_infoMethods(...
                      Tester.parseArgs('infoMethods', varargin{:}) );

% To ensure that tolgradnorm is small enough, repeatTests_tolGrad is set by
% default to 1e-14. 
self.set_params_opt('tolgradnorm',...
                    self.opts_tester.repeatTests_tolGrad);
len_m = sum(self.info_methods.willRun);
len_mm = sum(self.info_methods.willRun_multiphase);
ids_m = find(self.info_methods.willRun);
ids_mm = find(self.info_methods.willRun_multiphase);
method_init = self.params_opt.method_init;
n_tests = self.opts_tester.n_repeatTests; 

% Initialize the outputs 
res_fixedp = cell(numel(self.range_samplrates), size(tab_hp,1), n_tests, len_m);
res_multip = cell(numel(self.range_samplrates), size(tab_hp,1), n_tests, len_mm);
mcsc_fixedp = zeros(numel(self.range_samplrates), size(tab_hp,1),len_m, length(self.KEYS_MCscores));
mcsc_multip = zeros(numel(self.range_samplrates), size(tab_hp,1),len_mm,length(self.KEYS_MCscores) ); 
for i = 1 : length(self.range_samplrates)
    % (0) The next line is active only for experiments on real data without 
    %     side info, L needs to be updated whenever Omega is different: 
    self.refresh_Lmat(self.MCINFOS{i}, varargin{:});
    for j = 1 : size(tab_hp, 1)
        % s_fixedp = nan(length(self.KEYS_MCscores)); 
        % s_multip = nan(length(self.KEYS_MCscores)); 
        for t = 1 : n_tests
            mcinfo = GRLRMC.genOMEGA_MC(self.data.mat, self.range_samplrates(i)); 
            for im = 1 : len_m 
                % Reset the parameters of the problem: 
                self.refresh_GRLRMC(self.info_methods(ids_m(im),'name_man').name_man{1},...
                                    mcinfo, tab_hp(j,:), []);
                algo_name = self.info_methods(ids_m(im),'name_algo').name_algo{1}; 
                ls_type = self.info_methods(ids_m(im),:).type_ls{1}; 
                X = self.initialization(method_init); 
                [X, stats] = self.runsolver_fromListMethods(X, ...
                                    algo_name, ls_type, varargin{:}); 
                mcsc_fixedp(i,j,im,:) = ...
                squeeze(mcsc_fixedp(i,j,im,:))+self.compute_MCscores_sol(X, mcinfo); 
                res_fixedp{i, j, t, im}=self.get2dArray_resultsPerIter(stats); 
            end
            for im = 1 : len_mm 
                % Reset the parameters of the problem: 
                self.refresh_GRLRMC(self.info_methods(ids_mm(im),'name_man').name_man{1},...
                                    mcinfo, tab_hp(j,:), []);
                algo_name = self.info_methods(ids_mm(im),'name_algo').name_algo{1}; 
                ls_type = self.info_methods(ids_mm(im),:).type_ls{1}; 
                X = self.initialization(method_init); 
                % (#506.note-2) multiphase solver 
                [X, stats_] = self.runsolverMultiph_fromListMethods(X, algo_name, ls_type, varargin{:}); 
                mcsc_multip(i,j,im,:) = ...
                squeeze(mcsc_multip(i,j,im,:)) + self.compute_MCscores_sol(X, mcinfo); 
                res_multip{i, j, t, im}=self.get2dArray_resultsPerIter(stats_); 
            end
        end
        mcsc_fixedp(i,j,:,:) =mcsc_fixedp(i,j,:,:) / n_tests; 
        mcsc_multip(i,j,:,:) =mcsc_multip(i,j,:,:) / n_tests; 
    end
end

self.resPerI_fixedp_srByHpByTeByMethod = res_fixedp; 
self.resPerI_multip_srByHpByTeByMethod = res_multip; 
self.scores_fixedp_srByHpByMethodBySctype = mcsc_fixedp;
self.scores_multip_srByHpByMethodBySctype = mcsc_multip; 
% self.scores_multiphase_srByMethodBySctype = mcsc_multip; 

end 

