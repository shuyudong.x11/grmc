function run_tester_multiphase(self, alpha, varargin)
% Pseudo-code:
% function run1_multiphase(self, args)
% INPUT: alpha  ..................1d array of size (l,1). 
%        beta_r ….................a single value 
%        method_init .............string {'random', 'M0'}
%        
% OUTPUT: X, stats................same as existing implementations.
% 
% todo (#327.1): add `willRun_multiphase` to `(property) info_methods`. 
self.opts_tester   = Tester.parseArgs('runTester', varargin{:});
method_init = self.params_opt.method_init;
ROWID_RMSEt = self.get_rowid_MCscores('RMSE_t');
self.params_opt = Tester.parseArgs('opt',varargin{:});
tab_hp = self.gen_tableHp_multiphase(varargin{:}); 
self.table_hp_multiphase = tab_hp; 
self.set_params_opt('tolgradnorm',...
                    self.opts_tester.repeatTests_tolGrad);
self.set_params_opt('maxiter', ...
                    self.opts_tester.maxiter_perPhase)
self.resPerIter_multiphase_srByHpByMethod = ...
                    cell(numel(self.range_samplrates),...
                         size(tab_hp,1),...
                         sum(self.info_methods.willRun_multiphase));

self.resPerIter_multiphase_srByMethod = ...
                    cell(numel(self.range_samplrates),...
                         sum(self.info_methods.willRun_multiphase));

len = sum(self.info_methods.willRun_multiphase);
ids_m = find(self.info_methods.willRun_multiphase);
for im = 1 : len 
    algo_name = self.info_methods(ids_m(im),'name_algo').name_algo{1}; 
    % note: the method/algo index is "ids_m(im)", the index 1 at the end is to
    % ensure returning a string and not a cell containing a string. 
    self.refresh_GRLRMC(self.info_methods(ids_m(im),'name_man').name_man{1});
% (0) generate a grid of values for betar:
% table_hp = self.gen_tableHp_multiphase();
% (1) initialization: could use `self(solver).initialization()` 
    % X = self.initialization(method_init); 
    mcscores = nan(length(self.range_samplrates),size(self.table_hyperparams,1),...
                   length(self.KEYS_MCscores));
    self.resPerIter_multiphase_srByHpByMethod = ...
                cell(numel(self.range_samplrates),...
                     size(tab_hp, 1),...
                     sum(len));
    for i = 1: length(self.range_samplrates)
        mcinfo = self.MCINFOS{i} ;
        for j = 1 : size(self.table_hyperparams,1)
            params_pb = self.table_hyperparams(j,:);
            % the fileds 'alpha_{r,c}' will not be used, but set by alpha(l)
            % for each value of betar, (actually \beta_r/\alpha_r)
            betar = params_pb.Lreg_betar ;
            X = self.initialization(method_init); 
            stats_ = [];
        % refresh/instantiate the problem: could use `self(grlrmc).refresh_GRLRMC()`. 
            for l = 1 : length(alpha)
            % (2) call solver: could use `[X,stats] = self(solver).solve_{algoname}(.., X)`. 
                params_pb.alpha_r = alpha(l);
                params_pb.alpha_c = alpha(l);
                params_pb.Lreg_betar = alpha(l) * betar; 
                self.refresh_GRLRMC([], mcinfo, params_pb) ; 
                [X, stats] = self.runsolver_fromListMethods(X, algo_name, varargin{:});
                stats_ = self.append_newstats(stats_, stats);
            end
            % (3) collect, append results: could just do `stats_all = [stats_all, stats]`
            %     compute the mcscores of a solution. See
            %     `grmc-coding.md#327.note-1` and `Tester/~`. 
            mcscores(i,j,:) = self.compute_MCscores_sol(X, mcinfo); 
            self.resPerIter_multiphase_srByHpByMethod{i,j,im} = ...
                                            self.get2dArray_resultsPerIter(stats_); 
        end
        % select results to return by selecting the best score among `scoresByMethods`
        % for each method.    
        [~, argj] = min(mcscores(i,:, ROWID_RMSEt)); 
        self.info_methods(ids_m(im), 'jstar_gridsel').jstar_gridsel = argj;
        self.info_methods(ids_m(im), 'score_gridsel').score_gridsel = ...
                                                mcscores(i, argj, ROWID_RMSEt);
        self.scores_multiphase_srByMethodBySctype(i,im,:) = mcscores(i,argj,:); 
        self.resPerIter_multiphase_srByMethod{i,im} = ...
                                self.resPerIter_multiphase_srByHpByMethod{i,argj,im}; 
    end

end

end


