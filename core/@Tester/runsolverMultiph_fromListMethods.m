function [X, stats_] = runsolverMultiph_fromListMethods(self, X, algo_name,...
                                                ls_type, type_ss0, varargin)

if nargin < 5
    type_ss0 = Solver.default_opt.stepsize0_type;
end
    
    fprintf('start solving problem(alpha(%f), beta(%f))\n',...
    self.params_pb.alpha_r,self.params_pb.Lreg_betar); 

    % #506-note.1: the number of iterations for each phase 
    self.set_params_opt('maxiter', ...
                        self.opts_tester.maxiter_perPhase); 
    
    % fa = [logspace(0, -6, 2), 0]; 
    fa = [logspace(0, 0, 1), 0]; % the 2-phase scheme: fa = [1, 0] 
    stats_ = [];
    for l = 1  : length(fa) % depth of decrease of alpha to 0 
        self.params_pb.alpha_r = self.params_pb.alpha_r * fa(l);
        self.params_pb.alpha_c = self.params_pb.alpha_r * fa(l);
        self.params_pb.Lreg_betar = self.params_pb.Lreg_betar * fa(l);
        self.params_pb.Lreg_betac = self.params_pb.Lreg_betac * fa(l);
        % after having updated the 4 parameters, refresh the problem class  
        self.refresh_GRLRMC(); 
        [X, stats] = self.runsolver_fromListMethods(X, algo_name, ...
                                                ls_type, type_ss0, varargin{:});
        stats_ = self.append_newstats(stats_, stats);
    end
    % Whenever finished, reset maxiter to the original value for fixed-param GRMC 
    self.set_params_opt('maxiter', self.opts_tester.maxiter_fixedp); 
end


