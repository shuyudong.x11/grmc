function [X, stats] = runsolver_fromListMethods(self, X, algo_name,...
                                    ls_type, type_ss0, varargin)
% INPUT: (given with self) alpha  ..................a single value 
%        (given with self) beta_r ….................a single value 
%        (given with self) method_init .............string {'random', 'M0'}
%        
% OUTPUT: X, stats................same as existing implementations.
 
if nargin < 5
    type_ss0 = Solver.default_opt.stepsize0_type;
end
self.set_params_opt('stepsize0_type', type_ss0); 

    fprintf('Algorithm:%s, start solving problem(alpha(%f), betar(%f))\n',...
            algo_name, self.params_pb.alpha_r,self.params_pb.Lreg_betar); 
     
    switch algo_name 
       case 'RCG'
           switch ls_type
               case 'lsArmijo'
               [X, stats] = self.solve_RCG_lsArmijo(X, varargin{:}); 
               case 'lsFree'
               [X, stats] = self.solve_RCG_lsFree(X, varargin{:}); 
               case 'lsBB'
               [X, stats] = self.solve_RCG_lsBB(X, varargin{:}); 
           end
       case 'RSD'
           % We use RSD in the keywords and RGD in the function name. 
           switch ls_type
               case 'lsArmijo'
               [X, stats] = self.solve_RGD_lsArmijo(X, varargin{:}); 
               case 'lsFree'
               [X, stats] = self.solve_RGD_lsFree(X, varargin{:}); 
               case 'lsBB'
               [X, stats] = self.solve_RGD_lsBB(X, varargin{:}); 
           end
       case 'RSD_MANOPT'
            [X, stats] = self.solve_rsd_manopt(X,varargin{:});
       case 'RCG_MANOPT'
            [X, stats] = self.solve_rcg_manopt(X,varargin{:});
       case 'RCGprecon_MANOPT'
            self.params_opt.rcg_usePrecon = true;
            if isempty(self.preconditioner) 
                error('Error: no preconditioner is provided with the pb class!\n');
            end
            [X, stats] = self.solve_rcg_manopt(X, varargin{:});
            self.params_opt.rcg_usePrecon = false;
       case 'AltMin1'
           % Accuracy for each LS subproblem is highest. In this case, the
           % number of inner (CG) loops is exactly n_CG = grmf_maxiter_cg. 
            self.params_opt.grmf_eps = 1e-14; 
            self.params_opt.grmf_maxiter_cg = 500; 
            temp = self.params_opt.maxiter; 
            self.params_opt.maxiter = 50; 
            [X, stats] = self.solve_AltMin(X, varargin{:});                        
            self.params_opt.maxiter = temp; 
       case 'AltMin2'
           % Accuracy for each LS subproblem is low. In this case, the
           % number of inner (CG) loops can be easily smaller than the preset n_CG. 
            self.params_opt.grmf_eps = 1e-6; 
            self.params_opt.grmf_maxiter_cg = 500; 
            temp = self.params_opt.maxiter; 
            self.params_opt.maxiter = 50; 
            [X, stats] = self.solve_AltMin(X, varargin{:});                        
            self.params_opt.maxiter = temp; 
       case 'GRALS1'
           % Accuracy for each LS subproblem is highest. In this case, the
           % number of inner (CG) loops is exactly n_CG = grmf_maxiter_cg. 
            self.params_opt.grmf_eps = 1e-10; 
            self.params_opt.grmf_maxiter_cg = 500; 
            [X, stats] = self.runGRMF(X, varargin{:});                        
       case 'GRALS2'
           % Accuracy for each LS subproblem is low. In this case, the
           % number of inner (CG) loops can be easily smaller than the preset n_CG. 
            self.params_opt.grmf_eps = 1e-1; 
            self.params_opt.grmf_maxiter_cg = 10; 
            [X, stats] = self.runGRMF(X, varargin{:});                        
       case 'GRALS'
           % Accuracy for each LS subproblem is set by default (1e-6). In 
           % this case, the number of inner (CG) loops that are effectively
           % carried out during each global iteration is unknown, and
           % depends on e.g. the scale of the data; the norm of the
           % gradient simply gets uniformly larger or smaller according to the
           % scale of the data matrix. 
            [X, stats] = self.runGRMF(X, varargin{:});                        
    end
end

