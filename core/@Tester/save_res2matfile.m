function sid=save_res2matfile(pp)

DIR = './archive'; 

if ~isprop(pp,'tid')
    tid = pp.gen_timeStr();
elseif isempty(pp.tid )
    tid = pp.gen_timeStr();
else
    tid= pp.tid; 
end
sid = sprintf('resAll_%dSRs_%s', ...
               length(pp.range_samplrates),...
               tid) ; 
%% create folder 
dir_name = sprintf('%s/%s', DIR, sid); 
if isdir(dir_name)
    warning('The subfolder %s already exists!\n', dir_name); 
else
    mkdir(dir_name); 
    if isdir(dir_name)
        warning('The subfolder %s is created now!\n', dir_name); 
    else
        error('The subfolder %s does NOT exist!\n', dir_name); 
    end
end
%% pack results and save to dir/matfile.mat
res = struct('resPerI_fixedp', {pp.resPerI_fixedp_srByHpByTeByMethod},...
             'resPerI_multip', {pp.resPerI_multip_srByHpByTeByMethod},...
             'data', pp.data,...
             'SRs', pp.range_samplrates,...
             'Lap', pp.L, ...
             'info_methods', pp.info_methods,...
             'tab_hps', pp.table_hyperparams,...
             'paramsopt', pp.params_opt); 
save(sprintf('%s/matfile.mat', dir_name), 'res'); 

%% add additional information in text files to the same subfolder
Tester.produce_txtTesterId(pp, sid, DIR);

end

