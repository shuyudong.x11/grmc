classdef GRLRMC < Solver
% The problem class of the graph-regularized matrix completion problem. 

    properties
        pbinfo; 
        Xstar;
        Xstar_r; 
        mcinfo;
        params_pb;
        L;
        evals;
    end
    properties (Constant)
        default_pb = struct('sampl_rate', 0.1,...
                            'alpha_r', 0, ...
                            'alpha_c', 0,...
                            'Lreg_betar',0,...
                            'Lreg_betac',0,...
                            'rank', 10);

        NAMES_MAN  = {'GH_Euc', ...
                     'USV_embeded', ...
                     'GH_qprecon',... % 'GH_EucCruderls',...
                     'GH_qpreconExa',...
                     'GH_qleftinv', ...
                     'GH_qleftinvExa',...
                     'GH_qpreconLeqk',...
                     'GH_qleftinvLeqk'};
        KEYS_MCscores = {'RMSE','MSE','NRMSE','ND','MAPE',...
                        'RMSE_tr','MSE_tr','NRMSE_tr','ND_tr','MAPE_tr',...
                        'RMSE_t','MSE_t','NRMSE_t','ND_t','MAPE_t',...
                        'relErr', 'SNR'};
        DEFAULT_resPerIter_labelnames = {'niter', 'time', 'RMSE_t',...
                                        'RMSE', 'RMSE_tr', 'relErr', ...
                                        'gradnorm', 'cost',...
                                        'ncgGPerI','ncgHPerI','fevalsPerI', 'flopsCum'};
    end
    methods (Static)
        function rowid = get_rowid_MCscores(key)
            rowid =  find(strcmp(key, GRLRMC.KEYS_MCscores));
        end
        function rowid = get_rowid_resPerIter(key)
            rowid =  find(strcmp(key, GRLRMC.DEFAULT_resPerIter_labelnames));
        end
        function p = build_parser(paramset, p)
            % build a parser for a structure `paramset`:
            if nargin < 2
                p = inputParser;
            end
            fds = fieldnames(paramset);
            for i = 1 : length(fds)
                % note (#605): see [here](https://nl.mathworks.com/help/matlab/ref/inputparser.html) for
                % more details about the inputParser-related functions
                if ~strcmp(fds{i},'Properties')
                    addParameter(p, fds{i}, paramset.(fds{i}));
                    % addOptional(p, fds{i}, paramset.(fds{i}));
                end
            end
        end
        function SNR = computeSCORES_SNR(Xsol, Xclean)
            sn_ratio = sum(Xclean(:).^2) / sum((Xsol(:)-Xclean(:)).^2);
            SNR = 10 * log10(sn_ratio);
        end
        function MCscores = computeSCORES_MC(Delta, Xtar, rmse)
            % from 1 to 5: RMSE, MSE, NRMSE, ND, MAPE
            % 
            % input: Delta,  Xtar must be of size [_ x 1].
            if nargin < 3 
                MSE = mean(Delta.^2); 
                rmse = sqrt(MSE);
            else
                MSE = rmse^2;
            end
            % mtemp2 = mean(abs(Xtar));

            MCscores = [rmse;...
                        MSE;...
                        nan;...
                        nan;...
                        nan ];
        end
        function mcscores = compute_NRMSEscores(mcscores, Xtar, Xtar_tr, Xtar_t)
        % 
            rowid = [3; 8; 13];
            rowid_rmse = [1; 6; 11];
            mtemp2 = [mean(abs(Xtar(:))); mean(abs(Xtar_tr)); ...
                      mean(abs(Xtar_t))];
            mcscores(rowid) = mcscores(rowid_rmse)./mtemp2;
        end
        function M = gendata_LR(m,n,r)
            A = randn(m,r);
            B = randn(n,r);
            M = A * B';
        end
        function mcinfo = genOMEGA_MC(Xtar, rho)
        % Use matlab built-in function:randperm(n,k), k-permutation (sampling without
        % replacement), see [here](https://nl.mathworks.com/help/matlab/ref/randperm.html)
        % for details. 
        % note (#1228): compatiblize MovieLens data matrices, large and
        % SPARSE. 
            [m, n] = size(Xtar);
            if ~issparse(Xtar)
                nnz = min(ceil(m*n*rho), m*n); 
                %oinds = randperm(m*n, nnz)';
                %oindsc = setdiff([1:m*n]', oinds);
                % --------- sampling -------------
                % The entries in Omega are i.i.d. samples of the Bernoulli
                % distribution B(p), where p = sampling rate. 
                subsample = rand(m*n,1)<rho ;
                oinds = find(subsample);
                % When m*n is large, the test set becomes too large for the
                % compuation of the test error, especially when it is needed
                % for each iteration. Hence we randomly sample 1/3 the amount
                % of |Omega| in the complementary set of Omega. 
                oindsc = find(~subsample);
                if m*n > 0.25e6 % e.g. m>500, n>500  
                    % size of Omega ~ rho * m*n, let the target size of test
                    % set be 0.5 * |Omega| 
                    oindsc = randsample(oindsc,...
                                min(numel(oindsc),round(0.5*rho*m*n)) ); 
                end
                [I, J] = ind2sub([m,n], oinds);
                [Itest, Jtest] = ind2sub([m,n], oindsc);
                % ===> note: the above 3 lines take only ~0.28sec for m=900, n=10000,
                %      and rho = 0.2.
                % --------- sampling -------------
                PXtar = Xtar(oinds);
                PXtar_t = Xtar(oindsc);
            else
                % find out available entries of Xtar:
                [Iall, Jall, Xall] = find(Xtar);
                nall = numel(Iall); 
                nnz = min(ceil(nall*rho), nall); 
                % subsample rho|Xall| entries from (Iall, Jall, Xall):
                subsample = rand(nall, 1)<rho;
                I = Iall(subsample);
                J = Jall(subsample);
                PXtar = Xall(subsample);
                Itest = Iall(~subsample);
                Jtest = Jall(~subsample);
                PXtar_t = Xall(~subsample);
            end
            mcinfo = struct('PXtar', PXtar, 'PXtar_t', PXtar_t,...
                            'I',uint32(I),'J',uint32(J),'Itest',uint32(Itest),...
                            'Jtest',uint32(Jtest), 'sampl_rate',rho,...
                            'sz_tr', numel(I),...
                            'sz_te', numel(Itest),...
                            'size_M', [m,n]);
 
        end
        function relErr = computeSCORES_relErr(mse, mcinfo)
        % Computue the relative error, which is defined as the ratio between the
        % error matrix and the true hidden matrix in the Frobenius norm.
            distF = sqrt( (numel(mcinfo.I)+numel(mcinfo.Itest))*mse ) ;
            relErr = distF/sqrt(sum([mcinfo.PXtar;mcinfo.PXtar_t].^2));
        end
        function stop  = stopfun_manopt(optpb, X, info, last) 
        % Customized stopping criteria to be considered in addition to the
        % standard stopping criteria (time and/or iteration budget and the
        % accuracy tolerance).
         
        % Get statistics for the following criteria
        % on consecutive increases in test RMSE (which is a sign of
        % serious overfitting issues).
            len = 200; 
            T = last; 
            tmin = max(2, last-len); 
            incrs = (diff([info(tmin:last).RMSE_t])>= 0);
            
            if last>1 && info(last).gradnorm > 1e2 ...
               && info(last).gradnorm / info(last-1).gradnorm > 5
               % The above conditions suggest that the iterates of gradnorm is
               % almost surely exploding.
                stop = true;
            elseif last > len+3 && sum(incrs) > .8*len 
                % RMSE_t (validation score in fact, since (Itest,Jtest)
                % belongs to the training set) scores per iteration continue to
                % increase. This suggests that the model is very likely to be
                % overfitting. This could happen when the model is non-regularized or
                % badly regularized.
                stop = true; 
            else
                stop = false;
            end
        end
        function [stats] = statsfun0(Xstar, Xstar_r, Xmat, mcinfo)
        % This function is called by run_GRMF for computing the errors of the
        % solution.
                Xmat_tr = spmask(Xmat, mcinfo.I, mcinfo.J) ;
                Xmat_t = spmask(Xmat, mcinfo.Itest, mcinfo.Jtest);

                temp_tr = GRLRMC.computeSCORES_MC(Xmat_tr-mcinfo.PXtar, mcinfo.PXtar);
                temp_t  = GRLRMC.computeSCORES_MC(Xmat_t-mcinfo.PXtar_t, mcinfo.PXtar_t);

                rmse_all = sqrt( temp_tr(1)^2 *mcinfo.sampl_rate +...
                                 temp_t(1)^2 *(1-mcinfo.sampl_rate));
                temp     = GRLRMC.computeSCORES_MC(nan,[mcinfo.PXtar;...
                                                        mcinfo.PXtar_t], ...
                                                   rmse_all );
                tempr   = nan(size(temp));
                                
                relErr = GRLRMC.computeSCORES_relErr(temp(2), mcinfo);
                stats.MCscores = [temp; temp_tr; temp_t; tempr; relErr];
        end
        function val = compute_resPerIter_relErr(rmse_all, mcinfo)
                val = rmse_all /...
                         sqrt( mean([mcinfo.PXtar;mcinfo.PXtar_t].^2) ) ;             
        end
        function val = compute_resPerIter_RMSE(Delta)
            % MSE = mean(Delta.^2); 
            val = sqrt(mean(Delta.^2));
        end
        function [stats] = statsfun_unified_(optpb, X, stats, store, mcinfo)
            if isfield(X,'G')
                G = X.G; H = X.H;
            elseif isfield(X,'R')
                G = X.L; H = X.R;
            elseif isfield(X,'U')
                G = X.U * X.S;
                H = X.V; 
            end
            if ~isfield(store,'PX')
                store.PX = spmaskmult(G, H', mcinfo.I, mcinfo.J);
            end
            if ~isfield(store,'PX_t')
                store.PX_t = spmaskmult(G, H', mcinfo.Itest,...
                                            mcinfo.Jtest);
            end
            if ~isfield(store,'err_Omega')
                store.err_Omega = store.PX - mcinfo.PXtar;
            end
            rmse_tr = GRLRMC.compute_resPerIter_RMSE(store.err_Omega);
            rmse_te = GRLRMC.compute_resPerIter_RMSE(store.PX_t-mcinfo.PXtar_t);
            ntr     = mcinfo.sz_tr ;
            nte     = mcinfo.sz_te ;
            mse_all     = (rmse_tr^2 *ntr + rmse_te^2 *nte)/(ntr+nte) ;
            rmse_all    = sqrt( mse_all );
            relErr = GRLRMC.compute_resPerIter_relErr(rmse_all, mcinfo);
            
            stats.RMSE_t  = rmse_te ; 
            stats.RMSE    = rmse_all ;
            stats.RMSE_tr = rmse_tr ; 
            stats.relErr  = relErr ;
        end
        function [f, store] = cost_GH_Euclidean(X, store, mcinfo, params_pb, L)
        % The cost function of 
        % about alpha: alpha_c \equiv alpha_r, we penalize the loss function with equal weights by R(G):=betar*Tr(G'G) and R(H):=betac*Tr(H'H). 
        % about Lreg_beta: _betar \equiv _betac, we penalize L_r and L_c.
            if ~isfield(store, 'val')
                if ~isfield(store,'PX')
                    % store.PX = spmask(X.G*(X.H'), mcinfo.I, mcinfo.J);
                    store.PX = spmaskmult(X.G, X.H', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                % note (#1003): replace "Lr" by "alpha_r*I + beta_r*Lr", same for "Lc"
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.G + params_pb.Lreg_betar*L.Lr * X.G;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.H + params_pb.Lreg_betac*L.Lc * X.H; 
                end
                store.val = .5*sum(store.err_Omega.^2) ...
                            + .5*trace((X.G')*store.LG) ...
                            + .5*trace((X.H')*store.LH) ; 
            end
            f = store.val;
        end
        function [precon_dir, store] = precon_GH_Euclidean(X, dir, store)
        % This is the function that preconditions the Euclidean gradient with
        % the following approximate inverse of the Hessian diagonal blocks. 
           if ~isfield(store, 'precon_dir')
               if ~isfield(store,'GtG')
                    store.GtG = (X.G')*X.G; 
                end
                if ~isfield(store,'HtH')
                    store.HtH = (X.H')*X.H; 
                end
                store.precon_dir = struct('G', (dir.G)/store.HtH,...
                                          'H', (dir.H)/store.GtG ) ;
            end
            precon_dir = store.precon_dir ;   
        end
        function [grad, store] = rgrad_GH_Euclidean(X, store, mcinfo, params_pb, L)
        % This function computes the Euclidean gradient. 
            if ~isfield(store, 'grad')
                m = size(X.G,1); n = size(X.H,1);
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.G,(X.H'), mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.G + params_pb.Lreg_betar*L.Lr * X.G;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.H + params_pb.Lreg_betac*L.Lc * X.H; 
                end
                S = sparse(double(mcinfo.I), double(mcinfo.J), store.err_Omega,...
                            mcinfo.size_M(1), mcinfo.size_M(2));

                store.grad = struct('G', (S*X.H + store.LG),...
                                    'H', ((S')*X.G + store.LH));
            end
            grad = store.grad ;   
        end
        function [hess, store] = hess_GH_Euclidean(X, dir, store, mcinfo, params_pb, L)
        % This function returns the Euclidean Hessian of f along a direction.  
            if ~isfield(store, 'hess')
                m = size(X.G,1); n = size(X.H,1);
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.G,(X.H'), mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LrdirG')
                    store.LrdirG= params_pb.alpha_r*dir.G + params_pb.Lreg_betar*L.Lr * dir.G;
                end
                if ~isfield(store,'LcdirH')
                    store.LcdirH = params_pb.alpha_c*dir.H + params_pb.Lreg_betac*L.Lc * dir.H; 
                end
                if ~isfield(store,'pomega_dirGH_GdirH')
                     store.pomega_dirGH_GdirH = ...
                     sparse(double(mcinfo.I),double(mcinfo.J),...
                            spmaskmult(dir.G,(X.H'), mcinfo.I,mcinfo.J) +...
                            spmaskmult(X.G,(dir.H'), mcinfo.I,mcinfo.J),...
                            mcinfo.size_M(1), mcinfo.size_M(2)) ;
                end
                S = sparse(double(mcinfo.I), double(mcinfo.J), store.err_Omega,...
                            mcinfo.size_M(1), mcinfo.size_M(2)) ;
                store.hess = struct('G', (store.pomega_dirGH_GdirH*X.H + store.LrdirG +...
                                          S*dir.H), ...
                                     'H', ((store.pomega_dirGH_GdirH')*X.G + store.LcdirH +...
                                          (S')*dir.G) );
            end
            hess = store.hess ;  
        end
        function [tmin, store] = linesearch_initialpt(X, dir, store, mcinfo, params_pb, L)
        % This function computes the initial step-size by exact line
        % minimization in the partial direction on the tangent space at x.
            if ~isfield(X,'G')
                G = X.L; H = X.R;
                dirG = dir.L; dirH = dir.R;
            else
                G = X.G; H = X.H;
                dirG = dir.G; dirH = dir.H;
            end
            if ~isfield(store,'PX')
                store.PX = spmaskmult(G, H', mcinfo.I, mcinfo.J);
            end
            if ~isfield(store,'err_Omega')
                store.err_Omega = store.PX - mcinfo.PXtar;
            end
            if ~isfield(store, 'GtG')
                store.GtG = (G') * G;
            end
            if ~isfield(store, 'HtH')
                store.HtH = (H') * H;
            end
            if ~isfield(store,'LG')
                store.LG = params_pb.alpha_r*G + params_pb.Lreg_betar*L.Lr * G;
            end
            if ~isfield(store,'LetaG')
                store.LetaG = params_pb.alpha_r*dirG + params_pb.Lreg_betar*L.Lr * dirG;
            end
            if ~isfield(store,'LH')
                store.LH = params_pb.alpha_c*H + params_pb.Lreg_betac*L.Lc * H; 
            end
            if ~isfield(store,'LetaH')
                store.LetaH = params_pb.alpha_c*dirH + params_pb.Lreg_betac*L.Lc * dirH;
            end
            
            if ~isfield(store,'pomega_xeta')
                % store.pomega_xeta = spmask( G*(dirH') + dirG *(H'), mcinfo.I, mcinfo.J);
                store.pomega_xeta = spmaskmult(G, dirH', mcinfo.I,mcinfo.J) + ...
                                    spmaskmult(dirG, H', mcinfo.I, mcinfo.J);
            end
            if ~isfield(store,'pomega_eta')
                % store.pomega_eta = spmask( dirG*(dirH') , mcinfo.I, mcinfo.J);
                store.pomega_eta = spmaskmult(dirG, dirH', mcinfo.I,mcinfo.J);
            end

            if ~isfield(store,'etaGtLG')
                store.etaGtLG = (dirG') * store.LG;
            end
            if ~isfield(store,'etaGtLetaG')
                store.etaGtLetaG = (dirG') *store.LetaG;
            end

            if ~isfield(store,'etaHtLH')
                store.etaHtLH = (dirH') * store.LH;
            end
            if ~isfield(store,'etaHtLetaH')
                store.etaHtLetaH = (dirH') *store.LetaH;
            end
            %if 1
            c1 = (store.err_Omega') * store.pomega_xeta + ...
            trace(store.etaGtLG + store.etaHtLH);
            
            c2 = 0.5*sum(store.pomega_xeta.^2) + (store.err_Omega')*store.pomega_eta +...
                0.5*trace(store.etaGtLetaG + store.etaHtLetaH);
            
            c3 = (store.pomega_xeta')* store.pomega_eta ;

            c4 = 0.5*sum(store.pomega_eta.^2) ;
            
            c = [c1,c2,c3,c4];
            if ~any(isnan(c))
                ts = roots(fliplr([1:4].*c));
                ts = ts(imag(ts)==0);
                ts = ts(ts>0);
                
                if isempty(ts)
                    ts = 1;
                end
                dfts = polyval([c4 c3 c2 c1 0], ts);
                [~, iarg] = min(dfts);
                tmin = ts(iarg);
            else
                tmin = 10;
            end
        end
        function [f, store] = cost_GH_qpreconExa(X, store, mcinfo, params_pb, L)
        % This function computes the cost function of the GRMF problem, same as
        % in [Rao et al. 2015]. 
            if ~isfield(store, 'val')
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
                end
                store.val = (.5*sum(store.err_Omega.^2) ...
                                + .5*trace(X.L'*store.LG) ... 
                                + .5*trace(X.R'*store.LH));
            end
            f = store.val;
        end
        function [grad, store] = rgrad_GH_qpreconExa(X, store, mcinfo, params_pb, L)
        % This function computes the Riemannian gradient of f w.r.t. the
        % preconditioned metric.
            if ~isfield(store, 'grad')
                m = size(X.L,1); n = size(X.R,1);
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
                end
                if ~isfield(store, 'GtG')
                    store.GtG = (X.L') * X.L;
                end
                if ~isfield(store, 'HtH')
                    store.HtH = (X.R') * X.R;
                end

                S = sparse(double(mcinfo.I), double(mcinfo.J), store.err_Omega, m, n );
                store.grad = struct('L', (S*X.R + store.LG)/store.HtH,...
                                    'R', (S'*X.L+ store.LH)/store.GtG);
            end
            grad = store.grad ;   
        end
        function [grad, store] = rgrad_GH_qpreconLeqk(X, store, mcinfo, params_pb, L)
            if ~isfield(store, 'grad')
                m = size(X.L,1); n = size(X.R,1);
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
                end
                if ~isfield(store, 'GtG')
                    store.GtG = (X.L') * X.L;
                end
                if ~isfield(store, 'HtH')
                    store.HtH = (X.R') * X.R;
                end

                S = sparse(double(mcinfo.I), double(mcinfo.J), store.err_Omega, m, n );
                HtH = store.HtH + 1e-4*eye(params_pb.rank);
                GtG = store.GtG + 1e-4*eye(params_pb.rank);
                store.grad = struct('L', (S*X.R + store.LG)/HtH,...
                                    'R', (S'*X.L+ store.LH)/GtG);
            end
            grad = store.grad ;   
        end
        function [f, store] = cost_GH_qleftinvExa(X, store, mcinfo, params_pb, L)
        % This function computes the cost function of the GRMF problem, same as
        % in [Rao et al. 2015]. 
            if ~isfield(store, 'val')
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
                end
                store.val = (.5*sum(store.err_Omega.^2) ...
                                + .5*trace(X.L'*store.LG) ... 
                                + .5*trace(X.R'*store.LH));
            end
            f = store.val;
        end
        function [grad, store] = rgrad_GH_qleftinvExa(X, store, mcinfo, params_pb, L)
        % This function computes the Riemannian gradient of f w.r.t. the
        % right-invariant metric.
            if ~isfield(store, 'grad')
                m = size(X.L,1); n = size(X.R,1);
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
                end
                if ~isfield(store, 'GtG')
                    store.GtG = (X.L') * X.L;
                end
                if ~isfield(store, 'HtH')
                    store.HtH = (X.R') * X.R;
                end

                S = sparse(double(mcinfo.I), double(mcinfo.J), store.err_Omega, m, n );
                store.grad = struct('L', (S*X.R + store.LG)*store.GtG,...
                                    'R', (S'*X.L+ store.LH)*store.HtH );
            end
            grad = store.grad ;   
        end
        function [grad, store] = rgrad_GH_qleftinvLeqk(X, store, mcinfo, params_pb, L)
        % This function computes the Riemannian gradient of f w.r.t. the
        % right-invariant metric with a (potentially nonzero) delta parameter.
        % In this present version, delta = 1e-8. 
            if ~isfield(store, 'grad')
                m = size(X.L,1); n = size(X.R,1);
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
                end
                if ~isfield(store, 'GtG')
                    store.GtG = (X.L') * X.L;
                end
                if ~isfield(store, 'HtH')
                    store.HtH = (X.R') * X.R;
                end
                S = sparse(double(mcinfo.I), double(mcinfo.J), store.err_Omega, m, n );
                GtG = store.GtG + 1e-8 * eye(params_pb.rank);
                HtH = store.HtH + 1e-8 * eye(params_pb.rank);
                store.grad = struct('L', (S*X.R + store.LG)*GtG,...
                                    'R', (S'*X.L+ store.LH)*HtH );
            end
            grad = store.grad ;   
        end
        function [f, store] = cost_GH_qprecon(X, store, mcinfo, params_pb, L)
        % This function computes the cost function of the GRMF problem, same as
        % in [Rao et al. 2015]. 
            if ~isfield(store, 'val')
                if ~isfield(store,'PX')
                    % store.PX = spmask(X.L*(X.R'), mcinfo.I, mcinfo.J);
                    store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                % note (#1003): replace "Lr" by "alpha_r*I + beta_r*Lr", same for "Lc"
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
                    % store.LG = L.Lr * X.L;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
                    % L.Lc * X.R;
                end
                if ~isfield(store, 'GtG')
                    store.GtG = (X.L') * X.L;
                end
                if ~isfield(store, 'HtH')
                    store.HtH = (X.R') * X.R;
                end
                store.val = (.5*sum(store.err_Omega.^2) ...
                                + .5*trace(store.HtH*(X.L'*store.LG)) ... 
                                + .5*trace(store.GtG*(X.R'*store.LH)));
           end
            f = store.val;
        end
        function [grad, store] = rgrad_GH_qprecon(X, store, mcinfo, params_pb, L)
        % This function computes the Riemannian gradient of f w.r.t. the
        % preconditioned metric.
            if ~isfield(store, 'grad')
                m = size(X.L,1); n = size(X.R,1);
                if ~isfield(store,'PX')
                    store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
                end
                if ~isfield(store,'err_Omega')
                    store.err_Omega = store.PX - mcinfo.PXtar;
                end
                if ~isfield(store,'LG')
                    store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
                end
                if ~isfield(store,'LH')
                    store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
                end
                if ~isfield(store, 'GtG')
                    store.GtG = (X.L') * X.L;
                end
                if ~isfield(store, 'HtH')
                    store.HtH = (X.R') * X.R;
                end

                S = sparse(double(mcinfo.I), double(mcinfo.J), store.err_Omega, m, n );
                store.grad = struct('L', ((S*X.R +X.L*(X.R'*store.LH))/store.HtH...
                                           + store.LG),...
                                    'R', ((S'*X.L+X.R*(X.L'*store.LG))/store.GtG...
                                           + store.LH) );
            end
            grad = store.grad ;   
        end
        function [tmin, store] = lsfun_twofactors_Lipschitz(X, dir, store, mcinfo, params_pb, L)
        % This function computes nabla f(X) and nabla f(X+dir) 
        % nabla f(X) = (SH+LrG, S'G + LcH) 
            if ~isfield(X,'G')
                G = X.L; H = X.R;
                dirG = dir.L; dirH = dir.R;
            else
                G = X.G; H = X.H;
                dirG = dir.G; dirH = dir.H;
            end
            m = size(G,1); n = size(H, 1); 
            if ~isfield(store,'PX')
                store.PX = spmaskmult(G, H', mcinfo.I, mcinfo.J);
            end
            if ~isfield(store,'err_Omega')
                store.err_Omega = store.PX - mcinfo.PXtar;
            end
            PXb         = spmaskmult(G+dirG, (H+dirH)', mcinfo.I, mcinfo.J); 
            errb_Omega  = PXb - mcinfo.PXtar; 
            LetaG       = params_pb.alpha_r*dirG + params_pb.Lreg_betar*L.Lr * dirG;
            LetaH       = params_pb.alpha_c*dirH + params_pb.Lreg_betar*L.Lc * dirH;

            Sb = sparse(double(mcinfo.I), double(mcinfo.J), errb_Omega , m, n );
            diffS = sparse(double(mcinfo.I), double(mcinfo.J), errb_Omega - store.err_Omega, m, n );
            DeltaG = diffS*H + Sb*dirH + LetaG;
            DeltaH = diffS'*G + Sb'*dirG + LetaH;
            
            tmin =sqrt( (sum(DeltaG(:).^2) + sum(DeltaH(:).^2))/(sum(dirG(:).^2)+sum(dirH(:).^2)) ) ;
        end
        function [tmin, store, ts, dfts] = lsfun_GH_qprecon(X, dir, store, mcinfo, params_pb, L)
        % Compute the initial step-size
        % Exact line search in the direction of dir on the tangent space of x. 
        % Given the polynomial of degree 4 and its derivative 
        % P'(t) = c_1 + 2*c_2 *t + 3*c_3 t^2 + 4*c_4 t^3, 
        % the roots of P' can be computed using "roots([1:4].*c)", where c = [c_1,..,c_4].
        % the exact line minimization.
            if ~isfield(store,'PX')
                store.PX = spmaskmult(X.L, X.R', mcinfo.I, mcinfo.J);
            end
            if ~isfield(store,'err_Omega')
                store.err_Omega = store.PX - mcinfo.PXtar;
            end
            if ~isfield(store, 'GtG')
                store.GtG = (X.L') * X.L;
            end
            if ~isfield(store, 'HtH')
                store.HtH = (X.R') * X.R;
            end
            if ~isfield(store,'LG')
                store.LG = params_pb.alpha_r*X.L + params_pb.Lreg_betar*L.Lr * X.L;
            end
            if ~isfield(store,'LetaG')
                store.LetaG = params_pb.alpha_r*dir.L + params_pb.Lreg_betar*L.Lr * dir.L;
            end
            if ~isfield(store,'LH')
                store.LH = params_pb.alpha_c*X.R + params_pb.Lreg_betac*L.Lc * X.R; 
            end
            if ~isfield(store,'pomega_xeta')
                store.pomega_xeta = spmaskmult( X.L, dir.R', mcinfo.I, mcinfo.J)+...
                                    spmaskmult( dir.L, X.R', mcinfo.I, mcinfo.J);
            end
            if ~isfield(store,'pomega_eta')
                % store.pomega_eta = spmask( dir.L*(dir.R') , mcinfo.I, mcinfo.J);
                store.pomega_eta = spmaskmult( dir.L,(dir.R') , mcinfo.I, mcinfo.J);
            end

            if ~isfield(store,'GtLG')
                store.GtLG = (X.L') * store.LG;
            end
            if ~isfield(store,'etaGtLG')
                store.etaGtLG = (dir.L') * store.LG;
            end
            if ~isfield(store,'etaGtLetaG')
                store.etaGtLetaG = (dir.L') *store.LetaG;
            end

            if ~isfield(store,'etaHtH')
                store.etaHtH = (dir.R') *X.R;
            end
            if ~isfield(store,'etaHtetaH')
                store.etaHtetaH = (dir.R') *dir.R;
            end
            if 1
            c1 = (store.err_Omega') * store.pomega_xeta + ...
            trace(store.GtLG * store.etaHtH + store.etaGtLG'*store.HtH);
            
            c2 = 0.5*sum(store.pomega_xeta.^2) + (store.err_Omega')*store.pomega_eta +...
                0.5*trace(store.GtLG * store.etaHtetaH +store.etaGtLG*store.etaHtH +...
                            (store.etaGtLG')*(store.etaHtH') + store.etaGtLetaG * store.HtH)+...
                trace( store.etaGtLG * (store.etaHtH') );
            
            c3 = (store.pomega_xeta')* store.pomega_eta + ...
                trace((store.etaGtLG')*store.etaHtetaH + store.etaGtLetaG * store.etaHtH);

            c4 = 0.5*sum(store.pomega_eta.^2) + 0.5*trace(store.etaGtLetaG*store.etaHtetaH);
            
            c = [c1,c2,c3,c4];
            ts = roots(fliplr([1:4].*c));
            % Debug: MATLAB's built-in function "isreal" does NOT test element-wise values but only the first one's!
            % ts = ts(isreal(ts));
            ts = ts(imag(ts)==0);
            ts = ts(ts>0);
            
            if isempty(ts)
            % No real positive solution to the above "approximate line min.", which can never happen actually; c.f. [(book) Pytlak: conjugate gradient methods for nonconvex optimization pbs], keywords "line search", "strong Wolfe condition" etc.
                ts = 1;
            end
            dfts = polyval([c4 c3 c2 c1 0], ts);
            [~, iarg] = min(dfts);
            tmin = ts(iarg);
            else
            tmin = 100;
            end
        end
        function funhandles = construct_pb(manifoldname, mcinfo, params_pb, L)
            switch manifoldname
                 case 'GH_qleftinvLeqk' 
                    man    = fixedrankfactory_2factors(mcinfo.size_M(1),mcinfo.size_M(2), params_pb.rank);
                    man.name    = @() manifoldname;
                    
                    funhandles = struct('man',man,...
                    'costfun',  @(X, store)GRLRMC.cost_GH_qleftinvExa(X, store, mcinfo, params_pb, L),...
                    'gradfun', @(X, store)GRLRMC.rgrad_GH_qleftinvLeqk(X, store, mcinfo, params_pb, L),...
                    'lsfun', @(X, dir, store)GRLRMC.linesearch_initialpt(X, dir,...
                               store, mcinfo, params_pb, L),...
                    'stopfun', @(optpb, X, info, last)GRLRMC.stopfun_manopt(optpb, X, info,...
                                                      last) );                
                case 'GH_qpreconLeqk' 
                    man    = fixedrankfactory_2factors_preconditioned(mcinfo.size_M(1),mcinfo.size_M(2), params_pb.rank);
                    man.name    = @() manifoldname;
                    funhandles = struct('man',man,...
                    'costfun',  @(X, store)GRLRMC.cost_GH_qpreconExa(X, store, mcinfo, params_pb, L),...
                    'gradfun', @(X, store)GRLRMC.rgrad_GH_qpreconLeqk(X, store, mcinfo, params_pb, L),...
                    'lsfun', @(X, dir, store)GRLRMC.linesearch_initialpt(X, dir,...
                               store, mcinfo, params_pb, L),...
                    'stopfun', @(optpb, X, info, last)GRLRMC.stopfun_manopt(optpb, X, info,...
                                                      last) );                
                case 'GH_qpreconExa' 
                    man    = fixedrankfactory_2factors_preconditioned(mcinfo.size_M(1),mcinfo.size_M(2), params_pb.rank);
                    man.name    = @() manifoldname;
                    funhandles = struct('man',man,...
                    'costfun',  @(X, store)GRLRMC.cost_GH_qpreconExa(X, store, mcinfo, params_pb, L),...
                    'gradfun', @(X, store)GRLRMC.rgrad_GH_qpreconExa(X, store, mcinfo, params_pb, L),...
                    'lsfun', @(X, dir, store)GRLRMC.linesearch_initialpt(X, dir,...
                               store, mcinfo, params_pb, L),...
                    'stopfun', @(optpb, X, info, last)GRLRMC.stopfun_manopt(optpb, X, info,...
                                                      last) );
                 case 'GH_qleftinvExa' 
                    man    = fixedrankfactory_2factors(mcinfo.size_M(1),mcinfo.size_M(2), params_pb.rank);
                    man.name    = @() manifoldname;
                    
                    funhandles = struct('man',man,...
                    'costfun',  @(X, store)GRLRMC.cost_GH_qleftinvExa(X, store, mcinfo, params_pb, L),...
                    'gradfun', @(X, store)GRLRMC.rgrad_GH_qleftinvExa(X, store, mcinfo, params_pb, L),...
                    'lsfun', @(X, dir, store)GRLRMC.linesearch_initialpt(X, dir,...
                               store, mcinfo, params_pb, L),...
                    'stopfun', @(optpb, X, info, last)GRLRMC.stopfun_manopt(optpb, X, info,...
                                                      last) );
                case 'GH_EucCruderls' 
                    man = productmanifold( struct('G', euclideanfactory(mcinfo.size_M(1), params_pb.rank), 'H', euclideanfactory(mcinfo.size_M(2), params_pb.rank)) );
                    man.name    = @() manifoldname;
                    
                    funhandles = struct('man', man, ...
                    'costfun', @(X, store)GRLRMC.cost_GH_Euclidean(X, store, mcinfo, params_pb, L),...
                    'gradfun', @(X, store)GRLRMC.rgrad_GH_Euclidean(X, store, mcinfo, params_pb, L), ...
                    'stopfun', @(optpb, X, info, last)GRLRMC.stopfun_manopt(optpb, X, info,...
                                                      last),...
                    'hessfun', @(X, dir, store)GRLRMC.hess_GH_Euclidean(X, dir, store, mcinfo, params_pb, L),...
                    'precon',  @(X, dir, store)GRLRMC.precon_GH_Euclidean(X, dir, store));
                 case 'GH_qprecon' 
                    man    = fixedrankfactory_2factors_preconditioned(mcinfo.size_M(1),mcinfo.size_M(2), params_pb.rank);
                    man.name    = @() manifoldname;
                    
                    funhandles = struct('man',man,...
                    'costfun',  @(X, store)GRLRMC.cost_GH_qprecon(X, store, mcinfo, params_pb, L),...
                    'gradfun', @(X, store)GRLRMC.rgrad_GH_qprecon(X, store, mcinfo, params_pb, L),...
                    'stopfun', @(optpb, X, info, last)GRLRMC.stopfun_manopt(optpb, X, info,...
                                                      last),...
                    'lsfun', @(X, dir, store)GRLRMC.lsfun_GH_qprecon(X, dir, store, mcinfo, params_pb, L) );
                case {GRLRMC.NAMES_MAN{1}, 'GH_EUC'}
                    man = productmanifold( struct('G', euclideanfactory(mcinfo.size_M(1), params_pb.rank), 'H', euclideanfactory(mcinfo.size_M(2), params_pb.rank)) );
                    man.name    = @() manifoldname;
                     
                    funhandles = struct('man', man, ...
                    'costfun', @(X, store)GRLRMC.cost_GH_Euclidean(X, store, mcinfo, params_pb, L),...
                    'gradfun', @(X, store)GRLRMC.rgrad_GH_Euclidean(X, store, mcinfo, params_pb, L), ...
                    'stopfun', @(optpb, X, info, last)GRLRMC.stopfun_manopt(optpb, X, info,...
                                                      last),...
                                                      'lsfun', @(X, dir, store)GRLRMC.linesearch_initialpt(X, dir, store, mcinfo, params_pb, L),...
                                                      'hessfun', @(X, dir, store)GRLRMC.hess_GH_Euclidean(X, dir, store, mcinfo, params_pb, L),...
                    'precon',  @(X, dir, store)GRLRMC.precon_GH_Euclidean(X, dir, store));
                case 'USV_embedded' 
                    error(sprintf('%s is a deprecated manifold structure for GRMC. Please use Qprecon, Euclidean, AltMin or GRALS.\n', manifoldname) );
                otherwise
                    error(sprintf('%s is not implemented yet in Manopt..\n', manifoldname) );
            end
            funhandles.statsfun = @(optpb, X, stats,...
                store)GRLRMC.statsfun_unified_(optpb, X, stats, store, mcinfo); 
        end
    end % (end methods static)

    methods
        function self = GRLRMC(manifoldname, Xtar, mcinfo, params_pb, L, Xtar_r, params_opt)
            if nargin < 1
                manifoldname = GRLRMC.NAMES_MAN{1};
            end
            if ~any(strcmp(manifoldname, GRLRMC.NAMES_MAN))
                manifoldname = GRLRMC.NAMES_MAN{1};
            end
            if nargin < 2
                Xtar = GRLRMC.gendata_LR(300,300,10);
            end
            [m n] = size(Xtar);
            if nargin < 3
                mcinfo = GRLRMC.genOMEGA_MC(Xtar, params_pb.sampl_rate);
            end
            if nargin < 4
                params_pb = GRLRMC.default_pb; 
            end
            if nargin < 5
                L = struct('Lr', sparse(m,m), 'Lc', sparse(n,n) ); 
            end
            if nargin < 6
                Xtar_r = Xtar;
            end
            if nargin < 7
            % Define the default options of the method.
                params_opt  = Solver.default_opt;
            end
            fprintf('all elements constructed...\n');
            self = self@Solver(manifoldname, mcinfo, params_pb, L, params_opt);

            self.Xstar      = Xtar;
            self.Xstar_r    = Xtar_r;
            self.params_pb  = params_pb;
            self.mcinfo     = mcinfo;
            self.L          = L;
        end
        function self = set_params_opt(self, varargin)
            if length(varargin)>1
                for i = 1 : 2 : length(varargin)
                    self.params_opt.(varargin{i}) = varargin{i+1};
                end
            end
        end
        function self = set_params_pb(self, varargin)
        % This function allows setting up new parameter(s) in a standalone
        % manner. 
            parser = GRLRMC.build_parser(self.params_pb);
            parse(parser, varargin{:});
            fds = fieldnames(parser.Results);
            for i = 1 : length(fds)
                self.params_pb.(fds{i}) = parser.Results.(fds{i});
            end
            self.params_pb.pid = self.params_pb.pid + 1000;
            self.refresh_GRLRMC([], self.params_pb, []);
        end
        function self = loadnew_paramspb(self, params_pb)
        % This function is designed to be called by a subclass's function. 
            self.refresh_GRLRMC([], [], params_pb, []);
        end
        function self = refresh_GRLRMC(self, manifold_name, mcinfo, params_pb, L)
            if nargin < 2
                manifold_name = [];
            end
            if nargin < 3
                mcinfo = [];
            end
            if nargin < 4
                params_pb = [];
            end
            if nargin < 5
                L = [];
            end
            if isempty(manifold_name)
                manifold_name = self.manifold.name();
            end
             if isempty(mcinfo)
                mcinfo = self.mcinfo;
            end
            if isempty(params_pb)
                params_pb = self.params_pb;
            end
            if isempty(L)
                L = self.L;
            end
            % %Replaced Solver/refreshSolver by Solver/loadnew_funhandles
            % [man, costfun, gradfun, ...
            % statsfun, lsfun, hessfun] = GRLRMC.construct_pb(manifold_name, mcinfo, params_pb, L);
            % self.refreshSolver(man, costfun, gradfun, statsfun, lsfun, hessfun);  
            self.loadnew_funhandles(GRLRMC.construct_pb(manifold_name, mcinfo, params_pb, L) );
            self.params_pb = params_pb;
            self.mcinfo = mcinfo;
            self.L = L;
        end
      
        % Interfacing to the mexfunction glr_mf_train. [grmf-exp: Rao et
        % al.'15].
        function [X, stats, self] = runGRMF(self, method_init, varargin)
            method_init = self.params_opt.method_init;
            methodname = Solver.NAMES_ALGO{1};
            Lreg_L = self.params_pb.alpha_r*speye(self.data.dims(1))...
                        + self.params_pb.Lreg_betar*self.L.Lr;
            Lreg_Lc = self.params_pb.alpha_c*speye(self.data.dims(2))...
                        + self.params_pb.Lreg_betac*self.L.Lc;
            if strcmp(method_init,'random')
                [W, H, rmse, walltime, fobj, ncgs] = glr_mf_train(...
                                                [double(self.mcinfo.I), ...
                                                double(self.mcinfo.J), ...
                                                self.mcinfo.PXtar],...
                                                [double(self.mcinfo.Itest),...
                                                double(self.mcinfo.Jtest),...
                                                self.mcinfo.PXtar_t],... 
                                                sparse(Lreg_L),...
                                                sparse(Lreg_Lc),...
                                                sprintf('-n 1 -e %.9f -t %d -g %d -z %d -k %d',...
                                                self.params_opt.grmf_eps,...
                                                self.params_opt.grmf_maxiter,...
                                                self.params_opt.grmf_maxiter_cg,...
                                                self.params_opt.maxtime,...
                                                self.params_pb.rank)...
                                                );
            else
                % Give pre-defined Winit, Hinit using the initialization method
                % method_init. 
                t0 = tic;
                X0 = self.initialization(method_init);
                if isfield(X0, 'U')
                    W0 = (X0.U * sqrt(X0.S))'; H0 = (X0.V * sqrt(X0.S))'; 
                elseif isfield(X0, 'G')
                    W0 = (X0.G)'; H0 = (X0.H)'; 
                elseif isfield(X0, 'L')
                    W0 = (X0.L)'; H0 = (X0.R)';
                else
                    fprintf('grmf: init format incompatible!\n'); return;
                end
                t_init = toc(t0); 
                % // if nrhs == 6 or 7 => glr_mf_train(Y, testY, A, B, W, H, 'options')
                [W, H, rmse, walltime, fobj, ncgs] = glr_mf_train([double(self.mcinfo.I), ...
                                                    double(self.mcinfo.J), ...
                                                    self.mcinfo.PXtar],...
                                                    [double(self.mcinfo.Itest),...
                                                    double(self.mcinfo.Jtest),...
                                                    self.mcinfo.PXtar_t],... 
                                                    sparse(Lreg_L),...
                                                    sparse(Lreg_Lc),...
                                                    W0, H0, ...
                                                    sprintf('-n 1 -e %.9f -t %d -g %d -z %d -k %d',...
                                                    self.params_opt.grmf_eps,...
                                                    self.params_opt.grmf_maxiter,...
                                                    self.params_opt.grmf_maxiter_cg,...
                                                    self.params_opt.maxtime,...
                                                    self.params_pb.rank)...
                                                    );
            end
            X       = struct('G',W','H',H');
            stats = self.statsfun0(self.Xstar, self.Xstar_r, (W')*H, self.mcinfo);
            stats.time = walltime + t_init;
            stats.RMSE_t = rmse;
            stats.cost = fobj;
            stats.ncgGPerI = ncgs(1,:);
            stats.ncgHPerI = ncgs(2,:);
        end 
        function runpltGRMF_RMSEtPerIter(self, optlog, varargin)
            semilogy(optlog.cputime, optlog.rmse);
        end
        function h = runplt_statsPerIter(self, varargin)
        % Default mode: 1 figure with 3 curves: {RMSE, RMSEtr, RMSEt} vs #iter
            enum = {'RMSE', 'RMSE:train', 'RMSE:test'};
            h(1) = figure();
            self.plot_statsPerIter('MCscores', ...
                                    struct('labelnames',{enum},...
                                            'ids', [1,6,11]));
            h(2) = figure();
            self.plot_statsPerIter('gradnorm');
        end
    end
end
