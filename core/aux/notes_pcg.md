This file contains notes on the internal parameters in the Matlab built-in function pcg.m.   
Meaning of flag values in the Documentation

===========================================================================================
Flag  Convergence
0     pcg converged to the desired tolerance tol within maxit iterations.
1     pcg iterated maxit times but did not converge.
2     Preconditioner M was ill-conditioned.
3     pcg stagnated. (Two consecutive iterates were the same.)
4     One of the scalar quantities calculated during pcg became too small or too large to continue computing.

===========================================================================================

INPUT: tol, x0. 

### Notes on stop
- `normr = norm(r)`, that is, $\|r_k\|_2$, for $k\geq 0$. 
- `tolb = tol * n2b`, that is, $\tilde{\epsilon} = \epsilon \|b\|_2$. 

Remark: With this notation, `normr <= tolb` is equivalent to $\|r_k\| \leq \epsilon \|b\|_2= \epsilon \|r(x=0)\|$. However, when x0 is not 0, then the initial residual r_0 = r(x=x0) is not r(x=0)=b. 
In order to extend the criterion $\|r_k\|_2 \leq \|r(x=0)\|$ to $\|r_k\|_2 \leq \|r(x=x0)\|$, one needs to
use another "relative tolerance" parameter; Let `tolrel` ($\tilde{\epsilon}^{\text{rel}}$) denote this new parameter. It should be $\tilde{\epsilon}^{\text{rel}} = \epsilon \|b-Ax0\|_2$, that is, `tolrel = tol*norm(b-Ax0)`.

**update** from pcg.m to pcg_2.m: `normr = norm(r), n2b = norm(b-A*x0)`.  

```
+%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
+% compute r0 and norm(r0)
+r0 = b - iterapp('mtimes',afun,atype,afcnstr,x,varargin{:});
+n2b = norm(r0);
+%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 if ((nargin > 7) && strcmp(atype,'matrix') && ...
         strcmp(m1type,'matrix') && strcmp(m2type,'matrix'))
@@ -157,7 +162,7 @@ flag = 1;
 xmin = x;                          % Iterate which has minimal residual so far
 imin = 0;                          % Iteration at which xmin was computed
 tolb = tol * n2b;                  % Relative tolerance
-r = b - iterapp('mtimes',afun,atype,afcnstr,x,varargin{:});
+r = r0; %b - iterapp('mtimes',afun,atype,afcnstr,x,varargin{:});
 normr = norm(r);                   % Norm of residual
 normr_act = normr;

```

1. Convergence of type 0 (flag=0)

- a. "when the initial guess is a good enough solution": Line 164. 
     ```matlab
     if (normr <= tolb)                 % Initial guess is a good enough solution
         flag = 0;
         relres = normr / n2b;
         iter = 0;
         resvec = normr;
         if (nargout < 2)
             itermsg('pcg',tol,maxit,0,flag,iter,relres);
         end
         return
     end
     ```
    
- b. "check for convergence" `if (normr <= tolb || stag >= maxstagsteps || moresteps)`
  ```matlab
       if (normr_act <= tolb)
            flag = 0;
            iter = ii;
            break
 
  ```



### Influence of max_iter_cg
It limits the nb of CG iterations when the tolerance parameter $epsilon$ turn out to be too small at the iterate in question.
Below the warning message appears when `iter >= max_cg_iter`: 

> Algorithm:AltMin1, start solving problem(alpha(0.000000), betar(0.000000))
>  iter         RMSE val      grad. norm
>     0   +1.08883256e-01 3.53957898e+02
>     1   +1.97253798e-02 1.85391416e+00
> Warning: Input tol may not be achievable by PCG
>          Try to use a bigger tolerance
> > In pcg_2 (line 270)
>   In Solver/solve_grls_H (line 64)
>   In Solver/solve_AltMin (line 80)
>   In Tester/runsolver_fromListMethods (line 55)
>   In Tester/run_tester_2 (line 118)
>   In demo_mc_largemn_1SR (line 173)
>     2   +7.99465044e-03 9.29104625e-01
> Warning: Input tol may not be achievable by PCG
>          Try to use a bigger tolerance
> > In pcg_2 (line 270)
>   In Solver/solve_grls_H (line 64)
>   In Solver/solve_AltMin (line 80)
>   In Tester/runsolver_fromListMethods (line 55)
>   In Tester/run_tester_2 (line 118)
>   In demo_mc_largemn_1SR (line 173)
>     3   +9.53968863e-04 1.04967358e-01
>     4   +1.85069430e-04 1.99479560e-02
>     5   +4.16222087e-05 4.42411686e-03
>     6   +9.91461141e-06 1.04251701e-03
>     7   +2.43571596e-06 2.53770026e-04
>     8   +6.10591555e-07 6.30766686e-05
>     9   +1.55397316e-07 1.59202450e-05
>    10   +4.00416835e-08 4.06808911e-06
>    11   +1.04290148e-08 1.05061847e-06
>    12   +2.74261572e-09 2.73938246e-07
>    13   +7.27663550e-10 7.20615869e-08
>    14   +1.94650124e-10 1.91146025e-08
>    15   +5.24659789e-11 5.11017868e-09
>    16   +1.42412707e-11 1.37632840e-09
>    17   +3.89059676e-12 3.73271376e-10
>    18   +1.06912688e-12 1.01897376e-10
>    19   +2.95346990e-13 2.80139584e-11
>    20   +8.19782759e-14 7.83958019e-12
>    21   +2.28500397e-14 2.51322073e-12
>    22   +6.39728288e-15 1.40128833e-12
>    23   +1.80242303e-15 1.11337029e-12
>    24   +5.27417491e-16 9.62165996e-13
> Gradient norm tolerance reached; options.tolgradnorm = 1e-12.
> Total time is 16.615893 [s] (excludes statsfun)
> 
> 



