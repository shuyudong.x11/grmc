/*=================================================================
% function D = com2_egradf(S, I, J, G, H)
% Computes (S*H,S'*G) and returns the result in
% a mr-by-1 real double array D. Here the S is not given as a sparse m-by-n matrix but as an 1D array of size k1-by-1. 

% I and J must be UINT32 matrices of size k1-by-1.
%
% S: k1-by-1, real, double
% I: k1-by-1 row indices, uint32
% J: k1-by-1 column indices, uint32
% G: m-by-r, real, double
% H: n-by-r, real, double
%
% Complexity: O(2*k1*r)
% 
% Contact: Shuyu Dong, UCLouvain (shuyu.dong@uclouvain.be). 
*=================================================================*/

/* #include <math.h> */
#include "mex.h"
#include "matrix.h"

/* Input Arguments */

#define	pS	prhs[0]
#define	pI	prhs[1]
#define	pJ	prhs[2]
#define	pG	prhs[3]
#define	pH	prhs[4]

/* Output Arguments */

#define	pDG	plhs[0]
#define	pDH	plhs[1]

void mexFunction(
          int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray* prhs[] )
{
    uint32_T *I, *J;
    double *S, *G, *H, *DG, *DH;
    mwSize m, n, r, k1, k2, numit;
    mwIndex k, it;


    /* Check the dimensions of input arguments */ 
    m = mxGetM(pG);
    n = mxGetM(pH);
    r = mxGetN(pH);
    k1 = mxGetM(pI);
    k2 = mxGetN(pI);

    /* Get pointers to the data in A, B, I, J */
    S = mxGetPr(pS);
    I = (uint32_T*) mxGetData(pI);
    J = (uint32_T*) mxGetData(pJ);
    G = mxGetPr(pG);
    H = mxGetPr(pH);
     
    /* Create a matrix for the ouput argument */ 
    pDG = mxCreateDoubleMatrix(m*r, 1, mxREAL);
    pDH = mxCreateDoubleMatrix(n*r, 1, mxREAL);
    if(pDH == NULL)
        mexErrMsgTxt("SPMASKMULT: Could not allocate X. Out of memory?");
    S = mxGetPr(pS);
    DG = mxGetPr(pDG);
    DH = mxGetPr(pDH);

    numit = k1*k2;
    for(it = 0; it < numit; ++it)
    {
        /* Compute the increment 
         * DG(i_l,k)=sum_{j_l\in\Omega_{i_l}} S(i_l,j_l)*H(j_l,k), for each
         * k in [1,r]. 
         */
        for (k=0; k<r; ++k){
             DG[I[it]-1 + m*k] += S[it]*H[ J[it]-1 + n*k]; 
             DH[J[it]-1 + n*k] += S[it]*G[ I[it]-1 + m*k]; 
        }
    }
    return;
}


