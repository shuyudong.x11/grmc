/*=================================================================
% function Ax = comp_AOmegaVec(s, Q, I, J)
% Computes AOmega*s, where AOmega is a kn x kn matrix with n diagonal blocks of
% size kxk. 
%
% s: kn-by-1, real, double
% Q: n-by-k, real, double
% I: k1-by-1 row indices, uint32
% J: k1-by-1 column indices, uint32
%
% Complexity: O(..)
%
% Warning: no check of data consistency is performed. Matlab will
% most likely crash if I or J go out of bounds.
%
% Compile with: mex comp_AOmegaVec.c -largeArrayDims
%
% Shuyu Dong, July, 2019. UCLouvain. 
 *=================================================================*/

#include "mex.h"
#include "matrix.h"

/* Input Arguments */

#define	ps	prhs[0]
#define	pQ	prhs[1]
#define	pI	prhs[2]
#define	pJ	prhs[3]

/* Output Arguments */

#define	pAx	plhs[0]

void mexFunction(
          int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray* prhs[] )
{
    uint32_T *I, *J;
    double *s, *Q, *Ax, temp;
    mwSize sz, n, r, N, numit;
    mwIndex k, it;
    
    /* Check for proper number of arguments */
    if (nrhs != 4) { 
        mexErrMsgTxt("Four input arguments are required."); 
    } else if (nlhs != 1) {
        mexErrMsgTxt("A single output argument is required."); 
    } 
    
    /* Check argument classes */
    if(!mxIsUint32(pI) || !mxIsUint32(pJ)) {
        mexErrMsgTxt("I and J must be of class UINT32."); 
    }
    if(!mxIsDouble(pQ)) {
        mexErrMsgTxt("Q must be of class DOUBLE."); 
    }
    if(mxIsComplex(pQ)) {
        mexErrMsgTxt("Q must be REAL."); 
    }
    
    /* Check the dimensions of input arguments */ 
    sz = mxGetM(ps);
    n = mxGetM(pQ);
    r = mxGetN(pQ);
    N = mxGetM(pI);
    
    if(mxGetM(pJ) != N)
        mexErrMsgTxt("Matrix dimensions mismatch for I and J.");
    
    
    /* Get pointers to the data in A, B, I, J */
    s = mxGetPr(ps);
    Q = mxGetPr(pQ);
    I = (uint32_T*) mxGetData(pI);
    J = (uint32_T*) mxGetData(pJ);
    
    /* Create a matrix for the ouput argument */ 
    pAx = mxCreateDoubleMatrix(sz, 1, mxREAL);
    if(pAx == NULL)
        mexErrMsgTxt("SPMASKMULT: Could not allocate Ax. Out of memory?");
    Ax = mxGetPr(pAx);
    
    /* Compute */
    for(it = 0; it < N; ++it)
    {
        /* Compute inner product(j-th row of Q, i-th col of unvec(s)) */
        // That is, compute temp = Q(j-th row, :) * unvec(s)[:, i-th col];
        temp = Q[J[it]-1] * s[(I[it]-1)*r]; 
        for(k = 1; k < r; ++k)
        {
            temp += Q[ J[it]-1 + n*k ]*s[ k + (I[it]-1)*r ];
        }
        // contribute to i-th col of unvec(Ax).
        for (k=0; k<r; ++k){
            Ax[ k + (I[it]-1)*r ] += temp * Q[J[it]-1 + n*k]; 
        }
    }
    
    return;
}



