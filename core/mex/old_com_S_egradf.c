/*=================================================================
% function D = com_egradf(A, B, M, I, J)
% Computes P_Omega(A*B)*H and returns the result in
% a m-by-r real double matrix D.
% I and J must be UINT32 matrices of size k1-by-1.
%
% A: m-by-r, real, double
% B: r-by-n, real, double
% I: k1-by-1 row indices, uint32
% J: k1-by-1 column indices, uint32
%
% Complexity: O(k1k2r)
%
*=================================================================*/


/* #include <math.h> */
#include "mex.h"
#include "matrix.h"

/* Input Arguments */

#define	pA	prhs[0]
#define	pB	prhs[1]
#define	pM	prhs[2]
#define	pI	prhs[3]
#define	pJ	prhs[4]

/* Output Arguments */

#define	pX	plhs[0]
#define	pS	plhs[1]
#define	pDG	plhs[2]
#define	pDH	plhs[3]

void mexFunction(
          int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray* prhs[] )
{
    uint32_T *I, *J;
    double *A, *B,*M, *X, *S, *DG, *DH ; 
    mwSize m, n, r, k1, k2, numit;
    mwIndex k, it;


    /* Check the dimensions of input arguments */ 
    m = mxGetM(pA);
    r = mxGetN(pA);
    n = mxGetN(pB);
    k1 = mxGetM(pI);
    k2 = mxGetN(pI);

    /* Get pointers to the data in A, B, I, J */
    A = mxGetPr(pA);
    B = mxGetPr(pB);
    M = mxGetPr(pM);
    I = (uint32_T*) mxGetData(pI);
    J = (uint32_T*) mxGetData(pJ);
    
    /* Create a matrix for the ouput argument */ 
    pX = mxCreateDoubleMatrix(k1, k2, mxREAL);
    pS = mxCreateDoubleMatrix(k1, k2, mxREAL);
    pDG = mxCreateDoubleMatrix(m, r, mxREAL);
    pDH = mxCreateDoubleMatrix(n, r, mxREAL);
    if(pX == NULL)
        mexErrMsgTxt("SPMASKMULT: Could not allocate X. Out of memory?");
    X = mxGetPr(pX);
    S = mxGetPr(pS);
    DG = mxGetPr(pDG);
    DH = mxGetPr(pDH);

    numit = k1*k2;
    for(it = 0; it < numit; ++it)
    {
        /* Multiply row I(it) of A with col J(it) of B */
        X[it] = A[ I[it]-1 ] * B[ r*(J[it]-1) ];
        for(k = 1; k < r; ++k)
        {
            X[it] += A[ I[it]-1 + m*k ]*B[ k + r*(J[it]-1) ];
        }
        S[it] = X[it] - M[it]; 
        for (k=0; k<r; ++k){
             //DG[I[it]-1 + m*k] += S[it]*H[ J[it]-1 + n*k]; 
             // H equals the transpose of B 
             DG[I[it]-1 + m*k] += S[it]*B[k+ r*(J[it]-1) ]; 
             // G equals A  
             DH[J[it]-1 + n*k] += S[it]*A[ I[it]-1 + m*k]; 
        }
    }
    return;
}


