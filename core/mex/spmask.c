/*=================================================================
% function X = spmask(A, I, J)
% Return a masked matrix of A: observed entries only at (I(k), J(k)) 
% X: a k-by-1 real double matrix X the same size as I and J.
% I and J must be UINT32 arrays of size k-by-1.
%
% A: m-by-n, real, double
% I: k-by-1 row indices, uint32
% J: k-by-1 column indices, uint32
%
% Warning: no check of data consistency is performed. Matlab will
% most likely crash if I or J go out of bounds.
%
% Compile with: mex spmask.c -largeArrayDims
%
% Shuyu Dong (adapted from `rtrmc/spmaskmult.c` by Nicolas Boumal, UCLouvain)
 *=================================================================*/

#include "mex.h"
#include "matrix.h"

/* Input Arguments */

#define	pA	prhs[0]
#define pI  prhs[1]
#define	pJ	prhs[2]

/* Output Arguments */

#define	pX	plhs[0]

/*function X = spmask(A, I, J) */
void mexFunction(
          int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray* prhs[] )
{
    uint32_T *I, *J;
    double *A, *X; 
    mwSize m, n, k;
    mwIndex it;
    
    /* Check for proper number of arguments */
    if (nrhs != 3) { 
        mexErrMsgTxt("Three input arguments are required."); 
    } else if (nlhs != 1) {
        mexErrMsgTxt("A single output argument is required."); 
    } 
    
    /* Check argument classes */
    if(!mxIsUint32(pI) || !mxIsUint32(pJ)) {
        mexErrMsgTxt("I and J must be of class UINT32."); 
    }
    if(!mxIsDouble(pA) ) {
        mexErrMsgTxt("A must be of class DOUBLE."); 
    }
    if(mxIsComplex(pA) ) {
        mexErrMsgTxt("A and B must be REAL."); 
    }
    
    /* Check the dimensions of input arguments */ 
    m = mxGetM(pA);
    n = mxGetN(pA);
    k = mxGetM(pI);
    
    if(mxGetM(pJ) != k )
        mexErrMsgTxt("Matrix dimensions mismatch for I and J.");
    
    
    /* Get pointers to the data in A, B, I, J */
    A = mxGetPr(pA);
    I = (uint32_T*) mxGetData(pI);
    J = (uint32_T*) mxGetData(pJ);
    
    /* Create a matrix for the ouput argument */ 
    pX = mxCreateDoubleMatrix(k, 1, mxREAL);
    if(pX == NULL)
        mexErrMsgTxt("SPMASKMULT: Could not allocate X. Out of memory?");
    X = mxGetPr(pX);
    
    
    /* Compute */
    for(it = 0; it < k; ++it)
    {
        /* Access (row I(it), col J(it)) of A */
        X[it] = A[ I[it]-1 + m*(J[it]-1) ];
    }
    
    return;
}


