function [As, Os] = temp_grls_HessVec(s, H, I, J, Os)

% s: input vector of size [sz,1]. s = vec(G'). 
    sz = numel(s); 
    [nH, r] = size(H); 
    n = sz / r; % This is always an integer, since 
    As = zeros(sz,1); 
    if isempty(Os)
        for i = 1 : n 
            % build B_i.
            % get Omega_i = {j, (i,j)\in\Omega}. 
            O = get_OmegaI(i, I, J); 
            B = H(O,:)'*H(O,:); 
            As((i-1)*r+1: i*r) = B * s((i-1)*r+1: i*r); 
            Os{i} = O; 
        end
    else
        for i = 1 : n 
            % build B_i.
            % get Omega_i = {j, (i,j)\in\Omega}. 
            O = Os{i}; % get_OmegaI(i, I, J); 
            B = H(O,:)'*H(O,:); 
            As((i-1)*r+1: i*r) = B * s((i-1)*r+1: i*r); 
        end
    end

    function O = get_OmegaI(i, I, J)
        ids = find(I==i); 
        O = J(ids); 
    end
    function O = get_OmegaJ(j, I, J)
        ids = find(J==j); 
        O = I(ids); 
    end


end
