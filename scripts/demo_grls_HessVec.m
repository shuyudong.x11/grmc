% Script to see how the following two implementations scale with (m, n, r, SR).  
% Implementation 1: mex/bin/comp_AOmegaVec.mexa64. 
% Implementation 2: mex/temp_AOmegaVec.m 
% Recall: the quadratic form satisfies 
% (1/2)s'As = (1/2)|P_\Omega(GH')|_F^2, where s = vec(G') when A=A^{(1)} and s = vec(H') when A= A^{(2)} defined in Appendix C.5.   
% Denote f(G) or f(H) = (1/2)|P_\Omega(GH')|_F^2. 

clear; close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compare time-efficiency of As(G) between mex and m, for growing n (matrix
% size nxn). 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nte = 10; 
ns = 1000:500:5000; 
% m = n; % without loss of generality, since we only compare time efficiency.  
r = 20; % constant. 
SR = 0.1; 

nset = numel(ns); 
ts_mex = []; 
ts_m = []; 

for i = 1 : nset
    n = ns(i); 
    fprintf('working on n=%d...\n', ns(i)); 
    % Xtar = zeros(n, n); 
    % mcinfo = GRLRMC.genOMEGA_MC(Xtar, SR); 
    sz_omega = SR * n * n; 
    fprintf('Size of Omega is: %d...\n', sz_omega); 

    inds = randsample(n*n, sz_omega); 
    [I, J] = ind2sub([n,n], inds); 
    I = uint32(I); 
    J = uint32(J); 

    tmex = []; tm = []; 
    Os = {}; 
    for t = 1 : nte
        G = randn(n, r); 
        H = randn(n, r); 
        temp = G'; 
        s = temp(:); 

        tic;
        As = comp_AOmegaVec(s, H, I, J); 
        tmex(t) = toc;

        tic;
        [As, Os] = temp_grls_HessVec(s, H, I, J, Os); 
        tm(t) = toc;
    end
    fprintf("=======> Average time used for As(G') with (mex, m) is (%.3e (sec), %.3e (sec))----> speed-up of mex over m is %.2e\n",mean(tmex), mean(tm), mean(tm)/mean(tmex)); 
     
    ts_mex(i) = mean(tmex); 
    ts_m(i) = mean(tm); 
end


figure(); plot(ts_mex); hold on; plot(ts_m); 


