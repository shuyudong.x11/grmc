function spi_noisy = expe_gdatPI_noisy_rsmaller_fewSRs(scale)
% Script for producing matrix completion results on synthetic data. 
%
% Reference: 
% S. Dong, P.-A. Absil, and K. A. Gallivan. Riemannian gradient descent methods
% for graph-regularized matrix completion}}, Preprint, (2019), pp.1--41,
% https://sites.uclouvain.be/absil/2019.06. 
% 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be).
% 

% Data model (Case "graph-lowrank", "graph-fullrank"):  
% 
% Z=AMB'+E, where A = Ug(Lambda), B = Identity and M is a Gaussian random
% matrix. The graph information is incorporated in Z via A and B using the
% eigen pairs of the graph Laplacian matrices. Here, the row-wise similarity
% graph is used. Let L = U.Lambda.U' be the eigen-decomposition of the given
% graph Laplacian matrix, generated according to a certain graph model (see
% keyword "gdatType_g"). Then A = U g(Lambda), where the real-valued function g
% acts on the diagonal entries of the (diagonal) matrix Lambda; the definition
% of the function g is determined by the keyword "gdatType_specfun". 
% 
% -----------------------------------------------------------------------------------------------
%  Keyword                      Description 
% -----------------------------------------------------------------------------------------------
%  gdatSizes                    Dimensions of the data matrix
%  gdatType_Z                   "graph-fullrank" or "graph-lowrank", this determines
%                               whether the random matrix M has a full rank or low rank.
%  gdatRank                     Rank of the data matrix 
%  gdatType_g                   Type of the graph 
%  gdatparam_g                  Optional parameters for sepcific graph models
%  gdatType_specfun             Type of the function g. 
%  gdatType_specfun_doTruncate  False by default: A = U g(Lambda), which is a
%                               full-rank m-by-m matrix. 
%  gdatParam_specfun            Optional parameters for the function g. 
%  gdatNoise                    A logical value followed by a noise level parameter
%                               (SNR) in case the logical value is true.
% -----------------------------------------------------------------------------------------------
%
% Parameters of the problem
% ---------------------------------------------------------------------------
%   Variable               Value corresponding to the notations of the paper 
% ---------------------------------------------------------------------------
%   alpha_r                alpha
%   alpha_c                alpha
%   Lreg_betar             alpha*gamma
%   rank                   k 
% ---------------------------------------------------------------------------
if nargin < 1 
    scale = 5e-2;
end
GDATTYPE_G       = {'community', 'sensor', 'random_k', 'erdos-renyi'} ; 
GDATTYPE_Z       = {'graph-fullrank', 'graph-lowrank', 'graph-agnostic'} ;
GDATTYPE_SPECFUN = {'tikhonov', 'pseudoinv', 'diffusion'};

GDATSIZES        = [1000, 900];
GDATRANK         = 12; 
GDATTYPE_gr      = GDATTYPE_G{1};
GDATTYPE_z       = GDATTYPE_Z{2};
GDATTYPE_spec    = GDATTYPE_SPECFUN{2};
GDATPARAM_spec   = 3; 
GDAT_TARGETKAPPA = nan;
GDATNOISE        = [1, 20]; % the first number being 0 means it is "noiseless". 
GDAT_SCALE       = scale; % Z-> scale*Z/mean(|Z|) so that |Z(i,j)| ~ scale 

SR_MINb = 0.01;
SR_MAXb = 0.035;
SR_MIN = 0.05;
SR_MAX = 0.18; %0.18;
SR_N   = 4;
SR_Nb  = 4; 
Nmethods   = Tester.N_METHODS; 
DOCROSSVAL = zeros(Nmethods, 1); 
WILLRUN    = zeros(Nmethods, 1);
WILLRUN_mul    = zeros(Nmethods, 1);

spi_noisy = Tester('synthetic',...
            'gdatSizes',            GDATSIZES,... 
            'gdatRank',             GDATRANK,...
            'gdatType_Z',           GDATTYPE_z,...
            'gdatType_g',           GDATTYPE_gr,...
            'gdatParam_g',          nan,...
            'gdatType_specfun',     GDATTYPE_spec,...
            'gdatType_specfun_doTruncate',  false,...
            'gdatParam_specfun',    GDATPARAM_spec,...
            'gdatParam_targetKappa',GDAT_TARGETKAPPA,...
            'gdatParam_scale',      GDAT_SCALE,...
            'gdatNoise',            GDATNOISE,...
            'sampl_rate',           [linspace(SR_MINb, SR_MAXb,SR_Nb),...
                                     linspace(SR_MIN, SR_MAX, SR_N)]...
            );


% return;
%% The problem parameters and information of methods to test
LAMBDA = -6;
GAMMAr_min = -2; 
GAMMAr_max = log10(5); 
RANK   = GDATRANK - 4; %ceil(1.0*GDATRANK); 
HP_NCONFIGS = 10;

tab_hp = Tester.gen_tableHp3_('alpha_r',    [LAMBDA; LAMBDA+3],...
                              'Lreg_gammar',[GAMMAr_min; GAMMAr_max],...
                              'rank',       [RANK;RANK], ...
                              'hp_nconfigs', HP_NCONFIGS ) ;
% List of manifold-pbtype: see GRLRMC.NAMES_MAN
% 1 'GH_Euc', ...
% 2 'USV_embeded', ...
% 3 'GH_qprecon',... % 'GH_EucCruderls',...
% 4 'GH_qpreconExa',...
% 5 'GH_qleftinv', ...
% 6 'GH_qleftinvExa'};

% List of algorithms: see Solver.NAMES_ALGO
% 1 'GRALS';...
% 2 'GRALS1';...
% 3 'GRALS2';...
% 4 'RSD_MANOPT';...
% 5 'RCG_MANOPT';...
% 6 'RCGprecon_MANOPT'; ...
% 7 'RSD'; 
% 8 'RCG'}

% List of linesearch methods: see Solver.NAMES_LS
% 1 'lsArmijo'; ...
% 2 'lsFree';...
% 3 'lsBB';...

% Will test: 
% Domain    Algo        SS-type             line-search 
% -------------------------------------------------------------
% Euc       GRALS1         -                -         ..........1 
% Euc       GRALS2         -                -         ..........1
% Euc       RSD-manopt  linemin (Eq.(30))   lsArmijo ...........1 
% Euc       RCG-manopt  linemin             lsArmijo ...........1
% Qprecon   RSD-manopt  linemin             lsArmijo  ..........1
% Qprecon   RCG-manopt  linemin             lsArmijo  ..........1
% Qprecon   RSD         linemin             lsFree    ..........1
% Qprecon   RCG         linemin             lsFree    ..........1
% Qprecon   RSD         one (s_t\equiv 1)   lsArmijo ...........1
% Euc       AltMin1         -                -         ..........1 
% Euc       AltMin2         -                -         ..........1

IDS_MAN =       [1 1 1 1 4 4 4 4 4 1 1] ;
IDS_ALG =       [2 3 4 5 4 5 7 8 7 9 10] ;
IDS_SS0 =       [1 1 1 1 1 1 1 1 2 1 1] ;
IDS_LS =        [1 1 1 1 1 1 2 2 1 1 1] ;

WILLRUN =       [1 0 1 1 0 0 1 1 1 1 1];
WILLRUN_mul =   [0 0 0 0 0 0 0 0 0 0 0];
doCrossval = repmat(0,[1,numel(WILLRUN)]); 

TOLGRADNORM = 1e-12;
NEPOCH = 3000;
MAXTIME    = 20; % in seconds. 
N_REPEATTESTS = 1;

nte_total = numel(spi_noisy.range_samplrates) * HP_NCONFIGS * N_REPEATTESTS *...
sum([WILLRUN, WILLRUN_mul]); 
time_total = nte_total * MAXTIME; 
fprintf('Estimated tests to run: %d \n', nte_total); 
fprintf('Estimated running time: %f sec (%f mins) \n', time_total, ...
time_total/60.0); 
pause(2);
fprintf('Start tests:...\n'); 

[res_fix,res_mul, fu] = ...
spi_noisy.run_tester_2(tab_hp, ...
                'ids_man',    IDS_MAN, ...
                'ids_alg',    IDS_ALG, ...
                'ids_ss0',    IDS_SS0, ...
                'ids_ls',     IDS_LS, ...
                'doCrossval2',         doCrossval,...
                'willRun2',            WILLRUN,...
                'willRun2_multiphase', WILLRUN_mul,...
                'repeatTests_tolGrad', TOLGRADNORM,...
                'tolgradnorm',         TOLGRADNORM,...
                'maxtime',             MAXTIME,...
                'n_epoch',             NEPOCH, ... 
                'n_repeatTests',       N_REPEATTESTS);

% %[hh,cur] = Tester.produce_fig512(spi_n0rstar, 1/na, 'all/1/na', 'iter/epoch/time/na', 'RMSE_t/gradnorm/na', 'id_ms/na') ;
% close all; [hh,cur] = Tester.produce_fig512(spi_noisy, 1, '1', 'flopsCum') ;
% close all; [hh,cur] = Tester.produce_fig512(spi_noisy, 1, '1', 'time') ;
% close all; [hh,cur] = Tester.produce_fig512(spi_noisy, 1, '1', 'flopsCum','gradnorm') ;
% close all; [hh,cur] = Tester.produce_fig512(spi_noisy, 1, '1', 'time', 'gradnorm') ;
% close all; [hh,cur] = Tester.produce_figTrVsTe(spi_noisy, 1, '1', 'flopsCum') ;
% close all; [hh,cur] = Tester.produce_figTrVsTe(spi_noisy, 1, '1', 'time') ;
% close all; 
% Tester.save_res2matfile(spi_noisy) 

end

