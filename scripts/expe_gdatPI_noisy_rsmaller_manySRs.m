function spi_noisy = expe_gdatPI_noisy_rsmaller_manySRs()

GDATTYPE_G       = {'community', 'sensor', 'random_k', 'erdos-renyi'} ; 
GDATTYPE_Z       = {'graph-fullrank', 'graph-lowrank', 'graph-agnostic'} ;
GDATTYPE_SPECFUN = {'tikhonov', 'pseudoinv', 'diffusion'};

GDATSIZES        = [500, 600];
GDATRANK         = 12; 
GDATTYPE_gr      = GDATTYPE_G{1};
GDATTYPE_z       = GDATTYPE_Z{2};
GDATTYPE_spec    = GDATTYPE_SPECFUN{2};
GDATPARAM_spec   = 2; 
GDAT_TARGETKAPPA = nan;
GDATNOISE        = [1, 20]; % the first number being 0 means it is "noiseless". 
SR_MINb = 0.01;
SR_MAXb = 0.035;
SR_MIN = 0.05;
SR_MAX = 0.18;
SR_N   = 7;
SR_Nb  = 3; 
Nmethods   = Tester.N_METHODS; 
DOCROSSVAL = zeros(Nmethods, 1); 
WILLRUN    = zeros(Nmethods, 1);
WILLRUN_mul    = zeros(Nmethods, 1);

% List of manifold-pbtype: see GRLRMC.NAMES_MAN
% 1 'GH_Euc', ...
% 2 'USV_embeded', ...
% 3 'GH_qprecon',... % 'GH_EucCruderls',...
% 4 'GH_qpreconExa',...
% 5 'GH_qleftinv', ...
% 6 'GH_qleftinvExa'};

% List of algorithms: see Solver.NAMES_ALGO
% 1 'GRALS';...
% 2 'GRALS1';...
% 3 'GRALS2';...
% 4 'RSD_MANOPT';...
% 5 'RCG_MANOPT';...
% 6 'RCGprecon_MANOPT'; ...
% 7 'RSD'; 
% 8 'RCG'}

% List of linesearch methods: see Solver.NAMES_LS
% 1 'lsArmijo'; ...
% 2 'lsFree';...
% 3 'lsBB';...

% Will test (depending on what we want to show in the paper) 
% GH-qprecon RSD lsArmijo 

IDS_MAN =       [1 3 3 4 6] ;
IDS_ALG =       [2 4 5 5 5] ;
IDS_LS =        [1 1 1 1 1] ;
WILLRUN =       [1 1 1 1 1];
WILLRUN_mul =   [0 1 1 1 1];
doCrossval = repmat(0,[1,5]); 

spi_noisy = Tester('synthetic',...
            'gdatSizes',            GDATSIZES,... 
            'gdatRank',             GDATRANK,...
            'gdatType_Z',           GDATTYPE_z,...
            'gdatType_g',           GDATTYPE_gr,...
            'gdatParam_g',          nan,...
            'gdatType_specfun',     GDATTYPE_spec,...
            'gdatType_specfun_doTruncate',  false,...
            'gdatParam_specfun',    GDATPARAM_spec,...
            'gdatParam_targetKappa',GDAT_TARGETKAPPA,...
            'gdatNoise',            GDATNOISE,...
            'sampl_rate',           [linspace(SR_MINb, SR_MAXb,SR_Nb),...
                                     linspace(SR_MIN, SR_MAX, SR_N)]...
            );
            
LAMBDA = -4;
GAMMAr_min = -2; 
GAMMAr_max = log10(5); 
RANK   = GDATRANK - 2; %ceil(1.0*GDATRANK); 
HP_NCONFIGS = 10;

TOLGRADNORM = 1e-12;
NEPOCH = 600;
N_REPEATTESTS = 11;

%% the problem parameters and information of methods to test
tab_hp = Tester.gen_tableHp3_('alpha_r',    [LAMBDA; LAMBDA+4],...
                              'Lreg_gammar',[GAMMAr_min; GAMMAr_max],...
                              'rank',       [RANK;RANK], ...
                              'hp_nconfigs', HP_NCONFIGS ) ;
[res_fix,res_mul] = ...
spi_noisy.run_tester_2(tab_hp, ...
                'ids_man',    IDS_MAN, ...
                'ids_alg',    IDS_ALG, ...
                'ids_ls',     IDS_LS, ...
                'doCrossval2',         doCrossval,...
                'willRun2',            WILLRUN,...
                'willRun2_multiphase', WILLRUN_mul,...
                'repeatTests_tolGrad', TOLGRADNORM,...
                'tolgradnorm',         TOLGRADNORM,...
                'n_epoch',             NEPOCH, ... 
                'n_repeatTests',       N_REPEATTESTS);

%[hh,cur] = Tester.produce_fig512(spi_n0rstar, 1/na, 'all/1/na', 'iter/epoch/time/na', 'RMSE_t/gradnorm/na', 'id_ms/na') ;

end

