function pb = expe_gdat_PIn0rstar_manySRs()
% Script for producing matrix completion results on synthetic data. 
%
% Reference: 
% S. Dong, P.-A. Absil, and K. A. Gallivan. Riemannian gradient descent methods
% for graph-regularized matrix completion}}, Preprint, (2019), pp.1--41,
% https://sites.uclouvain.be/absil/2019.06. 
% 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be).
% 

% Data model (Case "graph-lowrank", "graph-fullrank"):  
% 
% Z=AMB'+E, where A = Ug(Lambda), B = Identity and M is a Gaussian random
% matrix. The graph information is incorporated in Z via A and B using the
% eigen pairs of the graph Laplacian matrices. Here, the row-wise similarity
% graph is used. Let L = U.Lambda.U' be the eigen-decomposition of the given
% graph Laplacian matrix, generated according to a certain graph model (see
% keyword "gdatType_g"). Then A = U g(Lambda), where the real-valued function g
% acts on the diagonal entries of the (diagonal) matrix Lambda; the definition
% of the function g is determined by the keyword "gdatType_specfun". 
% 
% -----------------------------------------------------------------------------------------------
%  Keyword                      Description 
% -----------------------------------------------------------------------------------------------
%  gdatSizes                    Dimensions of the data matrix
%  gdatType_Z                   "graph-fullrank" or "graph-lowrank", this determines
%                               whether the random matrix M has a full rank or low rank.
%  gdatRank                     Rank of the data matrix 
%  gdatType_g                   Type of the graph 
%  gdatparam_g                  Optional parameters for sepcific graph models
%  gdatType_specfun             Type of the function g. 
%  gdatType_specfun_doTruncate  False by default: A = U g(Lambda), which is a
%                               full-rank m-by-m matrix. 
%  gdatParam_specfun            Optional parameters for the function g. 
%  gdatNoise                    A logical value followed by a noise level parameter
%                               (SNR) in case the logical value is true.
% -----------------------------------------------------------------------------------------------
%
% Parameters of the problem
% ---------------------------------------------------------------------------
%   Variable               Value corresponding to the notations of the paper 
% ---------------------------------------------------------------------------
%   alpha_r                alpha
%   alpha_c                alpha
%   Lreg_betar             alpha*gamma
%   rank                   k 
% ---------------------------------------------------------------------------

GDATTYPE_G       = {'community', 'sensor', 'random_k', 'erdos-renyi'} ; 
GDATTYPE_Z       = {'graph-fullrank', 'graph-lowrank', 'graph-agnostic'} ;
GDATTYPE_SPECFUN = {'tikhonov', 'pseudoinv', 'diffusion'};

GDATSIZES        = [500, 600];
GDATRANK         = 12; 
GDATTYPE_gr      = GDATTYPE_G{1};
GDATTYPE_z       = GDATTYPE_Z{2};
GDATTYPE_spec    = GDATTYPE_SPECFUN{2};
GDATPARAM_spec   = 2; 
GDAT_TARGETKAPPA = nan;
GDATNOISE        = [0, 20]; % the first number being 0 means it is "noiseless". 
SR_MINb = 0.005;
SR_MAXb = 0.05;
SR_MIN = 0.05;
SR_MAX = 0.28;
SR_N   = 10;
SR_Nb  = 0; 
Nmethods   = Tester.N_METHODS; 
DOCROSSVAL = zeros(Nmethods, 1); 
WILLRUN    = zeros(Nmethods, 1);
WILLRUN_mul    = zeros(Nmethods, 1);

% List of manifold-pbtype: see GRLRMC.NAMES_MAN
% 1 'GH_Euc', ...
% 2 'USV_embeded', ...
% 3 'GH_qprecon',... % 'GH_EucCruderls',...
% 4 'GH_qpreconExa',...
% 5 'GH_qleftinv', ...
% 6 'GH_qleftinvExa'};

% List of algorithms: see Solver.NAMES_ALGO
% 1 'GRALS';...
% 2 'GRALS1';...
% 3 'GRALS2';...
% 4 'RSD_MANOPT';...
% 5 'RCG_MANOPT';...
% 6 'RCGprecon_MANOPT'; ...
% 7 'RSD'; 
% 8 'RCG'}

% List of linesearch methods: see Solver.NAMES_LS
% 1 'lsArmijo'; ...
% 2 'lsFree';...
% 3 'lsBB';...

% Will test (depending on what we want to show in the paper) 
% GH-qprecon RSD lsArmijo 

IDS_MAN =       [1 1 3 3 4 6] ;
IDS_ALG =       [2 5 4 5 5 5] ;
IDS_LS =        [1 1 1 1 1 1] ;
WILLRUN =       [1 1 1 1 1 1];
WILLRUN_mul =   [0 1 1 1 1 1];
doCrossval = repmat(0,[1,6]); 

pb = Tester('synthetic',...
            'gdatSizes',            GDATSIZES,... 
            'gdatRank',             GDATRANK,...
            'gdatType_Z',           GDATTYPE_z,...
            'gdatType_g',           GDATTYPE_gr,...
            'gdatParam_g',          nan,...
            'gdatType_specfun',     GDATTYPE_spec,...
            'gdatType_specfun_doTruncate',  false,...
            'gdatParam_specfun',    GDATPARAM_spec,...
            'gdatParam_targetKappa',GDAT_TARGETKAPPA,...
            'gdatNoise',            GDATNOISE,...
            'sampl_rate',           [linspace(SR_MINb, SR_MAXb,SR_Nb),...
                                     linspace(SR_MIN, SR_MAX, SR_N)]...
            );
            
LAMBDA = -4;
GAMMAr_min = -2; 
GAMMAr_max = log10(5); 
RANK   = ceil(1.0*GDATRANK); 
HP_NCONFIGS = 5;

TOLGRADNORM = 1e-12;
NEPOCH = 600;
N_REPEATTESTS = 20; % no need to make it an odd number. 

%% the problem parameters and information of methods to test
tab_hp = Tester.gen_tableHp3_('alpha_r',    [LAMBDA; LAMBDA+4],...
                              'Lreg_gammar',[GAMMAr_min; GAMMAr_max],...
                              'rank',       [RANK;RANK], ...
                              'hp_nconfigs', HP_NCONFIGS ) ;

pb.info_methods = Tester.gen_infoMethods2(Tester.parseArgs('infoMethods2',...
                                'ids_man',          IDS_MAN, ...
                                'ids_alg',          IDS_ALG, ...
                                'ids_ls',           IDS_LS, ...
                                'doCrossval2',         doCrossval,...
                                'willRun2',            WILLRUN,...
                                'willRun2_multiphase', WILLRUN_mul) );

% return; 
%%                               
[res_fix,res_mul] = ...
pb.run_tester_2(tab_hp, ...
                'ids_man',    IDS_MAN, ...
                'ids_alg',    IDS_ALG, ...
                'ids_ls',     IDS_LS, ...
                'doCrossval2',         doCrossval,...
                'willRun2',            WILLRUN,...
                'willRun2_multiphase', WILLRUN_mul,...
                'repeatTests_tolGrad', TOLGRADNORM,...
                'tolgradnorm',         TOLGRADNORM,...
                'n_epoch',             NEPOCH, ... 
                'n_repeatTests',       N_REPEATTESTS);

%[hh,cur] = Tester.produce_fig512(spi_n0rstar, 1/na, 'all/1/na', 'iter/epoch/time/na', 'RMSE_t/gradnorm/na', 'id_ms/na') ;
close all; [hh,cur] = Tester.produce_fig512(pb, 1, '1', 'epoch') ;
close all; [hh,cur] = Tester.produce_fig512(pb, 1, '1', 'time') ;
close all; [hh,cur] = Tester.produce_fig512(pb, 1, '1', 'epoch','gradnorm') ;
close all; [hh,cur] = Tester.produce_fig512(pb, 1, '1', 'time', 'gradnorm') ;
close all; [hh,cur] = Tester.produce_figTrVsTe(pb, 1, '1', 'epoch') ;
close all; [hh,cur] = Tester.produce_figTrVsTe(pb, 1, '1', 'time') ;
close all; [hh,cur] = Tester.produce_figRecVsSR(pb, 'RMSE_t', 'rsuccess') ;
close all;
Tester.save_res2matfile(pb)

end


