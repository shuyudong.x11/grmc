function rtraf = expe_realTraffic_2()
% Script for producing matrix completion results on real data. 
%
% Reference: 
% S. Dong, P.-A. Absil, and K. A. Gallivan. Riemannian gradient descent methods
% for graph-regularized matrix completion}}, Preprint, (2019), pp.1--41,
% https://sites.uclouvain.be/absil/2019.06. 
% 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be).
%
datanames = {'traffic', 'lmafitML100k', 'lmafitML1M', 'pwsmooth'};
dataset = datanames{1};
methods_init = {'random','M0'};
method_init = methods_init{2};

SR_MIN = 0.05;
SR_MAX = 0.2;
SR_N   = 3;
RG_SR = [linspace(0.01,0.03,2), linspace(SR_MIN, SR_MAX, SR_N)];
rtraf = Tester(dataset,...
        'sampl_rate', RG_SR,...
        'use_submatrix', false, 'dims_submatrix', [nan, nan],...
        'fromSideinfo', ones(numel(Tester.DEFAULT_buildLmat.methodname),1)...
        );

%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%%%%%%%%
% Will test: 
% Domain        Algo        SS-type             line-search 
% -------------------------------------------------------------
% Euc           GRALS1         -                -         ..........1 
% Euc           GRALS2         -                -         ..........1
% Euc           RSD-manopt  linemin (Eq.(30))   lsArmijo ...........1 
% Euc           RCG-manopt  linemin             lsArmijo ...........1
% QpreconExa    RSD-manopt  linemin             lsArmijo  ..........1
% QpreconExa    RCG-manopt  linemin             lsArmijo  ..........1
% Qprecon~      RSD         linemin             lsFree    ..........1
% Qprecon~      RCG         linemin             lsFree    ..........1
% Qprecon~      RSD         one (s_t\equiv 1)   lsArmijo ...........1
% Qleftin~      RSD         linemin             lsFree    ..........1
% Qleftin~      RCG         linemin             lsFree    ..........1
% Euc       AltMin1         -                -         ..........1 
% Euc       AltMin2         -                -         ..........1
IDS_MAN =       [1 1 1 1 4 4 4 4 4 6 6 1 1] ;
IDS_ALG =       [2 3 4 5 4 5 7 8 7 7 8 9 10] ;
IDS_SS0 =       [1 1 1 1 1 1 1 1 2 1 1 1 1] ;
IDS_LS =        [1 1 1 1 1 1 2 2 1 2 2 1 1] ;
                                          
WILLRUN =       [1 0 1 1 0 0 1 1 1 1 1 1 1];
WILLRUN_mul =   [0 0 0 0 0 0 0 0 0 0 0 0 0];
doCrossval = repmat(0,[1,numel(WILLRUN)]); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LAMBDA = -2;
GAMMAr = -2; 
GAMMAr2 = log10(2); 
RANK   = 18; 
HP_NCONFIGS = 20;

TOLGRADNORM = 1e-8;
NEPOCH = 3000;
MAXTIME = 20; 
N_REPEATTESTS = 1;

nte_total = numel(rtraf.range_samplrates) * HP_NCONFIGS * N_REPEATTESTS *...
sum([WILLRUN, WILLRUN_mul]); 
time_total = nte_total * MAXTIME; 
fprintf('Estimated tests to run: %d \n', nte_total); 
fprintf('Estimated running time: %f sec (%f mins) \n', time_total, ...
time_total/60.0); 
pause;
fprintf('Start tests:...\n'); 

%% the problem parameters and information of methods to test
tab_hp = rtraf.gen_tableHp3_('alpha_r',      [LAMBDA;LAMBDA+3],...
                        'Lreg_gammar',  [GAMMAr; GAMMAr2],...
                        'rank',         [RANK;RANK], ...
                        'hp_nconfigs',  HP_NCONFIGS  );

info_methods = Tester.gen_infoMethods2(Tester.parseArgs('infoMethods2',...
                                'ids_man',          IDS_MAN, ...
                                'ids_alg',          IDS_ALG, ...
                                'ids_ss0',          IDS_SS0, ...
                                'ids_ls',           IDS_LS, ...
                                'doCrossval2',         doCrossval,...
                                'willRun2',            WILLRUN,...
                                'willRun2_multiphase', WILLRUN_mul) );

% return;
%%

rtraf.run_tester_2(tab_hp, ...
                'ids_man',          IDS_MAN, ...
                'ids_alg',          IDS_ALG, ...
                'ids_ls',           IDS_LS, ...
                'ids_ss0',          IDS_SS0, ...
                'doCrossval2',         doCrossval,...
                'willRun2',            WILLRUN,...
                'willRun2_multiphase', WILLRUN_mul,...
                'repeatTests_tolGrad', TOLGRADNORM,...
                'tolgradnorm',         TOLGRADNORM,...
                'maxtime',             MAXTIME,...
                'n_epoch',             NEPOCH, ... 
                'n_repeatTests',       N_REPEATTESTS);

end

