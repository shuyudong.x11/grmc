% Let's build a low-rank matrix M where blocs of consecutive rows and
% columns look alike.
%
% From such a data matrix, a GRMC algorithm should learn a graph
% (like in Kalofolias's paper) and apply a GRMC algorithm.

% PA, started 2018-12-12

%% Parameters:

m = 500;  % number of rows of M
n = 600;  % number of columns of M
r = 10;  % rank of M (exact rank if noise_level_M = 0)

nb_clusters_r = 6;  % number of clusters for the rows (the actual number may be smaller
         % because some clusters may get a zero size)
nb_clusters_c = n;  % number of clusters for the columns (same remark)

noise_level_r = 1e-1;  % parameters to adjust the level of noise
noise_level_c = 0; %1e-9;
noise_level_M = 1e-7;


%% Choose the start index of each cluster:

% Vector of cluster start indices for rows:
starts_clusters_r = zeros(nb_clusters_r+1,1);
starts_clusters_r(1) = 1;
starts_clusters_r(2:nb_clusters_r) = sort( floor(m*rand(nb_clusters_r-1,1))+1 );
starts_clusters_r(nb_clusters_r+1) = m+1;  % artificial final cluster start, for convenience

% Vector of cluster start indices for columns:
starts_clusters_c = zeros(nb_clusters_c+1,1);
starts_clusters_c(1) = 1;
starts_clusters_c(2:nb_clusters_c) = sort( floor(n*rand(nb_clusters_c-1,1))+1 );
starts_clusters_c(nb_clusters_c+1) = n+1;


%% Generate ordered selection matrices A_r and A_c:

A_r = zeros(m,nb_clusters_r);
for i = 1:nb_clusters_r
    A_r(starts_clusters_r(i):starts_clusters_r(i+1)-1,i) = ones(starts_clusters_r(i+1)-starts_clusters_r(i),1);
end

A_c = zeros(n,nb_clusters_c);
for i = 1:nb_clusters_c
    A_c(starts_clusters_c(i):starts_clusters_c(i+1)-1,i) = ones(starts_clusters_c(i+1)-starts_clusters_c(i),1);
end

%% Generate M:

P = randn(nb_clusters_r,r);
Q = randn(nb_clusters_c,r);

G = A_r * P + noise_level_r*randn(m,r);
H = A_c * Q + noise_level_c*randn(n,r); 
M = G * H' + noise_level_M * randn(m,n);

if 0
    save('data/pwsmooth.mat', 'M');
end

