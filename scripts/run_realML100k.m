%% script for experiment on ML100k. 

ml100k1 = expe_realML100k_2() ; 

close all; Tester.save_res2matfile(ml100k1); 

close all; [hh,rPIf] = Tester.produce_fig512(ml100k1, 1, '1', 'flopsCum') ;
close all; [hh,rPIt] = Tester.produce_fig512(ml100k1, 1, '1', 'time') ;
close all; [hh,gPIf] = Tester.produce_fig512(ml100k1, 1, '1', 'flopsCum','gradnorm') ;
close all; [hh,gPIt] = Tester.produce_fig512(ml100k1, 1, '1', 'time', 'gradnorm') ;

close all; [hh,cf] = Tester.produce_figTrVsTe(ml100k1, 1, '1', 'flopsCum') ;
close all; [hh,ct] = Tester.produce_figTrVsTe(ml100k1, 1, '1', 'time') ;
close all; [hh,cf3] = Tester.produce_figTrVsTe(ml100k1, 3, '1', 'time') ;

% close all; [hh,rec] = Tester.produce_figRecVsSR(ml100k1, 'RMSE_t', 'score') ;
tabs         = Tester.produce_tabsMCscore_meth_model(ml100k1) ; 

