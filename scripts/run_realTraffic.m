%% script for experiment on the Traffic dataset. 

rtra1 = expe_realTraffic_2() ; 

close all; Tester.save_res2matfile(rtra1); 

close all; [hh,rPIf] = Tester.produce_fig512(rtra1, 1, '1', 'flopsCum') ;
close all; [hh,rPIt] = Tester.produce_fig512(rtra1, 1, '1', 'time') ;
close all; [hh,gPIf] = Tester.produce_fig512(rtra1, 1, '1', 'flopsCum','gradnorm') ;
close all; [hh,gPIt] = Tester.produce_fig512(rtra1, 1, '1', 'time', 'gradnorm') ;

close all; [hh,cf] = Tester.produce_figTrVsTe(rtra1, 1, '1', 'flopsCum') ;
close all; [hh,ct] = Tester.produce_figTrVsTe(rtra1, 1, '1', 'time') ;
close all; [hh,cf3] = Tester.produce_figTrVsTe(rtra1, 3, '1', 'time') ;

close all; [hh,rec] = Tester.produce_figRecVsSR(rtra1, 'RMSE_t', 'score') ;

tabs         = Tester.produce_tabsMCscore_meth_model(rtra1) ; 

