% Script to test the correctness of mex/bin/comp_AOmegaVec.mexa64.
% To look at how these two implementations scale with (m, n, r, SR). See scripts/demo_grls_HessVec.m. 
% Recall: the quadratic form satisfies 
% (1/2)s'As = (1/2)|P_\Omega(GH')|_F^2, where s = vec(G') when A=A^{(1)} and s = vec(H') when A= A^{(2)} defined in Appendix C.5.   
% Denote f(G) or f(H) = (1/2)|P_\Omega(GH')|_F^2. 

m = 1000;
n = 2000;
r = 20; 
nte = 1; 
Xtar = randn(m, n); 

mcinfo = GRLRMC.genOMEGA_MC(Xtar, 0.1); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Equivalence Test for As(G) where A is defined via identification 
% f(G) = (1/2)vec(G')' A vec(G').  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Test-G (equivalence): test if As = vec(H' * P_\Omega(GH')'), which is true by identification s. 
fprintf("=======>(G-test) Starting to test the correctness of the Hessian-vector multiplication A{(1)}*vec(G'):.. \n"); 

fprintf("As is computed by comp_AomegaVec.mexa64, As_def computed from its definition As_def(G) = vec(H' * P_Omega(GH')').. \n"); 

iscorr = []; tol = 1e-8; 
for i = 1 : nte
G = randn(m, r); 
H = randn(n, r); 
temp = G'; 
s = temp(:); 

As = comp_AOmegaVec(s, H, mcinfo.I, mcinfo.J); 

% compute As_def
gh_omega = spmaskmult(G, H', mcinfo.I, mcinfo.J ); 
GH_omega = sparse(double(mcinfo.I), double(mcinfo.J), gh_omega,...
                            mcinfo.size_M(1), mcinfo.size_M(2));
mat_As_def = H' * GH_omega'; 
As_def = mat_As_def(:); 

% test 
err = norm(As-As_def) ; 
iscorr(end+1) = (err<tol); 
fprintf('Error (As, As_def) is %.3e ----> verif test is (true=1,false=0): %d\n',err, iscorr(end)); 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Equivalence Test for As(H) where A is defined via identification 
% f(H) = (1/2)vec(H')' A vec(H').  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Test-H (equivalence): test if As = vec(G' * P_\Omega(GH')'), which is true by identification s. 
fprintf("=======>(H-test) Starting to test the correctness of the Hessian-vector multiplication A{(2)}*vec(H'):.. \n"); 
fprintf("As is computed by comp_AomegaVecH.mexa64, As_def computed from its definition As_def(H) = vec(G' * P_Omega(GH')).. \n"); 

iscorr = []; tol = 1e-8; 
for i = 1 : nte
G = randn(m, r); 
H = randn(n, r); 
temp = H'; 
s = temp(:); 

As = comp_AOmegaVecH(s, G, mcinfo.I, mcinfo.J); 

% compute As_def
gh_omega = spmaskmult(G, H', mcinfo.I, mcinfo.J ); 
GH_omega = sparse(double(mcinfo.I), double(mcinfo.J), gh_omega,...
                            mcinfo.size_M(1), mcinfo.size_M(2));
mat_As_def = G' * GH_omega; 
As_def = mat_As_def(:); 

% test 
err = norm(As-As_def) ; 
iscorr(end+1) = (err<tol); 
fprintf('Error (As, As_def) is %.3e ----> verif test is (true=1,false=0): %d\n',err, iscorr(end)); 
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compare time-efficiency of As(G) between mex and m  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tmex = []; tm = []; 
for i = 1 : nte
G = randn(m, r); 
H = randn(n, r); 
temp = G'; 
s = temp(:); 

tic;
As = comp_AOmegaVec(s, H, mcinfo.I, mcinfo.J); 
tmex(i) = toc;

tic;
As = temp_grls_HessVec(s, H, mcinfo.I, mcinfo.J); 
tm(i) = toc;
end
fprintf("=======> Average time used for As(G') with (mex, m) is (%.3e (sec), %.3e (sec))----> speed-up of mex over m is %.2e\n",mean(tmex), mean(tm), mean(tm)/mean(tmex)); 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test w.r.t. a necessary condition 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if 0 
% Test 1: verify if the necessary condition (1/2)s'As = (1/2)|P_\Omega(GH')|_F^2 is satisfied. 
% s= vec(G'), As = comp_AOmegaVec(s, H, I, J). 
iscorrb = []; tol = 1e-5; 
for i = 1 : nte
G = randn(m, r); 
H = randn(n, r); 
temp = G'; 
s = temp(:); 

As = comp_AOmegaVec(s, H, mcinfo.I, mcinfo.J); 
val = 0.5 * s' * As; 

gh_omega = spmaskmult(G, H', mcinfo.I, mcinfo.J ); 
valb = 0.5 * sum(gh_omega.^2); 
% test 
err =abs(val-valb) ; 
fprintf('[Quadratic values (val vs val_def) are (%f, %f)===> error is %.3e\n',val, valb, err); 
iscorrb(end+1) = (err<tol); 
end
end




