addpath(genpath_exclude('./',{'/.git', '/.deprecated', '/utils/deprecated'}));

% set default marker style. see
% [here](https://nl.mathworks.com/matlabcentral/answers/49062-is-it-possible-to-increment-color-and-markers-automatically-for-a-plot-in-a-loop)
set(0,'DefaultAxesLineStyleOrder',{'^-','o-','*-','+-','x-','s-','d-','.-','v-','>-','<-','p-','h-'});


