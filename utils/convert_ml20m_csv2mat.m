% 1. read table
ff=readtable('data/ratings.csv');

% ff.{userId,movieId,ratings} form the data in COO format, of size N x 3. We only need to make sure about the matrix indices are [1:numel(unique(userId))] x [1:numel(unique(movieId))], which is the real/invariant/incompressible dimension of the user-movie-ratings data matrix. 

% It turns out: 
nu = numel(unique(ff.userId)) % which is ~ 138,000
nm = numel(unique(ff.movieId)) % which is ~ 27,000

% ff.userId is ready as user labels since it ranges exactly from 1 to numel(unique(userId)).

% re-assign labels to movieId so that they range from 1 to length(unique(movieId)) 
[mid2, ia, ic] = unique(ff.movieId);

rg_mid = [1:numel(mid2)]';

mid_f = rg_mid(ic); % movieId = mid2(ic); according to the definition of the functiono [C, ia, ic] = unique(A). 

% 
mat = sparse(ff.userId, mid_f, ff.rating, nu,nm);

% 
mat = mat'; % only because we want to construct the graph on the smaller dimension, which is numel(unique(movieId)) here.

save('data/ML20m.mat', 'mat');

% %%%%%%%%%%%%%%%%%%%%%%%%
% % build k-NN graph 
% % to construct features:
% % the naive way is to use 
% [u,s,v] = svds(mat, rank_feat); % rank_feat = 20; 
% feat_r =u*s;

% % then call the knn_divconq.mexa64 
% K = 20;
% knn_divconq(feat_r, K, 0.15); 

% % look for the output file in COO format (neighbor[i][l], i, d[i,j(l)]) 
% %%%%%%%%%%%%%%%%%%%%%%%