function [U, Lambda]= eig_ordered( A, order )
if nargin < 2
    order = 'descend';
end
% Eigendecomposition of real symm matrix, ordered by decreasing eigenvalues 

[U, D]= eig(A);
[Lambda, I] = sort( diag(D), order);
U = U(:,I);

end

