
function dataset = loaddata(dataset_name, varargin)
    
    root_data = 'data/';

    matfile = load(sprintf('%s%s.mat',root_data, dataset_name));
    switch dataset_name
        case 'mgcnnML100k'
            dataset = matfile;
        case 'traffic'
            dataset = matfile.traffic.Ymat;
        case 'electricity'
            dataset = matfile.elec.Ymat;
        case {'lmafitML100k', 'lmafitML1M', 'jester_1'}
            dataset = matfile;
        otherwise
            dataset = matfile;
    end

end
