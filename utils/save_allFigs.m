function save_allFigs(format, figname, dir_name)
	if nargin < 1
		format = 'epsc';
	end
	if nargin < 2
		figname = '';
    end
	if nargin < 3
		dir_name = [];
	end	
    if isempty(dir_name)
	% if ~exist('dir_name','var')
		if ismac || isunix
			dir_name = '/data/Dropbox/DBucl/temporary/matfiles/';
			% dir_name = '~/ucl/reportnotes/src/figures/';
		else
			dir_name = 'C:\Users\sdong\Dropbox\DBucl\temporary\matfiles\';
		end
	end

	tmps= datetickstr(now,'hhMM_ddmmm_');
	timeStr = tmps{1};

    figlist=findobj('type','figure');
	for i=1:numel(figlist)
	    saveas(figlist(i),fullfile(dir_name,[timeStr,'figure_', figname, int2str(figlist(i).Number)]), format);
	    % saveas(figlist(i),fullfile(dir_name,[timeStr,'figure' int2str(figlist(i).Number)]), 'epsc');
	end
end
